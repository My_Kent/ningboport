
INSERT INTO Driver(driverName,password) VALUES ("admin","admin");
INSERT INTO Driver(driverName,password) VALUES ("a","a");
INSERT INTO Driver(driverName,password) VALUES ("b","b");
INSERT INTO Driver(driverName,password) VALUES ("c","c");
INSERT INTO Driver(driverName,password) VALUES ("d","d");
INSERT INTO Driver(driverName,password) VALUES ("e","e");
INSERT INTO Driver(driverName,password) VALUES ("f","f");

INSERT INTO Vehicle VALUES ("11223");
INSERT INTO Vehicle VALUES ("11111");
INSERT INTO Vehicle VALUES ("00000");

INSERT INTO Ports VALUES ("BLCT", 121.517, 28.33);
INSERT INTO Ports VALUES ("BLCT2", 120.2367, 28.3355);
INSERT INTO Ports VALUES ("BLCT3", 121.2367, 28.5544);
INSERT INTO Ports VALUES ("BLCTYD", 121.1122, 29.9833);
INSERT INTO Ports VALUES ("BLCTZS", 121.34211, 29.9822);
INSERT INTO Ports VALUES ("BLCTE", 121.8832, 30.58);
INSERT INTO Ports VALUES ("BLCTMS", 120.9982, 28.4433);
INSERT INTO Ports VALUES ("ZHCT", 122.2345, 28.9834);
INSERT INTO Ports VALUES ("B2SCT", 121.2345, 29.8888);
INSERT INTO Ports VALUES ("DXCTE", 120.2345, 30.3333);

INSERT INTO Session(session_driverName, session_vehicleID, startTime, expireTime) VALUES ("admin", "11223", "1" , "2");
INSERT INTO Session(session_driverName, session_vehicleID, startTime, expireTime) VALUES ("a", "00000", "1" , "2");
INSERT INTO Session(session_driverName, session_vehicleID, startTime, expireTime) VALUES ("b", "11111", "1" , "2");

INSERT INTO Gps(gps_sessionID, longitude, latitude, gps_time ) VALUES (1, "123.22", "22.44" , "3");
INSERT INTO Gps(gps_sessionID, longitude, latitude, gps_time ) VALUES (2, "159.45", "23.45" , "3");
INSERT INTO Gps(gps_sessionID, longitude, latitude, gps_time ) VALUES (3, "102.75", "76.38" , "3");

INSERT INTO Route (shift_normal_start_time, route_num) VALUES ("20140721150000", "1");
INSERT INTO Route (shift_normal_start_time, route_num) VALUES ("20140719142500", "1");
INSERT INTO Route (shift_normal_start_time, route_num) VALUES ("20140719135700", "2");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000004", "BLCTZS", "BLCT3", "20140507075715", "20140808020000", "15", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020086", "BLCTMS", "BLCT3", "20140507184227", "20140808080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020089", "BLCTMS", "BLCT3", "20140507184227", "20140808080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020091", "BLCTMS", "BLCT3", "20140507184227", "20140808080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020094", "BLCTMS", "BLCT3", "20140507184227", "20140808080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020093", "BLCTMS", "BLCT3", "20140507184227", "20140808080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020088", "BLCTMS", "BLCT3", "20140507184227", "20140808080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020072", "BLCTZS", "BLCTYD", "20140507075715", "20140808120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000003", "BLCTZS", "BLCT", "20140507075715", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028412", "BLCTZS", "BLCT3", "20140507184227", "20140808120000", "8", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020125", "BLCT2", "BLCT3", "20140507165615", "20140808170000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020121", "BLCT2", "BLCT3", "20140507165615", "20140808180000", "3", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028418", "BLCTMS", "BLCT3", "20140507165615", "20140808180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020122", "BLCT2", "BLCT3", "20140507165615", "20140808190000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028431", "BLCTZS", "BLCT3", "20140508120848", "20140808220000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000002", "BLCTZS", "BLCT2", "20140507075715", "20140809020000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028402", "BLCTZS", "BLCT2", "20140507075715", "20140809020000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000001", "BLCTZS", "DXCTE", "20140507075715", "20140809080000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020085", "BLCTMS", "BLCTZS", "20140507184227", "20140809080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020101", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020100", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020099", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020098", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020097", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020096", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020095", "BLCTYD", "BLCTZS", "20140507184227", "20140809080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020090", "BLCTMS", "BLCTZS", "20140507184227", "20140809080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000006", "BLCTZS", "BLCT", "20140508120848", "20140809080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028406", "BLCTZS", "BLCT2", "20140507075715", "20140809090000", "13", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028419", "BLCTZS", "BLCT3", "20140507165615", "20140809090000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028447", "BLCTZS", "BLCT", "20140508160110", "20140809120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028448", "BLCTZS", "BLCT2", "20140508160110", "20140809120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020069", "BLCTYD", "BLCTMS", "20140507075715", "20140809180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020115", "BLCTYD", "BLCT2", "20140508160110", "20140809180000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028436", "BLCTZS", "BLCT2", "20140509052403", "20140809180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020149", "BLCT2", "BLCT", "20140509052403", "20140809180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028433", "BLCTZS", "BLCT2", "20140509052403", "20140809180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028440", "BLCTZS", "BLCT", "20140509052403", "20140809180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028439", "BLCTZS", "BLCT", "20140509052403", "20140809180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028407", "BLCTZS", "BLCT2", "20140507075715", "20140809200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020074", "BLCTYD", "BLCTMS", "20140507075715", "20140809200000", "0", "32
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028443", "BLCTZS", "BLCT2", "20140508160110", "20140809200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028462", "BLCT2", "BLCTZS", "20140509163129", "20140809200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020150", "BLCT2", "BLCT", "20140509163129", "20140809200000", "1", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020170", "BLCTYD", "BLCTZS", "20140509163129", "20140809230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12010368", "BLCTMS", "BLCT2", "20140507125817", "20140810050000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020105", "BLCTMS", "BLCT3", "20140507184227", "20140810080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020104", "BLCTMS", "BLCT3", "20140507184227", "20140810080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020103", "BLCTMS", "BLCT3", "20140507184227", "20140810080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020092", "BLCTMS", "BLCTYD", "20140507184227", "20140810080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020134", "BLCTZS", "BLCT3", "20140509163129", "20140810080000", "5", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020142", "BLCTYD", "BLCT", "20140509163129", "20140810080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020160", "BLCTZS", "BLCTYD", "20140509163129", "20140810080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028465", "BLCT", "BLCTMS", "20140509163129", "20140810080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020153", "BLCT2", "BLCTZS", "20140509104824", "20140810100000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020154", "BLCT2", "BLCTYD", "20140509104824", "20140810100000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020148", "BLCT2", "BLCTZS", "20140509104824", "20140810100000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000010", "BLCTZS", "BLCT", "20140509135550", "20140810110000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020135", "BLCTYD", "BLCT", "20140508160110", "20140810113000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020110", "BLCTMS", "BLCT2", "20140507125817", "20140810120000", "0", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020140","BLCTMS", "BLCT", "20140807165615", "20120210120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020084", "BLCTZS", "BLCT2", "20140507165615", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028446", "BLCTZS", "BLCT", "20140508160110", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020131", "BLCTYD", "BLCT", "20140508160110", "20140810120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028459", "BLCT2", "BLCTYD", "20140509104824", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028464", "BLCT3", "BLCT2", "20140509104824", "20140810120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028466", "BLCT", "BLCT2", "20140509163129", "20140810120000", "2", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028467", "BLCTZS", "BLCT", "20140509163129", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020175", "BLCT", "BLCT2", "20140509163129", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020119", "BLCTMS", "BLCT3", "20140507165615", "20140810130000", "2", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020164", "BLCTMS", "BLCT3", "20140509163129", "20140810130000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020163", "BLCTMS", "BLCT3", "20140509163129", "20140810130000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028456", "BLCT", "BLCT3", "20140509163129", "20140810150000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028484", "BLCT", "BLCT2", "20140510172914", "20140810150000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028415", "BLCTZS", "BLCT2", "20140507184227", "20140810160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028475", "BLCTZS", "BLCT2", "20140510172914", "20140810160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020130", "BLCTYD", "BLCT", "20140508160110", "20140810180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020129", "BLCTYD", "BLCT", "20140508160110", "20140810180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028441", "BLCTZS", "BLCT", "20140509052403", "20140810180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028438", "BLCTZS", "BLCT", "20140509052403", "20140810180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020171", "BLCT3", "BLCT", "20140509163129", "20140810180000", "1", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020162", "BLCTMS", "BLCT3", "20140509163129", "20140810180000", "4", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020137", "BLCTZS", "BLCT", "20140509163129", "20140810180000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020117", "BLCT2", "BLCT3", "20140507165615", "20140810200000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020145", "BLCT2", "BLCT3", "20140509163129", "20140810200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028469", "BLCTZS", "BLCT", "20140510131548", "20140810200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028473", "BLCTZS", "BLCT2", "20140510131548", "20140810200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028472", "BLCTZS", "BLCT2", "20140510131548", "20140810200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028445", "BLCTZS", "BLCT2", "20140508160110", "20140810220000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028495", "BLCTZS", "BLCT2", "20140510172914", "20140810230000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020079", "BLCTZS", "BLCT", "20140507075715", "20140811010000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020174", "BLCTMS", "BLCT3", "20140509163129", "20140811080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000014", "BLCTZS", "BLCT2", "20140509163129", "20140811080000", "15", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020176", "BLCT3", "BLCT2", "20140509163129", "20140811080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020225", "BLCTZS", "BLCTYD", "20140510172914", "20140811080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020159", "BLCTZS", "BLCTYD", "20140510172914", "20140811080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028508", "BLCTMS", "BLCT3", "20140510172914", "20140811090000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020156", "BLCTMS", "BLCTYD", "20140509163129", "20140811100000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028509", "BLCT2", "BLCT", "20140510172914", "20140811110000", "20", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028480", "BLCT", "BLCT3", "20140510172914", "20140811120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028483", "BLCTYD", "BLCTZS", "20140510172914", "20140811120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028485", "BLCTYD", "BLCTZS", "20140510172914", "20140811120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028486", "BLCTYD", "BLCTZS", "20140510172914", "20140811120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028502", "BLCT2", "BLCTZS", "20140510172914", "20140811120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028493", "BLCTZS", "BLCT3", "20140510172914", "20140811140000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000011", "BLCT3", "ZHCT", "20140509150555", "20140811150000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020108", "BLCTMS", "BLCT2", "20140507125817", "20140811160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020114", "BLCTZS", "BLCT2", "20140507184227", "20140811160000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020111", "BLCTZS", "BLCT2", "20140507184227", "20140811160000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028411", "BLCTZS", "BLCT2", "20140507165615", "20140811180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020132", "BLCTYD", "BLCT", "20140508160110", "20140811180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028479", "BLCT3", "BLCTMS", "20140510131548", "20140811180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028503", "BLCT2", "BLCT", "20140510172914", "20140811180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028490", "BLCTYD", "BLCTZS", "20140510172914", "20140811180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028489", "BLCTYD", "BLCTZS", "20140510172914", "20140811180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028482", "BLCTMS", "BLCT2", "20140510172914", "20140811180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028492", "BLCTMS", "BLCTZS", "20140510172914", "20140811180000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028506", "BLCT2", "BLCT", "20140510172914", "20140811180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020172", "BLCT", "BLCTZS", "20140509163129", "20140811200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020179", "BLCT", "BLCT2", "20140509163129", "20140811200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020182", "BLCT", "BLCT2", "20140509163129", "20140811200000", "7", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020181", "BLCT3", "BLCT2", "20140509163129", "20140811200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028470", "BLCTZS", "BLCT2", "20140510131548", "20140811200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020191", "BLCTZS", "BLCTYD", "20140510131548", "20140811200000", "1", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020206", "BLCT2", "BLCT3", "20140510172914", "20140811200000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020235", "BLCTZS", "BLCT3", "20140511172227", "20140811200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020218", "BLCT2", "BLCT3", "20140510172914", "20140811220000", "4", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028499", "BLCT2", "BLCT3", "20140510172914", "20140811220000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028481", "BLCT", "BLCT3", "20140510172914", "20140811230000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000013", "BLCTZS", "BLCT", "20140509163129", "20140812080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000012", "BLCTZS", "BLCT", "20140509163129", "20140812080000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000015", "BLCT2", "BLCT", "20140510152415", "20140812080000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028504", "BLCT2", "BLCT", "20140510172914", "20140812080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028507", "BLCT2", "BLCT", "20140510172914", "20140812080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028505", "BLCT2", "BLCT", "20140510172914", "20140812080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020201", "BLCTZS", "BLCTYD", "20140510172914", "20140812090000", "0", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000016", "BLCTZS", "BLCTYD", "20140510152415", "20140812120000", "20", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028512", "BLCTZS", "BLCTYD", "20140511172227", "20140812120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028521", "BLCTZS", "BLCT2", "20140511172227", "20140812140000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028454", "BLCT2", "BLCT3", "20140509163129", "20140812150000", "0", "40
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020223", "BLCT2", "BLCTZS", "20140510172914", "20140812160000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020224", "BLCT2", "BLCTZS", "20140510172914", "20140812160000", "3", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028437", "BLCTZS", "BLCT", "20140509052403", "20140812180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028457", "BLCT2", "BLCT", "20140509163129", "20140812180000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028524", "BLCTZS", "BLCT2", "20140511172227", "20140812180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020173", "BLCTYD", "BLCTZS", "20140509163129", "20140812190000", "0", "20
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020237", "BLCTYD", "BLCTMS", "20140511172227", "20140812190000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028460", "BLCT", "BLCTYD", "20140509163129", "20140812200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020143", "BLCTMS", "BLCTZS", "20140509163129", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020197", "BLCTYD", "BLCTZS", "20140510172914", "20140812200000", "8", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020196", "BLCTYD", "BLCTZS", "20140510172914", "20140812200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020198", "BLCTYD", "BLCTZS", "20140510172914", "20140812200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028478", "BLCT3", "BLCTZS", "20140510172914", "20140812200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028477", "BLCT3", "BLCTZS", "20140510172914", "20140812200000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020199", "BLCTYD", "BLCTZS", "20140510172914", "20140812200000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028497", "BLCT2", "BLCTYD", "20140510172914", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028498", "BLCT2", "BLCTYD", "20140510172914", "20140812200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020217", "BLCTMS", "BLCTZS", "20140510172914", "20140812200000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020195", "BLCTMS", "BLCTZS", "20140510172914", "20140812200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020202", "BLCTMS", "BLCTZS", "20140510172914", "20140812200000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020203", "BLCTMS", "BLCTZS", "20140510172914", "20140812200000", "26", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020204", "BLCTMS", "BLCTZS", "20140510172914", "20140812200000", "8", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020207", "BLCTMS", "BLCTZS", "20140510172914", "20140812200000", "6", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028510", "BLCT3", "BLCTZS", "20140511172227", "20140812200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028517", "BLCTZS", "BLCTYD", "20140511172227", "20140812200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030241", "BLCT", "BLCT3", "20140512114813", "20140812200000", "10", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028428", "BLCTZS", "BLCT2", "20140509163129", "20140812220000", "24", "67
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020144", "BLCTMS", "BLCT2", "20140510172914", "20140812220000", "88", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020157", "BLCTMS", "BLCTYD", "20140509163129", "20140813080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020155", "BLCTMS", "BLCTYD", "20140509163129", "20140813080000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028461", "BLCT", "BLCTZS", "20140509163129", "20140813120000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028455", "BLCT", "BLCTYD", "20140509163129", "20140813120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000088", "BLCTZS", "BLCT2", "20140512101542", "20140813120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000089", "BLCTZS", "BLCT3", "20140512101542", "20140813120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020228", "BLCT", "BLCT2", "20140512150141", "20140813120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020166", "BLCTMS", "BLCT2", "20140510172914", "20140813160000", "6", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028526", "BLCTZS", "BLCT2", "20140512150141", "20140813160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020259", "BLCT", "BLCTYD", "20140514162919", "20140813160000", "18", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028538", "BLCTYD", "BLCTZS", "20140513111220", "20140813170000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028463", "BLCT2", "BLCT", "20140509163129", "20140813180000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000017", "BLCTZS", "BLCT3", "20140510152415", "20140813180000", "30", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020208", "BLCTZS", "BLCT2", "20140510172914", "20140813180000", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028520", "BLCT2", "BLCTYD", "20140511172227", "20140813180000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020220", "BLCT3", "BLCT", "20140510172914", "20140813200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028494", "BLCTZS", "BLCT2", "20140510172914", "20140813200000", "13", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028523", "BLCTZS", "BLCT2", "20140511172227", "20140813200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028519", "BLCT2", "BLCTYD", "20140511172227", "20140813200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028511", "BLCT2", "BLCTYD", "20140511172227", "20140813200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020212", "BLCT2", "BLCT", "20140513111220", "20140813200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020209", "BLCT", "BLCTYD", "20140513121101", "20140813220000", "0", "36
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020186", "BLCT2", "BLCTYD", "20140513111220", "20140813230000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020213", "BLCT2", "BLCTYD", "20140513111220", "20140813230000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020244", "BLCT2", "BLCTYD", "20140513165309", "20140813230000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028540", "BLCT2", "BLCTYD", "20140513165309", "20140813230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028542", "BLCT2", "BLCTYD", "20140513165309", "20140813230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020246", "BLCTYD", "BLCTMS", "20140513165309", "20140814000000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020245", "BLCTMS", "BLCTZS", "20140513165309", "20140814020000", "0", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020260", "BLCTZS", "BLCTYD", "20140513165309", "20140814030000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028528", "BLCT", "BLCT2", "20140513165309", "20140814060000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028529", "BLCT", "BLCT2", "20140513165309", "20140814060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028530", "BLCT", "BLCT2", "20140513165309", "20140814060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028531", "BLCT", "BLCT2", "20140513165309", "20140814060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028532", "BLCT", "BLCT2", "20140513165309", "20140814060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028546", "BLCT", "BLCT2", "20140513165309", "20140814060000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020257", "BLCTZS", "BLCTYD", "20140513165309", "20140814080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020185", "BLCT2", "BLCTMS", "20140510131548", "20140814120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020180", "BLCT", "BLCT2", "20140512150141", "20140814120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020165", "BLCTYD", "BLCT2", "20140512150141", "20140814120000", "0", "32
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020230", "BLCT", "BLCT2", "20140512150141", "20140814120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028515", "BLCTZS", "BLCT3", "20140511172227", "20140814140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028513", "BLCTZS", "BLCT2", "20140511172227", "20140814140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028518", "BLCT2", "BLCT", "20140511172227", "20140814140000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028527", "BLCTZS", "BLCT2", "20140512150141", "20140814160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028533", "BLCT3", "BLCT2", "20140513165309", "20140814160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020255", "BLCT", "BLCT2", "20140513165309", "20140814160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028534", "BLCT3", "BLCT2", "20140513165309", "20140814160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020169", "BLCTMS", "BLCT2", "20140513172047", "20140814160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028458", "BLCT2", "BLCT", "20140509163129", "20140814180000", "0", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028468", "BLCTZS", "BLCT", "20140510131548", "20140814180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028514", "BLCT3", "BLCTMS", "20140511172227", "20140814180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020178", "BLCTMS", "BLCT2", "20140509163129", "20140814200000", "8", "37
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020177", "BLCTMS", "BLCT2", "20140509163129", "20140814200000", "15", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020183", "BLCT3", "BLCTZS", "20140509163129", "20140814200000", "8", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028471", "BLCTZS", "BLCT", "20140510131548", "20140814200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020219", "BLCT3", "BLCT2", "20140510172914", "20140814200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028537", "BLCT2", "BLCTYD", "20140513111220", "20140814200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028544", "BLCT3", "BLCT2", "20140513165309", "20140814200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020243", "BLCT2", "BLCT3", "20140513165309", "20140814200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028543", "BLCTZS", "BLCT2", "20140513165309", "20140814200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028564", "BLCTYD", "BLCT", "20140514162919", "20140814210000", "0", "-1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020233", "BLCTZS", "BLCTMS", "20140514162919", "20140814220000", "0", "17
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020222", "BLCTMS", "BLCT", "20140513153658", "20140815020000", "29", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020262", "BLCTMS", "BLCT3", "20140514162919", "20140815080000", "16", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020261", "BLCTMS", "BLCT3", "20140514162919", "20140815080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内码0000018", "BLCTZS", "DXCTE", "20140513111220", "20140815100000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020254", "BLCTMS", "BLCT2", "20140513165309", "20140815100000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000019", "BLCT2", "ZHCT", "20140513165309", "20140815110000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000021", "DXCTE", "BLCT2", "20140514120638", "20140815110000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028555", "BLCT", "BLCT2", "20140514161611", "20140815113000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020184", "BLCT2", "BLCT3", "20140512150141", "20140815120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000020", "BLCTZS", "DXCTE", "20140513165309", "20140815120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028560", "BLCT", "BLCT2", "20140514161611", "20140815120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028573", "BLCT", "BLCT2", "20140514161611", "20140815120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028568", "BLCTYD", "BLCT", "20140514162919", "20140815120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028566", "BLCTYD", "BLCT", "20140514162919", "20140815120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028574", "BLCTYD", "BLCT2", "20140514162919", "20140815120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028561", "BLCT2", "BLCTZS", "20140514162919", "20140815120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028550", "BLCTYD", "BLCTMS", "20140514162919", "20140815120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020295", "BLCT", "BLCT2", "20140515171035", "20140815120000", "1", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028522", "BLCTZS", "BLCT2", "20140511172227", "20140815140000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020127", "BLCTMS", "BLCT2", "20140510172914", "20140815160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020126", "BLCTMS", "BLCT", "20140510172914", "20140815160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020128", "BLCTMS", "BLCT2", "20140510172914", "20140815160000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020270", "BLCT2", "BLCT3", "20140514120638", "20140815160000", "12", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028556", "BLCT", "BLCT2", "20140514161611", "20140815160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028557", "BLCT3", "BLCT2", "20140514162919", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028558", "BLCTYD", "BLCT2", "20140514162919", "20140815160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028559", "BLCTYD", "BLCT2", "20140514162919", "20140815160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028562", "BLCT2", "BLCT3", "20140514162919", "20140815160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020189", "BLCTYD", "BLCT2", "20140514162919", "20140815160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020234", "BLCTZS", "BLCT", "20140514162919", "20140815160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028501", "BLCTZS", "BLCT2", "20140510172914", "20140815170000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020269", "BLCT2", "BLCT3", "20140514120638", "20140815170000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020280", "BLCTMS", "BLCT", "20140514220844", "20140815174500", "49", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020273", "BLCTYD", "BLCT2", "20140514162919", "20140815175000", "14", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020256", "BLCT", "BLCT2", "20140513165309", "20140815180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020249", "BLCT2", "BLCT3", "20140513165309", "20140815180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028572", "BLCTYD", "BLCT2", "20140514162919", "20140815180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020286", "BLCT2", "BLCT3", "20140514162919", "20140815180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020221", "BLCT3", "BLCT2", "20140510172914", "20140815200000", "26", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020242", "BLCT", "BLCT2", "20140513165309", "20140815200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028541", "BLCTZS", "BLCT2", "20140513165309", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028549", "BLCT", "BLCT2", "20140514162919", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020275", "BLCTYD", "BLCTZS", "20140514162919", "20140815200000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020285", "BLCT2", "BLCT3", "20140514162919", "20140815200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020251", "BLCTMS", "BLCT", "20140513153658", "20140815220000", "50", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020253", "BLCTYD", "BLCTMS", "20140513165309", "20140815230000", "0", "44
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028570", "BLCT2", "BLCT", "20140514152606", "20140815230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028571", "BLCT2", "BLCT", "20140514152606", "20140815230000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020282", "BLCTZS", "BLCT2", "20140515105814", "20140815230000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("Z12020001", "ZHCT", "BLCTYD", "20140515105814", "20140815230000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000022", "ZHCT", "BLCT2", "20140514220844", "20140815233000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000024", "ZHCT", "BLCT2", "20140514221650", "20140815233000", "36", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020264", "BLCTMS", "BLCT3", "20140514162919", "20140816080000", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000026", "BLCT", "BLCT2", "20140515074513", "20140816100000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000029", "ZHCT", "BLCT", "20140515102231", "20140816100000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020229", "BLCT", "BLCT2", "20140512150141", "20140816120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020158", "BLCT2", "BLCT", "20140512150141", "20140816120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020226", "BLCT2", "BLCT", "20140512150141", "20140816120000", "40", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020187", "BLCT2", "BLCT", "20140513111220", "20140816120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028536", "BLCTZS", "BLCT2", "20140513111220", "20140816120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028539", "BLCT3", "BLCT2", "20140513165309", "20140816120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020227", "BLCT2", "BLCT", "20140513172047", "20140816120000", "9", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000028", "BLCT", "ZHCT", "20140515090308", "20140816120000", "22", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020290", "BLCT", "BLCTMS", "20140515105814", "20140816120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020289", "BLCT2", "BLCT", "20140515105814", "20140816120000", "0", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020292", "BLCTYD", "BLCTZS", "20140515171035", "20140816120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028576", "BLCTYD", "BLCTZS", "20140515171035", "20140816120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020294", "BLCTYD", "BLCTZS", "20140515171035", "20140816120000", "7", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020293", "BLCTYD", "BLCTZS", "20140515171035", "20140816120000", "4", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028525", "BLCTZS", "BLCT2", "20140512150141", "20140816160000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020192", "BLCT", "BLCT2", "20140513111220", "20140816160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028553", "BLCT2", "BLCT", "20140514162919", "20140816160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028551", "BLCT3", "BLCT", "20140514162919", "20140816160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028586", "BLCT2", "BLCT", "20140515171035", "20140816160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028587", "BLCT2", "BLCT", "20140515171035", "20140816160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020278", "BLCTYD", "BLCTMS", "20140514181257", "20140816180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020297", "BLCTZS", "BLCTMS", "20140515171035", "20140816180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028597", "BLCT", "BLCT2", "20140516145634", "20140816180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020277", "BLCTZS", "BLCTMS", "20140516164710", "20140816180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020276", "BLCT2", "BLCT", "20140514162919", "20140816200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028547", "BLCTZS", "BLCT2", "20140514220844", "20140816200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028611", "BLCT", "BLCT2", "20140516145634", "20140816200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028602", "BLCTYD", "BLCT2", "20140516164710", "20140816200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028620", "BLCT2", "BLCT", "20140516145634", "20140817040000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028584", "BLCT2", "BLCT", "20140515171035", "20140817060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028582", "BLCT2", "BLCT", "20140515171035", "20140817060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028607", "BLCT", "BLCT2", "20140516145634", "20140817060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028608", "BLCT", "BLCT2", "20140516145634", "20140817060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028612", "BLCTZS", "BLCT2", "20140516145634", "20140817070000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020263", "BLCTMS", "BLCTZS", "20140514162919", "20140817080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020331", "BLCT", "BLCT3", "20140516164710", "20140817080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020284", "BLCTYD", "BLCT", "20140514162919", "20140817120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000023", "BLCT2", "ZHCT", "20140514162919", "20140817120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028548", "BLCTMS", "BLCT2", "20140514220844", "20140817120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000025", "BLCTZS", "BLCT", "20140515074513", "20140817120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020300", "BLCTZS", "BLCT", "20140515171035", "20140817120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028577", "BLCT", "BLCT2", "20140515171035", "20140817120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028578", "BLCT", "BLCT2", "20140515171035", "20140817120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028579", "BLCT", "BLCT2", "20140515171035", "20140817120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028580", "BLCT", "BLCT2", "20140515171035", "20140817120000", "0", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020344", "BLCT", "BLCT2", "20140516164710", "20140817120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028619", "BLCT2", "BLCTZS", "20140516145634", "20140817150000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028613", "BLCT3", "BLCTMS", "20140516164710", "20140817160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020141", "BLCTZS", "BLCT", "20140515171035", "20140817180000", "2", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020304", "BLCT3", "BLCT", "20140515171035", "20140817180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020307", "BLCTZS", "BLCT", "20140515171035", "20140817180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020306", "BLCTZS", "BLCT3", "20140515171035", "20140817180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028625", "BLCT", "BLCTMS", "20140516145634", "20140817180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028629", "BLCT2", "BLCT", "20140516164710", "20140817180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020231", "BLCT", "BLCTMS", "20140512150141", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020216", "BLCT2", "BLCTMS", "20140513111220", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020188", "BLCT2", "BLCTYD", "20140513111220", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020239", "BLCT", "BLCTMS", "20140513111220", "20140817200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028535", "BLCTZS", "BLCT2", "20140513111220", "20140817200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020211", "BLCT2", "BLCT", "20140513111220", "20140817200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020247", "BLCT2", "BLCTMS", "20140513111220", "20140817200000", "3", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020250", "BLCTYD", "BLCTZS", "20140513165309", "20140817200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020236", "BLCT", "BLCT2", "20140514162919", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028554", "BLCTZS", "BLCT2", "20140514162919", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020167", "BLCTMS", "BLCT2", "20140510172914", "20140818040000", "4", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020316", "BLCTYD", "BLCTZS", "20140515171035", "20140818040000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020342", "BLCTZS", "BLCT", "20140516145634", "20140818040000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028575", "BLCTYD", "BLCT2", "20140515105814", "20140818120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028583", "BLCT2", "BLCT", "20140515171035", "20140818120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020248", "BLCT2", "BLCTMS", "20140513111220", "20140818160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020240", "BLCT3", "BLCTZS", "20140513111220", "20140818160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020238", "BLCT", "BLCTMS", "20140513111220", "20140818160000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020348", "BLCT", "BLCT3", "20140516164710", "20140818160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028614", "BLCT2", "BLCTYD", "20140516164710", "20140818160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028616", "BLCTZS", "BLCT2", "20140516145634", "20140818170000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020339", "BLCT", "BLCTYD", "20140516164710", "20140818180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020274", "BLCTYD", "BLCTZS", "20140514162919", "20140818200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020340", "BLCT", "BLCTYD", "20140516145634", "20140818200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028598", "BLCT", "BLCTYD", "20140516145634", "20140818200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020214", "BLCT2", "BLCTYD", "20140513111220", "20140819120000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020341", "BLCTZS", "BLCTMS", "20140516145634", "20140819120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028617", "BLCT2", "BLCTYD", "20140516145634", "20140819140000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020193", "BLCT", "BLCTZS", "20140513111220", "20140819160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020302", "BLCT", "BLCTZS", "20140515171035", "20140819160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028581", "BLCT2", "BLCT3", "20140515171035", "20140819160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028615", "BLCT2", "BLCTZS", "20140516164710", "20140819160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028599", "BLCT", "BLCTZS", "20140516145634", "20140820120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028589", "BLCT", "BLCT3", "20140516164710", "20140820120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020435", "BLCTMS", "BLCT", "20140524192025", "20140822120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020317", "BLCTMS", "BLCT2", "20140524192025", "20140823120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020406", "BLCTMS", "BLCT", "20140524192025", "20140823200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020411", "BLCTMS", "BLCT3", "20140524192025", "20140824080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020412", "BLCTMS", "BLCT3", "20140524192025", "20140824080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020414", "BLCTMS", "BLCT3", "20140524192025", "20140824180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020502", "BLCTMS", "BLCT2", "20140524183327", "20140824200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020516", "BLCTMS", "BLCTZS", "20140524183327", "20140824200000", "22", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020474", "BLCTMS", "BLCT2", "20140524183327", "20140824200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020373", "BLCTZS", "BLCT2", "20140524192025", "20140824200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020466", "BLCTMS", "BLCT3", "20140524192025", "20140825120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020389", "BLCTMS", "BLCT2", "20140524192025", "20140825120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028806", "BLCTMS", "BLCTZS", "20140524183327", "20140825160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028760", "BLCT2", "BLCTZS", "20140524192025", "20140825170000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028802", "BLCT2", "BLCT3", "20140524183327", "20140825200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000066", "DXCTE", "BLCTZS", "20140524183327", "20140826120000", "28", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028803", "BLCTMS", "BLCT3", "20140524183327", "20140826160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000068", "BLCTZS", "DXCTE", "20140524183327", "20140826180000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020472", "BLCTYD", "BLCT2", "20140527050655", "20140826180000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020470", "BLCTYD", "BLCT2", "20140527050655", "20140826180000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028847", "BLCT3", "BLCT", "20140527170413", "20140826180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000073", "BLCT2", "DXCTE", "20140527050655", "20140827080000", "6", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020446", "BLCT2", "BLCT", "20140527072551", "20140827100000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028837", "BLCT2", "BLCT3", "20140527050655", "20140827120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020456", "BLCT2", "BLCTYD", "20140527050655", "20140827120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028839", "BLCT2", "BLCTZS", "20140527103858", "20140827120000", "15", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000069", "BLCT2", "ZHCT", "20140524183327", "20140827160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028838", "BLCT2", "BLCTZS", "20140527170413", "20140827180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020513", "BLCTMS", "BLCTZS", "20140524183327", "20140827200000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020514", "BLCTMS", "BLCTZS", "20140524183327", "20140827200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020560", "BLCTMS", "BLCTZS", "20140527160849", "20140827200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028854", "BLCT2", "BLCTYD", "20140527170413", "20140827200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028844", "BLCT2", "BLCT", "20140527160849", "20140827230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020503", "BLCT2", "BLCTMS", "20140528182110", "20140828010000", "22", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020506", "BLCT2", "BLCTMS", "20140528182110", "20140828010000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020588", "BLCT2", "BLCT3", "20140528182110", "20140828020000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020487", "BLCT", "BLCTMS", "20140528182110", "20140828020000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020542", "BLCT", "BLCTYD", "20140528182110", "20140828030000", "35", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020578", "BLCTMS", "BLCT", "20140528182110", "20140828030000", "26", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020579", "BLCTMS", "BLCT", "20140528182110", "20140828030000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020575", "BLCTMS", "BLCT", "20140528182110", "20140828030000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028856", "BLCT3", "BLCT", "20140527170413", "20140828060000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020565", "BLCTZS", "BLCT2", "20140527170413", "20140828070000", "4", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000072", "BLCT2", "BLCT", "20140527061331", "20140828080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020539", "BLCT", "BLCTMS", "20140527170413", "20140828080000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020543", "BLCTZS", "BLCTYD", "20140527170413", "20140828080000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020493", "BLCTMS", "BLCT3", "20140524183327", "20140828100000", "3", "26
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000061", "BLCT2", "ZHCT", "20140527050655", "20140828120000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028793", "BLCTZS", "BLCT2", "20140527050655", "20140828120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028834", "BLCT3", "BLCT2", "20140527050655", "20140828120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020598", "BLCT2", "BLCT", "20140528182110", "20140828120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028824", "BLCT3", "BLCT2", "20140527050655", "20140828140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020547", "BLCTYD", "BLCTMS", "20140527160849", "20140828160000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028855", "BLCT2", "BLCT", "20140527170413", "20140828160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028842", "BLCT2", "BLCT3", "20140527170413", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028840", "BLCT3", "BLCTMS", "20140527170413", "20140828160000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020504", "BLCT2", "BLCT", "20140528182110", "20140828160000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020494", "BLCTMS", "BLCT3", "20140524183327", "20140828180000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028794", "BLCTZS", "BLCT", "20140527050655", "20140828180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028846", "BLCT3", "BLCT", "20140527170413", "20140828180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028841", "BLCTZS", "BLCT3", "20140527170413", "20140828180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020540", "BLCTYD", "BLCTMS", "20140527170413", "20140828180000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020405", "BLCTMS", "BLCT2", "20140524192025", "20140828200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020567", "BLCT", "BLCTYD", "20140527170413", "20140828200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028878", "BLCT2", "BLCT3", "20140528182110", "20140828200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028889", "BLCTZS", "BLCT3", "20140528182110", "20140828200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028887", "BLCT2", "BLCTYD", "20140528182110", "20140829070000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020501", "BLCTYD", "BLCT2", "20140527050655", "20140829080000", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028868", "BLCT3", "BLCT2", "20140528182110", "20140829080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028863", "BLCT3", "BLCT2", "20140528182110", "20140829080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020558", "BLCTMS", "BLCT3", "20140528182110", "20140829080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020562", "BLCTMS", "BLCT3", "20140528182110", "20140829080000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020563", "BLCTMS", "BLCT3", "20140528182110", "20140829080000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020570", "BLCTMS", "BLCT3", "20140528182110", "20140829080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020557", "BLCTMS", "BLCT3", "20140528182110", "20140829080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020573", "BLCT", "BLCT3", "20140528182110", "20140829080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020646", "BLCTZS", "BLCTYD", "20140501173231", "20140829080000", "4", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028880", "BLCT2", "BLCTZS", "20140528182110", "20140829100000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028885", "BLCT2", "BLCT", "20140528182110", "20140829110000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028886", "BLCT2", "BLCT", "20140528182110", "20140829110000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020538", "BLCTMS", "BLCT2", "20140527103858", "20140829120000", "111", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028853", "BLCT3", "BLCT2", "20140527160849", "20140829120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028866", "BLCTZS", "BLCT3", "20140528182110", "20140829120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028865", "BLCTZS", "BLCT3", "20140528182110", "20140829120000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020584", "BLCT2", "BLCT3", "20140528182110", "20140829120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028869", "BLCTZS", "BLCT3", "20140528182110", "20140829120000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020582", "BLCTMS", "BLCT3", "20140528182110", "20140829120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020583", "BLCTMS", "BLCT3", "20140528182110", "20140829120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020580", "BLCTMS", "BLCT", "20140528182110", "20140829120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020581", "BLCTMS", "BLCT", "20140528182110", "20140829120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020550", "BLCTYD", "BLCTMS", "20140528182110", "20140829120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028870", "BLCTYD", "BLCT2", "20140528182110", "20140829120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028873", "BLCTYD", "BLCT2", "20140528182110", "20140829120000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028872", "BLCTYD", "BLCT2", "20140528182110", "20140829120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028858", "BLCT3", "BLCTMS", "20140528182110", "20140829120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028876", "BLCT", "BLCT2", "20140528182110", "20140829120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028859", "BLCT", "BLCT2", "20140528182110", "20140829120000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020544", "BLCT3", "BLCT", "20140527160849", "20140829130000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028825", "BLCT3", "BLCT2", "20140527050655", "20140829140000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028888", "BLCT2", "BLCTYD", "20140528182110", "20140829140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028890", "BLCTZS", "BLCTYD", "20140528182110", "20140829140000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028871", "BLCTMS", "BLCT2", "20140528215843", "20140829140000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000090", "BLCT3", "BLCT2", "20140528232544", "20140829150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028922", "BLCTZS", "BLCTMS", "20140501173231", "20140829150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020488", "BLCT", "BLCTMS", "20140528182110", "20140829160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028881", "BLCT2", "BLCT", "20140528182110", "20140829160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020524", "BLCT2", "BLCT", "20140528182110", "20140829160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028877", "BLCT2", "BLCT", "20140528182110", "20140829160000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028875", "BLCTZS", "BLCT3", "20140528182110", "20140829160000", "6", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020569", "BLCTMS", "BLCT2", "20140528182110", "20140829160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028835", "BLCTYD", "BLCT2", "20140527050655", "20140829180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020548", "BLCT2", "BLCT3", "20140527170413", "20140829180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020546", "BLCT", "BLCT2", "20140527170413", "20140829180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020545", "BLCT", "BLCT2", "20140527170413", "20140829180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020564", "BLCT2", "BLCT3", "20140527170413", "20140829180000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020566", "BLCTYD", "BLCT2", "20140527170413", "20140829180000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020555", "BLCTMS", "BLCT3", "20140528182110", "20140829180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028891", "BLCT2", "BLCTYD", "20140529104705", "20140829180000", "26", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000093", "BLCTZS", "BLCTYD", "20140529170334", "20140829180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028879", "BLCT2", "BLCT3", "20140528182110", "20140829200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000096", "BLCTZS", "BLCTMS", "20140501054857", "20140829200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028843", "BLCT3", "BLCT2", "20140527160849", "20140829230000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020601", "BLCTYD", "BLCTZS", "20140529170334", "20140801000000", "7", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028861", "BLCT", "BLCTZS", "20140528182110", "20140801080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020571", "BLCTZS", "BLCT2", "20140528182110", "20140801080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028864", "BLCT3", "BLCT", "20140528182110", "20140801080000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028867", "BLCT3", "BLCT2", "20140528182110", "20140801080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020602", "BLCTYD", "BLCTZS", "20140529170334", "20140801080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020603", "BLCTYD", "BLCTZS", "20140529170334", "20140801080000", "4", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020610", "BLCTYD", "BLCTZS", "20140529170334", "20140801080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028911", "BLCT", "BLCT2", "20140529170334", "20140801100000", "0", "27
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028821", "BLCTZS", "BLCT", "20140527050655", "20140801120000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028833", "BLCT2", "BLCT", "20140527061331", "20140801120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020527", "BLCT2", "BLCT", "20140527061331", "20140801120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000085", "BLCT2", "BLCT", "20140527160849", "20140801120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028850", "BLCTYD", "BLCT2", "20140527160849", "20140801120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028848", "BLCTYD", "BLCT", "20140527160849", "20140801120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028852", "BLCT3", "BLCTMS", "20140527160849", "20140801120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020525", "BLCT2", "BLCT", "20140528182110", "20140801120000", "25", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000091", "BLCT2", "BLCT", "20140528232804", "20140801120000", "7", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000092", "BLCT2", "ZHCT", "20140529170334", "20140801120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028894", "BLCTMS", "BLCTYD", "20140529170334", "20140801120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020631", "BLCT2", "BLCT", "20140529170334", "20140801120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020594", "BLCT2", "BLCT", "20140529170334", "20140801120000", "3", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028893", "BLCTMS", "BLCTYD", "20140529170334", "20140801120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020577", "BLCT2", "BLCT3", "20140529170334", "20140801120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000095", "BLCTZS", "BLCT", "20140529170334", "20140801120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020553", "BLCT3", "BLCT2", "20140527160849", "20140801150000", "60", "33
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020489", "BLCTMS", "BLCT2", "20140524183327", "20140801160000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020508", "BLCTMS", "BLCT", "20140524183327", "20140801160000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020533", "BLCT3", "BLCT2", "20140527050655", "20140801160000", "0", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020541", "BLCTMS", "BLCT2", "20140527170413", "20140801160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020492", "BLCT", "BLCT2", "20140528182110", "20140801160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020505", "BLCT2", "BLCT", "20140528182110", "20140801160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028882", "BLCT2", "BLCT", "20140528182110", "20140801160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028884", "BLCT2", "BLCT", "20140528182110", "20140801160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028892", "BLCTZS", "BLCT", "20140529170334", "20140801160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020592", "BLCT3", "BLCT2", "20140529170334", "20140801160000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020597", "BLCT3", "BLCT", "20140528182110", "20140801180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020593", "BLCTMS", "BLCT3", "20140528182110", "20140801180000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020590", "BLCTYD", "BLCTZS", "20140528182110", "20140801180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020589", "BLCTYD", "BLCTZS", "20140528182110", "20140801180000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020596", "BLCTYD", "BLCT", "20140528182110", "20140801180000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020595", "BLCTYD", "BLCT", "20140528182110", "20140801180000", "6", "20
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020630", "BLCTYD", "BLCTZS", "20140529170334", "20140801180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028902", "BLCTZS", "BLCT", "20140529170334", "20140801180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028899", "BLCTZS", "BLCT", "20140529170334", "20140801180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028916", "BLCT", "BLCT2", "20140529170334", "20140801180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028901", "BLCTZS", "BLCT", "20140529170334", "20140801190000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020552", "BLCT2", "BLCTZS", "20140527170413", "20140801200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020549", "BLCT2", "BLCTZS", "20140527170413", "20140801200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020551", "BLCT2", "BLCTZS", "20140527170413", "20140801200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028851", "BLCT3", "BLCT", "20140527170413", "20140801200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020585", "BLCT2", "BLCT", "20140528182110", "20140801200000", "18", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020576", "BLCTYD", "BLCT2", "20140528182110", "20140801200000", "52", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028914", "BLCTZS", "BLCT2", "20140529170334", "20140801200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028912", "BLCT2", "BLCTZS", "20140529170334", "20140801200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028917", "BLCT", "BLCT2", "20140529170334", "20140801200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028915", "BLCTZS", "BLCT2", "20140529170334", "20140801200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028900", "BLCT2", "BLCTYD", "20140529170334", "20140801200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028925", "BLCT2", "BLCT", "20140501113552", "20140801200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028923", "BLCTZS", "BLCT2", "20140501113552", "20140801200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028924", "BLCTZS", "BLCT", "20140501113552", "20140801200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028939", "BLCT2", "BLCT", "20140501173231", "20140801200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028967", "BLCT2", "BLCT", "20140502131833", "20140801200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020586", "BLCT2", "BLCT", "20140528182110", "20140801210000", "62", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020609", "BLCTZS", "BLCT3", "20140529170334", "20140801220000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028934", "BLCTZS", "BLCTYD", "20140501113552", "20140801230000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020556", "BLCTMS", "BLCT3", "20140528182110", "20140802080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020607", "BLCT", "BLCT3", "20140529170334", "20140802080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000094", "BLCTZS", "BLCT", "20140529170334", "20140802080000", "7", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000099", "BLCTZS", "BLCT2", "20140501113552", "20140802080000", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028940", "BLCT2", "BLCT", "20140501173231", "20140802080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028941", "BLCT2", "BLCT", "20140501173231", "20140802080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028942", "BLCT2", "BLCT", "20140501173231", "20140802080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030011", "BLCT2", "BLCTZS", "20140501173231", "20140802080000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030010", "BLCT2", "BLCTZS", "20140501173231", "20140802080000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020611", "BLCTMS", "BLCT3", "20140501173231", "20140802080000", "3", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020608", "BLCTMS", "BLCTYD", "20140501173231", "20140802080000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000110", "BLCTZS", "BLCT2", "20140502174100", "20140802080000", "9", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028962", "BLCT2", "BLCT3", "20140501173231", "20140802100000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020530", "BLCTMS", "BLCT2", "20140527170413", "20140802120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020529", "BLCTMS", "BLCT2", "20140527170413", "20140802120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020531", "BLCTMS", "BLCT2", "20140528182110", "20140802120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020619", "BLCT", "BLCT3", "20140529170334", "20140802120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020621", "BLCT", "BLCTMS", "20140529170334", "20140802120000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028905", "BLCT2", "BLCTZS", "20140529170334", "20140802140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028918", "BLCT", "BLCT2", "20140529170334", "20140802140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028904", "BLCT2", "BLCT3", "20140529170334", "20140802140000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028919", "BLCTZS", "BLCT3", "20140529170334", "20140802150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028913", "BLCT2", "BLCT", "20140529170334", "20140802150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028883", "BLCT2", "BLCT", "20140528182110", "20140802160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020591", "BLCT3", "BLCT2", "20140529170334", "20140802160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020568", "BLCT", "BLCT2", "20140501173231", "20140802160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020599", "BLCT2", "BLCT", "20140502131833", "20140802160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000101", "BLCT2", "BLCT", "20140501113552", "20140802170000", "28", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028845", "BLCT3", "BLCT", "20140527170413", "20140802180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028849", "BLCT3", "BLCT", "20140527170413", "20140802180000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028874", "BLCT3", "BLCTMS", "20140528182110", "20140802180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020614", "BLCT3", "BLCT", "20140529170334", "20140802180000", "7", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020606", "BLCTZS", "BLCT3", "20140529170334", "20140802180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020605", "BLCTZS", "BLCT3", "20140529170334", "20140802180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020604", "BLCTZS", "BLCT3", "20140529170334", "20140802180000", "7", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020625", "BLCTYD", "BLCTZS", "20140529170334", "20140802180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020616", "BLCTYD", "BLCT", "20140529170334", "20140802180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020613", "BLCTYD", "BLCT", "20140529170334", "20140802180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028943", "BLCT2", "BLCT", "20140501173231", "20140802180000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028951", "BLCT", "BLCT2", "20140501173231", "20140802180000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030001", "BLCTZS", "BLCTYD", "20140501173231", "20140802180000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030002", "BLCTZS", "BLCTYD", "20140501173231", "20140802180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020600", "BLCT2", "BLCT", "20140502131833", "20140802180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030012", "BLCTMS", "BLCT2", "20140502174100", "20140802180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020587", "BLCTZS", "BLCT3", "20140528182110", "20140802200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020615", "BLCTZS", "BLCT", "20140529170334", "20140802200000", "2", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028948", "BLCT2", "BLCT3", "20140501173231", "20140802200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000105", "DXCTE", "BLCTZS", "20140501173231", "20140802200000", "9", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028952", "BLCT2", "BLCT3", "20140501173231", "20140802200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020645", "BLCTYD", "BLCTZS", "20140501173231", "20140802200000", "4", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028956", "BLCT3", "BLCTZS", "20140501173231", "20140802200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028972", "BLCT2", "BLCT3", "20140502131833", "20140802200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020554", "BLCTMS", "BLCT", "20140502174100", "20140802200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028937", "BLCTMS", "BLCTZS", "20140501173231", "20140802230000", "19", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028938", "BLCTMS", "BLCTZS", "20140501173231", "20140802230000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028935", "BLCTMS", "BLCTZS", "20140501173231", "20140802230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028936", "BLCTMS", "BLCTZS", "20140501173231", "20140802230000", "35", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000102", "BLCTZS", "BLCT2", "20140501173231", "20140803080000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028927", "BLCT", "BLCTYD", "20140501173231", "20140803080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028988", "BLCT", "BLCT2", "20140502174100", "20140803080000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000107", "BLCT", "ZHCT", "20140502105228", "20140803100000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028995", "BLCTZS", "BLCT3", "20140502174100", "20140803100000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020523", "BLCT2", "BLCTYD", "20140528182110", "20140803120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020507", "BLCT2", "BLCT", "20140528182110", "20140803120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12028895", "BLCTZS", "BLCT3", "20140529170334", "20140803120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000097", "BLCTZS", "BLCT3", "20140501054857", "20140803120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020632", "BLCTYD", "BLCT2", "20140501173231", "20140803120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020639", "BLCT", "BLCT3", "20140501173231", "20140803120000", "10", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028963", "BLCT3", "BLCTMS", "20140502174100", "20140803120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028987", "BLCTMS", "BLCT3", "20140502174100", "20140803120000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020643", "BLCTZS", "BLCT", "20140502174100", "20140803120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020644", "BLCTZS", "BLCT2", "20140502174100", "20140803120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028983", "BLCTZS", "BLCT3", "20140502174100", "20140803120000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028982", "BLCTZS", "BLCT3", "20140502174100", "20140803120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028960", "BLCT2", "BLCT3", "20140502174100", "20140803120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028980", "BLCTZS", "BLCT2", "20140502174100", "20140803120000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028978", "BLCT", "BLCT2", "20140502174100", "20140803120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028977", "BLCT", "BLCT2", "20140502174100", "20140803120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028984", "BLCT", "BLCT2", "20140502174100", "20140803120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020626", "BLCT", "BLCT2", "20140504151417", "20140803120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030016", "BLCTZS", "BLCTYD", "20140502174100", "20140803130000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020559", "BLCTMS", "BLCT3", "20140528182110", "20140803140000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028907", "BLCT2", "BLCTYD", "20140529170334", "20140803140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028908", "BLCT2", "BLCTYD", "20140529170334", "20140803140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030039", "BLCTZS", "BLCTYD", "20140502174100", "20140803140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030029", "BLCTZS", "BLCTYD", "20140502174100", "20140803140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028994", "BLCTZS", "BLCT3", "20140502174100", "20140803150000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020522", "BLCT2", "BLCTYD", "20140528182110", "20140803160000", "3", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020526", "BLCT2", "BLCTYD", "20140528182110", "20140803160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020398", "BLCT", "BLCT2", "20140528182110", "20140803160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028949", "BLCT2", "BLCTYD", "20140501173231", "20140803160000", "8", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028950", "BLCT2", "BLCTMS", "20140501173231", "20140803160000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020641", "BLCT2", "BLCTMS", "20140502131833", "20140803160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020637", "BLCTMS", "BLCT2", "20140502174100", "20140803160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028903", "BLCTZS", "BLCTYD", "20140529170334", "20140803180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028898", "BLCTZS", "BLCT", "20140529170334", "20140803180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020627", "BLCTYD", "BLCT", "20140529170334", "20140803180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028947", "BLCT2", "BLCT3", "20140501173231", "20140803180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000108", "DXCTE", "BLCTZS", "20140502105228", "20140803180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028969", "BLCT2", "BLCT", "20140502131833", "20140803180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030037", "BLCT2", "BLCTYD", "20140502174100", "20140803180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030041", "BLCTYD", "BLCT", "20140502174100", "20140803180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028993", "BLCT3", "BLCTMS", "20140502174100", "20140803180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12029007", "BLCTZS", "BLCT2", "20140503155037", "20140803180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12029008", "BLCTZS", "BLCT2", "20140503155037", "20140803180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029015", "BLCT2", "BLCT", "20140503155037", "20140803180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020574", "BLCTYD", "BLCTZS", "20140528182110", "20140803200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028957", "BLCTYD", "BLCTZS", "20140501173231", "20140803200000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028958", "BLCTYD", "BLCTZS", "20140501173231", "20140803200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030006", "BLCTYD", "BLCTZS", "20140502174100", "20140803200000", "8", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030042", "BLCT", "BLCTYD", "20140502174100", "20140803200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028975", "BLCTZS", "BLCT3", "20140502174100", "20140803200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028981", "BLCTZS", "BLCT3", "20140502174100", "20140803200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028974", "BLCTZS", "BLCT3", "20140502174100", "20140803200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020043", "BLCT", "BLCT2", "20140503155037", "20140803200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029001", "BLCTZS", "BLCTYD", "20140503155037", "20140803200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029013", "BLCTZS", "BLCT3", "20140503155037", "20140803200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029002", "BLCT2", "BLCTYD", "20140503155037", "20140803200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028989", "BLCT2", "BLCT", "20140502174100", "20140803220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028945", "BLCT3", "BLCTZS", "20140501173231", "20140803230000", "0", "20
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020612", "BLCT3", "BLCT", "20140529170334", "20140804000000", "6", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030003", "BLCTYD", "BLCT", "20140501173231", "20140804000000", "15", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000113", "ZHCT", "BLCT2", "20140502174100", "20140804000000", "24", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020044", "BLCT2", "BLCT3", "20140503155037", "20140804100000", "6", "31
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020026", "BLCT", "BLCT2", "20140503155037", "20140804110000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028857", "BLCT3", "BLCT2", "20140528182110", "20140804120000", "0", "27
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030004", "BLCT2", "BLCTZS", "20140501173231", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030009", "BLCT2", "BLCTZS", "20140501173231", "20140804120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030008", "BLCT2", "BLCTZS", "20140501173231", "20140804120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020025", "BLCT", "BLCT2", "20140503155037", "20140804120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029005", "BLCT2", "BLCTYD", "20140503155037", "20140804120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028906", "BLCT2", "BLCT3", "20140529170334", "20140804140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029026", "BLCT2", "BLCTYD", "20140503155037", "20140804150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028954", "BLCT2", "BLCTYD", "20140501173231", "20140804160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030014", "BLCT2", "BLCTZS", "20140502131833", "20140804160000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028965", "BLCT3", "BLCTMS", "20140502174100", "20140804160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028964", "BLCT3", "BLCTMS", "20140502174100", "20140804160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020638", "BLCTMS", "BLCT2", "20140502174100", "20140804160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029025", "BLCT2", "BLCT3", "20140503155037", "20140804160000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020622", "BLCTYD", "BLCT2", "20140529170334", "20140804180000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020620", "BLCTYD", "BLCT2", "20140529170334", "20140804180000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020618", "BLCTYD", "BLCT2", "20140529170334", "20140804180000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028946", "BLCT2", "BLCT3", "20140501173231", "20140804180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028944", "BLCT2", "BLCT", "20140501173231", "20140804180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028970", "BLCT2", "BLCT", "20140502131833", "20140804180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030036", "BLCTZS", "BLCTYD", "20140502174100", "20140804180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028991", "BLCT3", "BLCT2", "20140502174100", "20140804180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12029009", "BLCTZS", "BLCT2", "20140503155037", "20140804180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028930", "BLCT", "BLCTYD", "20140501173231", "20140804200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030033", "BLCTMS", "BLCTZS", "20140502174100", "20140804200000", "2", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030034", "BLCTMS", "BLCTZS", "20140502174100", "20140804200000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028976", "BLCT2", "BLCTZS", "20140502174100", "20140804200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028973", "BLCTZS", "BLCT3", "20140502174100", "20140804200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029023", "BLCTZS", "BLCT2", "20140503155037", "20140804200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029019", "BLCTZS", "BLCTYD", "20140503155037", "20140804200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029003", "BLCT2", "BLCTYD", "20140503155037", "20140804200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029004", "BLCT2", "BLCTYD", "20140503195954", "20140804200000", "0", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020628", "BLCT3", "BLCT", "20140529170334", "20140804210000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000111", "BLCTZS", "BLCT2", "20140502174100", "20140804210000", "30", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020623", "BLCTMS", "BLCTYD", "20140501173231", "20140805080000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020624", "BLCTMS", "BLCTYD", "20140501173231", "20140805080000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030013", "BLCTYD", "BLCTZS", "20140501173231", "20140805120000", "2", "25
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028933", "BLCT", "BLCTZS", "20140501173231", "20140805120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030040", "BLCT2", "BLCTYD", "20140502174100", "20140805120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029037", "BLCT", "BLCTZS", "20140505122046", "20140805120000", "15", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030035", "BLCTZS", "BLCT2", "20140502174100", "20140805130000", "3", "48
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028921", "BLCT", "BLCT2", "20140501173231", "20140805140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029022", "BLCTZS", "BLCT3", "20140503155037", "20140805140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028953", "BLCT2", "BLCT3", "20140501173231", "20140805160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028929", "BLCT", "BLCTYD", "20140501173231", "20140805160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029006", "BLCT2", "BLCTZS", "20140503155037", "20140805160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028996", "BLCTZS", "BLCTYD", "20140503195034", "20140805160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028897", "BLCTZS", "BLCT", "20140529170334", "20140805180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000114", "BLCTZS", "BLCT3", "20140502174100", "20140805180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028992", "BLCT3", "BLCTMS", "20140502174100", "20140805180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028985", "BLCT3", "BLCT", "20140502174100", "20140805180000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029012", "BLCT2", "BLCT", "20140503155037", "20140805180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029024", "BLCT3", "BLCTMS", "20140503155037", "20140805180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029010", "BLCT2", "BLCTZS", "20140503155037", "20140805180000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029017", "BLCT2", "BLCT", "20140503155037", "20140805180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029018", "BLCT2", "BLCT", "20140503155037", "20140805180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000106", "BLCT2", "DXCTE", "20140501173231", "20140805200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000104", "BLCTZS", "DXCTE", "20140501173231", "20140805200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000103", "BLCTZS", "DXCTE", "20140501173231", "20140805200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020561", "BLCTMS", "BLCT3", "20140502174100", "20140805200000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030030", "BLCTMS", "BLCTZS", "20140502174100", "20140805200000", "52", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030031", "BLCTMS", "BLCTZS", "20140502174100", "20140805200000", "5", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030032", "BLCTMS", "BLCTZS", "20140502174100", "20140805200000", "6", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028997", "BLCTZS", "BLCT3", "20140503155037", "20140805200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020064", "BLCTYD", "BLCTMS", "20140503155037", "20140805200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029011", "BLCT2", "BLCTZS", "20140503155037", "20140805200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030017", "BLCTYD", "BLCTZS", "20140505164914", "20140805220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030076", "BLCT", "BLCTYD", "20140505164914", "20140805220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030083", "BLCT", "BLCTYD", "20140505164914", "20140805220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028860", "BLCT", "BLCT2", "20140528181336", "20140806080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028862", "BLCTYD", "BLCT2", "20140528182110", "20140806080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000116", "BLCTZS", "BLCT", "20140504151417", "20140806080000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000117", "BLCTZS", "BLCT2", "20140505095938", "20140806080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028966", "BLCT3", "BLCTMS", "20140502131833", "20140806120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028986", "BLCTZS", "BLCT2", "20140502174100", "20140806120000", "40", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028961", "BLCT2", "BLCT3", "20140502174100", "20140806120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029035", "BLCT", "BLCT2", "20140505122046", "20140806120000", "5", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030074", "BLCT3", "BLCT", "20140505164914", "20140806120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030049", "BLCT", "BLCT2", "20140505164914", "20140806120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030018", "BLCT2", "BLCT3", "20140505164914", "20140806120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030075", "BLCT", "BLCT2", "20140505164914", "20140806120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029020", "BLCTZS", "BLCT3", "20140503155037", "20140806150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029021", "BLCTZS", "BLCT3", "20140503155037", "20140806150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028955", "BLCT2", "BLCT", "20140501173231", "20140806160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020617", "BLCT", "BLCTMS", "20140501173231", "20140806160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020642", "BLCT2", "BLCT", "20140502131833", "20140806160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030088", "BLCT2", "BLCTYD", "20140506132730", "20140806160000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030005", "BLCTMS", "BLCT3", "20140502174100", "20140806180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020024", "BLCT", "BLCT3", "20140503155037", "20140806180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029014", "BLCT2", "BLCT", "20140503155037", "20140806180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029016", "BLCT2", "BLCT", "20140503155037", "20140806180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029028", "BLCT2", "BLCTYD", "20140504151417", "20140806180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030007", "BLCT2", "BLCT3", "20140505164914", "20140806180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029033", "BLCT", "BLCT2", "20140505164914", "20140806180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029034", "BLCT", "BLCT2", "20140505164914", "20140806180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030080", "BLCTYD", "BLCT", "20140505164914", "20140806180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030081", "BLCTYD", "BLCT", "20140505164914", "20140806180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029042", "BLCT3", "BLCT", "20140505164914", "20140806180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030144", "BLCTZS", "BLCT3", "20140507165547", "20140806180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028971", "BLCT2", "BLCT", "20140502131833", "20140806190000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99020629", "BLCT", "BLCT2", "20140529170334", "20140806200000", "0", "17
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028931", "BLCT", "BLCT3", "20140501173231", "20140806200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000109", "BLCT2", "BLCT", "20140502174100", "20140806200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000112", "BLCT2", "BLCT", "20140502174100", "20140806200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028998", "BLCTZS", "BLCT3", "20140503155037", "20140806200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029000", "BLCTZS", "BLCT2", "20140503155037", "20140806200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000119", "BLCT2", "DXCTE", "20140505102652", "20140806200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030084", "BLCTYD", "BLCT2", "20140505164914", "20140806200000", "0", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000120", "BLCT", "BLCT2", "20140506101510", "20140806200000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039046", "BLCT2", "BLCTYD", "20140506174210", "20140806200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039045", "BLCTZS", "BLCTYD", "20140506174210", "20140806200100", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030110", "BLCTYD", "BLCT2", "20140506132730", "20140806200200", "28", "27
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039044", "BLCTZS", "BLCT3", "20140506174210", "20140806200200", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039043", "BLCTZS", "BLCT3", "20140506174210", "20140806200300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030098", "BLCT", "BLCT3", "20140506174210", "20140806200400", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000115", "BLCT2", "BLCT", "20140503155037", "20140806210000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028979", "BLCT2", "BLCT", "20140502174100", "20140807080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029072", "BLCTZS", "BLCT3", "20140507121336", "20140807080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000134", "BLCTZS", "BLCT2", "20140508165120", "20140807080000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030213", "BLCTZS", "BLCTYD", "20140510164958", "20140807080000", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030097", "BLCTMS", "BLCT3", "20140506174210", "20140807080100", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030096", "BLCTMS", "BLCT3", "20140506174210", "20140807080200", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030095", "BLCTMS", "BLCT3", "20140506174210", "20140807080300", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030191", "BLCTZS", "BLCT3", "20140509173025", "20140807080300", "8", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030094", "BLCTMS", "BLCT3", "20140506174210", "20140807080400", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029094", "BLCTZS", "BLCTMS", "20140507165547", "20140807100000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030130", "BLCTZS", "BLCTMS", "20140507165547", "20140807100000", "5", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000129", "BLCTZS", "BLCT3", "20140507215643", "20140807110000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028959", "BLCT2", "BLCT3", "20140502174100", "20140807120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020640", "BLCTMS", "BLCTYD", "20140502174100", "20140807120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029027", "BLCT2", "BLCTMS", "20140504151417", "20140807120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030089", "BLCT3", "BLCT2", "20140505164914", "20140807120000", "24", "35
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029040", "BLCT", "BLCT2", "20140505164914", "20140807120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039059", "BLCT3", "BLCTMS", "20140506174210", "20140807120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030116", "BLCT2", "BLCT3", "20140507121336", "20140807120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000136", "BLCTZS", "BLCT3", "20140509173025", "20140807120000", "28", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039060", "BLCT3", "BLCTMS", "20140506174210", "20140807120100", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030102", "BLCTZS", "BLCT2", "20140506160441", "20140807120600", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039056", "BLCT3", "BLCTZS", "20140506174210", "20140807140000", "13", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039054", "BLCTYD", "BLCT2", "20140506174210", "20140807140100", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039052", "BLCT", "BLCT2", "20140506174210", "20140807140200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028920", "BLCT", "BLCT2", "20140501173231", "20140807150000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029114", "BLCTZS", "BLCT3", "20140508164810", "20140807150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028928", "BLCT", "BLCT2", "20140501173231", "20140807160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039049", "BLCTYD", "BLCT2", "20140506174210", "20140807160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039053", "BLCTYD", "BLCT2", "20140506174210", "20140807160000", "32", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039048", "BLCTYD", "BLCT2", "20140506174210", "20140807160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039051", "BLCTYD", "BLCT2", "20140506174210", "20140807160100", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039047", "BLCTYD", "BLCT2", "20140506174210", "20140807160200", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039065", "BLCT2", "BLCT", "20140506174210", "20140807160300", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030101", "BLCTYD", "BLCT2", "20140506174210", "20140807190000", "8", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028896", "BLCT3", "BLCT2", "20140529170334", "20140807200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030038", "BLCT2", "BLCT3", "20140502174100", "20140807200000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020636", "BLCTMS", "BLCT2", "20140502174100", "20140807200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029038", "BLCT", "BLCT2", "20140505122046", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030077", "BLCT2", "BLCTZS", "20140505164914", "20140807200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000121", "BLCT2", "ZHCT", "20140506132730", "20140807200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000122", "BLCT2", "ZHCT", "20140506132730", "20140807200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000123", "BLCT2", "BLCTZS", "20140506142448", "20140807200000", "8", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039069", "BLCTZS", "BLCT3", "20140506174210", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030150", "BLCTMS", "BLCT2", "20140509173025", "20140807200300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030113", "BLCT", "BLCT3", "20140506174210", "20140807200500", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030071", "BLCTMS", "BLCT2", "20140505164914", "20140807210000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028932", "BLCT", "BLCT3", "20140501173231", "20140807220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030086", "BLCT2", "BLCT3", "20140505164914", "20140807230000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029071", "BLCT2", "BLCT", "20140507121336", "20140807230000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030125", "BLCTZS", "BLCT3", "20140507165547", "20140807230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000128", "DXCTE", "BLCTZS", "20140507121336", "20140808010000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029096", "BLCTZS", "BLCT", "20140507165547", "20140808010000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030087", "BLCT2", "BLCT3", "20140505164914", "20140808080000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029041", "BLCT2", "BLCTZS", "20140505164914", "20140808080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030093", "BLCTMS", "BLCTZS", "20140506174210", "20140808080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030122", "BLCTYD", "BLCTZS", "20140507165547", "20140808080000", "9", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030123", "BLCTYD", "BLCTZS", "20140507165547", "20140808080000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030124", "BLCTYD", "BLCTZS", "20140507165547", "20140808080000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029029", "BLCT3", "BLCT", "20140504151417", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030073", "BLCT", "BLCT2", "20140505164914", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030020", "BLCT2", "BLCT", "20140505164914", "20140808120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030019", "BLCT2", "BLCT3", "20140505164914", "20140808120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029032", "BLCT2", "BLCT", "20140505164914", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030072", "BLCT", "BLCT2", "20140505164914", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030051", "BLCT", "BLCT2", "20140505164914", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030114", "BLCT", "BLCTMS", "20140506174210", "20140808120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030115", "BLCT2", "BLCT", "20140507121336", "20140808120000", "2", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000126", "BLCTZS", "BLCT2", "20140507121336", "20140808120000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000125", "BLCTZS", "BLCT", "20140507121336", "20140808120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000124", "BLCTZS", "BLCT3", "20140507121336", "20140808120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030117", "BLCT2", "BLCT3", "20140507153104", "20140808120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029082", "BLCTZS", "BLCT2", "20140507153104", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029083", "BLCTZS", "BLCT2", "20140507153104", "20140808120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029084", "BLCT", "BLCT2", "20140507153104", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029085", "BLCT", "BLCT2", "20140507153104", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029086", "BLCT", "BLCT2", "20140507153104", "20140808120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029087", "BLCT2", "BLCT", "20140507153104", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029088", "BLCT2", "BLCT", "20140507153104", "20140808120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029081", "BLCT2", "BLCTYD", "20140507153104", "20140808120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030140", "BLCTYD", "BLCT", "20140507165547", "20140808120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029098", "BLCTZS", "BLCT2", "20140507165547", "20140808140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029073", "BLCTMS", "BLCT3", "20140507165547", "20140808140000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029100", "BLCT2", "BLCT3", "20140507165547", "20140808150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029112", "BLCTZS", "BLCT3", "20140508164810", "20140808150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029115", "BLCTZS", "BLCT3", "20140508164810", "20140808150000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029117", "BLCTZS", "BLCT3", "20140508164810", "20140808150000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020634", "BLCTMS", "BLCT2", "20140505164914", "20140808160000", "16", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039068", "BLCT2", "BLCT", "20140506174210", "20140808160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029079", "BLCTZS", "BLCT2", "20140507153104", "20140808160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029078", "BLCTZS", "BLCT2", "20140507153104", "20140808160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029080", "BLCTZS", "BLCT", "20140507153104", "20140808160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029099", "BLCTZS", "BLCT", "20140507165547", "20140808160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039067", "BLCT2", "BLCT", "20140506174210", "20140808160100", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039066", "BLCT2", "BLCT", "20140506174210", "20140808160200", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000118", "BLCTZS", "DXCTE", "20140505102652", "20140808180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039057", "BLCT2", "BLCTZS", "20140506174210", "20140808180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029102", "BLCT2", "BLCT", "20140507165547", "20140808180000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029103", "BLCT2", "BLCT", "20140507165547", "20140808180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029092", "BLCT3", "BLCTZS", "20140507165547", "20140808180000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030131", "BLCTYD", "BLCTMS", "20140507165547", "20140808180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030111", "BLCTYD", "BLCT", "20140507165547", "20140808180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030112", "BLCT3", "BLCT", "20140508094138", "20140808180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029109", "BLCT2", "BLCT", "20140508164810", "20140808180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030160", "BLCTYD", "BLCTZS", "20140508164810", "20140808180000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029108", "BLCT2", "BLCT", "20140508164810", "20140808180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029107", "BLCT2", "BLCT", "20140508164810", "20140808180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029182", "BLCT3", "BLCTMS", "20140510163259", "20140808180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029181", "BLCT3", "BLCTMS", "20140510163259", "20140808180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029104", "BLCTZS", "BLCTYD", "20140508133343", "20140808190000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030157", "BLCTYD", "BLCTZS", "20140508164810", "20140808190000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030158", "BLCTZS", "BLCTYD", "20140508164810", "20140808190000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029116", "BLCTZS", "BLCTYD", "20140508164810", "20140808190000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028809", "BLCT3", "BLCT", "20140529170334", "20140808200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028968", "BLCT2", "BLCT", "20140502131833", "20140808200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029036", "BLCT", "BLCTMS", "20140505122046", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039070", "BLCT2", "BLCTZS", "20140506174210", "20140808200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029097", "BLCTZS", "BLCT", "20140507165547", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029093", "BLCTZS", "BLCT3", "20140507165547", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029090", "BLCT2", "BLCT", "20140507165547", "20140808200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029089", "BLCTZS", "BLCT2", "20140507165547", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030135", "BLCT", "BLCTMS", "20140507165547", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030146", "BLCT3", "BLCT", "20140507165547", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030136", "BLCTYD", "BLCT", "20140507165547", "20140808200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030137", "BLCTYD", "BLCT", "20140507165547", "20140808200000", "16", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030138", "BLCTYD", "BLCT", "20140507165547", "20140808200000", "29", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030139", "BLCTYD", "BLCTZS", "20140507165547", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030147", "BLCTYD", "BLCT", "20140507165547", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029105", "BLCT2", "BLCT", "20140508133343", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030161", "BLCTYD", "BLCTMS", "20140508164810", "20140808200000", "2", "34
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029129", "BLCT", "BLCT2", "20140508164810", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039058", "BLCTZS", "BLCT2", "20140506174210", "20140808200100", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039062", "BLCT2", "BLCT", "20140506174210", "20140808200200", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030090", "BLCTYD", "BLCT", "20140506174210", "20140808200200", "2", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030091", "BLCTYD", "BLCT", "20140506174210", "20140808200300", "15", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039061", "BLCT2", "BLCTZS", "20140506174210", "20140808200300", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029095", "BLCT2", "BLCT", "20140507153104", "20140808220000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029039", "BLCT2", "BLCTZS", "20140505122046", "20140809080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030099", "BLCTMS", "BLCT3", "20140506174210", "20140809080000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030100", "BLCTMS", "BLCT3", "20140506174210", "20140809080000", "8", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020027", "BLCT", "BLCTMS", "20140503155037", "20140809110000", "6", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020028", "BLCT", "BLCTMS", "20140503155037", "20140809120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030048", "BLCT", "BLCTMS", "20140505164914", "20140809120000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029124", "BLCT", "BLCT3", "20140508164810", "20140809120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030046", "BLCT", "BLCT2", "20140508164810", "20140809120000", "52", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029128", "BLCT", "BLCTYD", "20140508164810", "20140809120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029142", "BLCTZS", "BLCT2", "20140508164810", "20140809120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030164", "BLCT2", "BLCTZS", "20140508164810", "20140809120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029125", "BLCT", "BLCT3", "20140508164810", "20140809120000", "1", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030120", "BLCTMS", "BLCT3", "20140506174210", "20140809130000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029031", "BLCT2", "BLCTYD", "20140505164914", "20140809140000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030159", "BLCT2", "BLCTYD", "20140508164810", "20140809150000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029113", "BLCTZS", "BLCT", "20140508164810", "20140809150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020633", "BLCTMS", "BLCT", "20140502174100", "20140809160000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12020635", "BLCTMS", "BLCT2", "20140502174100", "20140809160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030015", "BLCTMS", "BLCT2", "20140505164914", "20140809160000", "0", "34
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030121", "BLCT3", "BLCT2", "20140506174210", "20140809160000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030127", "BLCT2", "BLCT", "20140509173025", "20140809160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030065", "BLCT2", "BLCTMS", "20140506174210", "20140809160100", "3", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030069", "BLCT2", "BLCTMS", "20140506174210", "20140809160300", "10", "26
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99028999", "BLCTZS", "BLCT3", "20140503155037", "20140809180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030085", "BLCT2", "BLCTZS", "20140505164914", "20140809180000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030092", "BLCTYD", "BLCT", "20140506174210", "20140809180000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029101", "BLCT2", "BLCT", "20140507165547", "20140809180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029091", "BLCT3", "BLCTZS", "20140507165547", "20140809180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030143", "BLCT3", "BLCT", "20140507165547", "20140809180000", "17", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030148", "BLCTYD", "BLCT", "20140507165547", "20140809180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030145", "BLCTZS", "BLCT", "20140507165547", "20140809180000", "2", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030163", "BLCTZS", "BLCTYD", "20140508164810", "20140809180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030202", "BLCTYD", "BLCT", "20140509173025", "20140809180100", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030142", "BLCT3", "BLCT", "20140507165547", "20140809181000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029030", "BLCT2", "BLCT3", "20140505164914", "20140809200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039064", "BLCT2", "BLCT", "20140506174210", "20140809200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000127", "BLCT2", "DXCTE", "20140507121336", "20140809200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030149", "BLCTZS", "BLCTYD", "20140507165547", "20140809200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029106", "BLCT2", "BLCTZS", "20140508133343", "20140809200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029134", "BLCT2", "BLCT", "20140508164810", "20140809200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029136", "BLCT2", "BLCT", "20140508164810", "20140809200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030057", "BLCT3", "BLCT2", "20140508164810", "20140809200000", "15", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039147", "BLCTZS", "BLCT2", "20140509173025", "20140809200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039148", "BLCTZS", "BLCT2", "20140509173025", "20140809200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039149", "BLCTZS", "BLCT", "20140509173025", "20140809200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039063", "BLCT2", "BLCT", "20140506174210", "20140809200100", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030108", "BLCTYD", "BLCTZS", "20140506174210", "20140809200600", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029077", "BLCT2", "BLCT3", "20140507165547", "20140809220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029076", "BLCT2", "BLCT3", "20140507165547", "20140809220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029075", "BLCT2", "BLCT3", "20140507165547", "20140809220000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000133", "BLCTZS", "BLCT3", "20140508165120", "20140810080000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030197", "BLCT3", "BLCT2", "20140509173025", "20140810080000", "10", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029146", "BLCTZS", "BLCT3", "20140509173025", "20140810080000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030178", "BLCT2", "BLCT3", "20140509173025", "20140810080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030196", "BLCT2", "BLCT3", "20140509173025", "20140810080200", "11", "33
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030052", "BLCT", "BLCTZS", "20140505164914", "20140810120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030119", "BLCTMS", "BLCT", "20140506174210", "20140810120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030118", "BLCTMS", "BLCT", "20140506174210", "20140810120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029120", "BLCTMS", "BLCT3", "20140508164810", "20140810120000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029121", "BLCTMS", "BLCT3", "20140508164810", "20140810120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029122", "BLCTMS", "BLCT3", "20140508164810", "20140810120000", "12", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029123", "BLCTMS", "BLCT3", "20140508164810", "20140810120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030173", "BLCTMS", "BLCTYD", "20140508164810", "20140810120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030170", "BLCTMS", "BLCT3", "20140508164810", "20140810120000", "1", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030167", "BLCTMS", "BLCT", "20140508164810", "20140810120000", "5", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030056", "BLCT3", "BLCTMS", "20140508164810", "20140810120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029127", "BLCT", "BLCT3", "20140508164810", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029165", "BLCT", "BLCT2", "20140509173025", "20140810120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030185", "BLCT", "BLCT2", "20140509173025", "20140810120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029160", "BLCTZS", "BLCT2", "20140509173025", "20140810120000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030184", "BLCTZS", "BLCT2", "20140510005308", "20140810120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029161", "BLCT", "BLCT2", "20140509173025", "20140810120100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029154", "BLCT", "BLCT2", "20140509173025", "20140810120200", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029162", "BLCT", "BLCTZS", "20140509173025", "20140810120200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029163", "BLCT", "BLCT2", "20140509173025", "20140810120300", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029164", "BLCT", "BLCT2", "20140509173025", "20140810120400", "5", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029153", "BLCTMS", "BLCTZS", "20140509173025", "20140810120500", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030154", "BLCT2", "BLCT3", "20140507165547", "20140810140000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029145", "BLCTZS", "BLCT2", "20140508164810", "20140810160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030126", "BLCT2", "BLCTMS", "20140509173025", "20140810160000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030128", "BLCT2", "BLCTYD", "20140509173025", "20140810160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029168", "BLCT2", "BLCTMS", "20140510163259", "20140810160000", "3", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030204", "BLCTYD", "BLCT", "20140509173025", "20140810180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029158", "BLCT3", "BLCTMS", "20140509173025", "20140810180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030109", "BLCTYD", "BLCTZS", "20140506174210", "20140810200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030133", "BLCTYD", "BLCT2", "20140507165547", "20140810200000", "0", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030134", "BLCTYD", "BLCT2", "20140507165547", "20140810200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030141", "BLCT3", "BLCT", "20140507165547", "20140810200000", "0", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030168", "BLCT2", "BLCTYD", "20140508164810", "20140810200000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030132", "BLCTMS", "BLCTZS", "20140508164810", "20140810200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029138", "BLCT2", "BLCT", "20140508164810", "20140810200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029135", "BLCT2", "BLCT", "20140508164810", "20140810200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029152", "BLCTZS", "BLCT3", "20140509173025", "20140810200000", "9", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030223", "BLCTYD", "BLCT", "20140510163259", "20140810200000", "30", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029151", "BLCTZS", "BLCTYD", "20140509173025", "20140810200100", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029180", "BLCT", "BLCT2", "20140510163259", "20140810230000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029186", "BLCT2", "BLCTZS", "20140510164958", "20140810230000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029193", "BLCT2", "BLCTYD", "20140510164958", "20140810230000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030050", "BLCT", "BLCT2", "20140505164914", "20140811120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030068", "BLCT2", "BLCT", "20140506174210", "20140811120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000130", "BLCT2", "BLCT", "20140508133343", "20140811120000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000131", "BLCT2", "BLCT", "20140508133343", "20140811120000", "4", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000132", "BLCTZS", "BLCT2", "20140508144922", "20140811120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030165", "BLCT3", "BLCTMS", "20140508164810", "20140811120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030187", "BLCT2", "BLCTZS", "20140509173025", "20140811120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030186", "BLCT2", "BLCTZS", "20140509173025", "20140811120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000135", "BLCTZS", "BLCT3", "20140509173025", "20140811120000", "12", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000137", "BLCT2", "BLCT3", "20140510115250", "20140811120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000138", "BLCT3", "BLCT2", "20140510115250", "20140811120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029176", "BLCT", "BLCT2", "20140510163259", "20140811120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030053", "BLCTMS", "BLCT2", "20140509173025", "20140811120300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030151", "BLCT2", "BLCTYD", "20140507153104", "20140811160000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029169", "BLCT2", "BLCT", "20140509173025", "20140811160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029155", "BLCT", "BLCT2", "20140509173025", "20140811160000", "0", "18
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030066", "BLCT2", "BLCTYD", "20140506174210", "20140811160200", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030059", "BLCT3", "BLCT2", "20140511095659", "20140811170000", "6", "48
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029140", "BLCT3", "BLCTMS", "20140508164810", "20140811180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029141", "BLCT3", "BLCTMS", "20140508164810", "20140811180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030193", "BLCT3", "BLCT", "20140509173025", "20140811180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030203", "BLCTYD", "BLCT", "20140509173025", "20140811180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029157", "BLCT3", "BLCTMS", "20140509173025", "20140811180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000139", "DXCTE", "BLCTZS", "20140510123205", "20140811180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030183", "BLCT", "BLCT2", "20140510163259", "20140811180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029183", "BLCT2", "BLCT", "20140510163259", "20140811180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030227", "BLCT", "BLCTYD", "20140510164958", "20140811180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029159", "BLCT3", "BLCTMS", "20140509173025", "20140811180600", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030156", "BLCTYD", "BLCTZS", "20140507165547", "20140811200000", "3", "47
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030155", "BLCTYD", "BLCTZS", "20140507165547", "20140811200000", "3", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030169", "BLCT2", "BLCTMS", "20140508164810", "20140811200000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030221", "BLCTYD", "BLCT", "20140510163259", "20140811200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029178", "BLCTZS", "BLCTYD", "20140510164958", "20140811200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029170", "BLCT2", "BLCTYD", "20140510164958", "20140811200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030188", "BLCTMS", "BLCTZS", "20140509173025", "20140811200100", "14", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030189", "BLCTMS", "BLCTZS", "20140509173025", "20140811200200", "5", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030190", "BLCTMS", "BLCTZS", "20140509173025", "20140811200200", "100", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030078", "BLCTMS", "BLCT2", "20140508094203", "20140811200300", "15", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030054", "BLCTMS", "BLCT2", "20140509173025", "20140811200400", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029192", "BLCT2", "BLCTZS", "20140510163259", "20140811230000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030224", "BLCT", "BLCTYD", "20140510164958", "20140812080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030219", "BLCT", "BLCTMS", "20140512155500", "20140812110200", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029132", "BLCT", "BLCT3", "20140508164810", "20140812120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030175", "BLCTMS", "BLCT", "20140508164810", "20140812120000", "37", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030055", "BLCTZS", "BLCT2", "20140508164810", "20140812120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030226", "BLCTZS", "BLCT3", "20140510164958", "20140812120000", "1", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029110", "BLCT2", "BLCT3", "20140508164810", "20140812150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029143", "BLCTZS", "BLCT2", "20140508164810", "20140812160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030062", "BLCT2", "BLCT", "20140509173025", "20140812160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029187", "BLCT2", "BLCTZS", "20140510163259", "20140812160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029194", "BLCT2", "BLCTZS", "20140510164958", "20140812160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030171", "BLCTMS", "BLCT3", "20140508164810", "20140812180000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030172", "BLCTMS", "BLCT3", "20140508164810", "20140812180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029188", "BLCT2", "BLCT", "20140510163259", "20140812180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029156", "BLCT3", "BLCTMS", "20140509173025", "20140812180100", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030166", "BLCTZS", "BLCT2", "20140509173025", "20140812180200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029137", "BLCT2", "BLCT", "20140508164810", "20140812200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029139", "BLCT2", "BLCT", "20140508164810", "20140812200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029171", "BLCTZS", "BLCTYD", "20140510164958", "20140812200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039213", "BLCTZS", "BLCTYD", "20140512155500", "20140812200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039215", "BLCT2", "BLCTYD", "20140512155500", "20140812200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030234", "BLCTZS", "BLCTYD", "20140512155500", "20140813080000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030233", "BLCT", "BLCTYD", "20140513165136", "20140813080000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030235", "BLCTZS", "BLCTYD", "20140512155500", "20140813080100", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030229", "BLCT", "BLCTYD", "20140512155500", "20140813080200", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039150", "BLCT", "BLCT2", "20140509173025", "20140813080300", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030249", "BLCT", "BLCTYD", "20140512155500", "20140813080300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029219", "BLCT", "BLCT2", "20140512155500", "20140813100000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029220", "BLCT", "BLCT2", "20140512155500", "20140813100100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000141", "ZHCT", "BLCT3", "20140512155500", "20140813100200", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030241", "BLCT", "BLCTZS", "20140512161923", "20140813100200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030058", "BLCT3", "BLCT2", "20140508164810", "20140813120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029133", "BLCT", "BLCT3", "20140508164810", "20140813120000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030181", "BLCT", "BLCT2", "20140510163259", "20140813120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030107", "BLCT2", "BLCTMS", "20140510163259", "20140813120000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039222", "BLCTZS", "BLCT3", "20140512155500", "20140813120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039223", "BLCTZS", "BLCT3", "20140512155500", "20140813120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039221", "BLCTZS", "BLCT3", "20140512155500", "20140813120100", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030206", "BLCTMS", "BLCT2", "20140512155500", "20140813120100", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039228", "BLCTMS", "BLCTYD", "20140512155500", "20140813120100", "15", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039225", "BLCTZS", "BLCT3", "20140512155500", "20140813120100", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030177", "BLCTMS", "BLCT2", "20140512155500", "20140813120200", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030238", "BLCT2", "BLCT", "20140512155500", "20140813120400", "21", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030247", "BLCT2", "BLCTZS", "20140512155500", "20140813120600", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030220", "BLCT", "BLCTMS", "20140512155500", "20140813120600", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030239", "BLCT2", "BLCTYD", "20140512155500", "20140813120800", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029111", "BLCT2", "BLCT", "20140508164810", "20140813150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029195", "BLCT3", "BLCT2", "20140511142721", "20140813150000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039214", "BLCT2", "BLCTYD", "20140512160415", "20140813150000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029118", "BLCTZS", "BLCT2", "20140508164810", "20140813160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029144", "BLCTZS", "BLCT", "20140508164810", "20140813160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030180", "BLCT3", "BLCT2", "20140509173025", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029203", "BLCTZS", "BLCT2", "20140511095659", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030174", "BLCTMS", "BLCT3", "20140508164810", "20140813180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029189", "BLCT2", "BLCT", "20140510163259", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029191", "BLCT2", "BLCT", "20140510163259", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029185", "BLCT2", "BLCTYD", "20140510163259", "20140813180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030225", "BLCTYD", "BLCT2", "20140510164958", "20140813180000", "7", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030228", "BLCTZS", "BLCT2", "20140510164958", "20140813180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029200", "BLCTZS", "BLCT2", "20140511095659", "20140813180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029202", "BLCTZS", "BLCT2", "20140511095659", "20140813180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029201", "BLCTZS", "BLCT2", "20140511095659", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030205", "BLCTYD", "BLCT", "20140509173025", "20140813180400", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030222", "BLCTYD", "BLCT", "20140510163259", "20140813200000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039234", "BLCTZS", "BLCTYD", "20140512155500", "20140813200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039229", "BLCTZS", "BLCTYD", "20140512155500", "20140813200000", "24", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039217", "BLCT2", "BLCT3", "20140512160415", "20140813200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029251", "BLCT2", "BLCT3", "20140513165136", "20140813200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029239", "BLCT", "BLCT2", "20140513165136", "20140813200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030315", "BLCTMS", "BLCT3", "20140514183801", "20140813200000", "3", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030047", "BLCTMS", "BLCT2", "20140508094203", "20140813200400", "44", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000140", "BLCT2", "BLCT", "20140510135841", "20140813210000", "6", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030207", "BLCTMS", "BLCT2", "20140512155500", "20140813220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029276", "BLCT2", "BLCT", "20140514183801", "20140814000001", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029196", "BLCT3", "BLCT", "20140511142721", "20140814000002", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030281", "BLCT3", "BLCT", "20140513165136", "20140814000002", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030305", "BLCT2", "BLCTZS", "20140514183021", "20140814010000", "0", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030316", "BLCT2", "BLCTZS", "20140514202820", "20140814010001", "0", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030153", "BLCTZS", "BLCTYD", "20140514172641", "20140814020000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030289", "BLCTZS", "BLCT3", "20140514172641", "20140814020002", "5", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029281", "BLCTYD", "BLCT2", "20140514183801", "20140814030001", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029280", "BLCTYD", "BLCT2", "20140514183801", "20140814030002", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029278", "BLCTYD", "BLCT2", "20140514183801", "20140814030003", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030209", "BLCTYD", "BLCT", "20140514183801", "20140814030005", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030106", "BLCT3", "BLCT2", "20140509173025", "20140814080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039211", "BLCT3", "BLCT2", "20140512160415", "20140814080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039212", "BLCT3", "BLCT2", "20140512160415", "20140814080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030245", "BLCT2", "BLCT3", "20140512160415", "20140814080000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039210", "BLCT3", "BLCT2", "20140512160415", "20140814080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030251", "BLCTMS", "BLCT3", "20140513165136", "20140814080000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030250", "BLCT", "BLCT3", "20140513165136", "20140814080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030253", "BLCTMS", "BLCT3", "20140513165136", "20140814080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030254", "BLCTMS", "BLCT3", "20140513165136", "20140814080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030256", "BLCTMS", "BLCT3", "20140513165136", "20140814080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030252", "BLCTMS", "BLCT3", "20140513165136", "20140814080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029074", "BLCT3", "BLCTMS", "20140507153104", "20140814120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029126", "BLCT", "BLCT3", "20140508164810", "20140814120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029131", "BLCT", "BLCT2", "20140508164810", "20140814120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029177", "BLCT", "BLCT2", "20140510163259", "20140814120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030278", "BLCT3", "BLCT2", "20140513165136", "20140814120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029246", "BLCT2", "BLCTZS", "20140513165136", "20140814120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029252", "BLCT2", "BLCT", "20140513165136", "20140814120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029329", "BLCTYD", "BLCTMS", "20140516160712", "20140814120200", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029248", "BLCTYD", "BLCTZS", "20140513165136", "20140814140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029249", "BLCT", "BLCTZS", "20140513165136", "20140814140000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029247", "BLCT", "BLCTZS", "20140513165136", "20140814140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029119", "BLCTZS", "BLCT2", "20140508164810", "20140814150000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029199", "BLCT3", "BLCT2", "20140511142721", "20140814150000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029198", "BLCT3", "BLCT2", "20140511142721", "20140814150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029175", "BLCT", "BLCT2", "20140510163259", "20140814160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029250", "BLCT2", "BLCT", "20140513165136", "20140814160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029258", "BLCTYD", "BLCTMS", "20140514183801", "20140814160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029172", "BLCT2", "BLCT", "20140509173025", "20140814160100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029328", "BLCTYD", "BLCTMS", "20140515170526", "20140814160200", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029174", "BLCT", "BLCT2", "20140510163259", "20140814180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000143", "ZHCT", "BLCTZS", "20140513115651", "20140814180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000145", "ZHCT", "BLCTZS", "20140513115651", "20140814180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029283", "BLCTYD", "BLCTMS", "20140514183801", "20140814180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029166", "BLCT2", "BLCTYD", "20140509173025", "20140814200000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030194", "BLCT2", "BLCT3", "20140509173025", "20140814200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000144", "BLCTZS", "DXCTE", "20140513115651", "20140814200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030266", "BLCT", "BLCTZS", "20140513115651", "20140814200000", "17", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030265", "BLCT", "BLCT2", "20140513115651", "20140814200000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030240", "BLCT3", "BLCT2", "20140513115651", "20140814200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030274", "BLCTZS", "BLCT2", "20140513165136", "20140814200000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029245", "BLCTYD", "BLCTZS", "20140513165136", "20140814200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029243", "BLCT3", "BLCTZS", "20140513165136", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029241", "BLCT3", "BLCTZS", "20140513165136", "20140814200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029242", "BLCT3", "BLCTZS", "20140513165136", "20140814200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029244", "BLCTYD", "BLCTZS", "20140513165136", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029259", "BLCT", "BLCT2", "20140514115050", "20140814200000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029261", "BLCT2", "BLCT3", "20140514183801", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030267", "BLCT2", "BLCT3", "20140514183801", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029240", "BLCT2", "BLCT3", "20140513183218", "20140814220000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030297", "BLCTZS", "BLCT3", "20140514172641", "20140814230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000153", "BLCTZS", "BLCT3", "20140514183801", "20140815020001", "50", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039209", "BLCT2", "BLCT", "20140512160415", "20140815080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030255", "BLCTMS", "BLCTZS", "20140513165136", "20140815080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030291", "BLCTYD", "BLCTZS", "20140514183801", "20140815080000", "2", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030287", "BLCTYD", "BLCTZS", "20140514183801", "20140815080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029267", "BLCT", "BLCT2", "20140514183801", "20140815100000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029265", "BLCT", "BLCT2", "20140514183801", "20140815100000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029130", "BLCT", "BLCTYD", "20140508164810", "20140815120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030060", "BLCT3", "BLCT2", "20140511095659", "20140815120000", "3", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030176", "BLCT3", "BLCT2", "20140511095659", "20140815120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030061", "BLCT3", "BLCT", "20140511095659", "20140815120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030232", "BLCT", "BLCT2", "20140512155500", "20140815120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039206", "BLCT2", "BLCT", "20140512155500", "20140815120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039230", "BLCT2", "BLCT", "20140512155500", "20140815120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039207", "BLCT2", "BLCT", "20140512155500", "20140815120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029257", "BLCT2", "BLCT", "20140514183801", "20140815120000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030270", "BLCT2", "BLCT", "20140514183801", "20140815120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030230", "BLCT", "BLCT2", "20140512155500", "20140815120100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039204", "BLCT2", "BLCTYD", "20140512151819", "20140815120200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030231", "BLCT3", "BLCT2", "20140512155500", "20140815120200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039232", "BLCT2", "BLCTZS", "20140512155500", "20140815120200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030195", "BLCT", "BLCT2", "20140512155500", "20140815120200", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039205", "BLCT2", "BLCT", "20140512155500", "20140815120300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030218", "BLCT", "BLCT2", "20140512155500", "20140815120700", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000149", "BLCTZS", "BLCTYD", "20140514183801", "20140815140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000152", "BLCTZS", "BLCT2", "20140514183801", "20140815140000", "1", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000150", "BLCTZS", "BLCT3", "20140514183801", "20140815140000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000151", "BLCTZS", "BLCT", "20140514183801", "20140815140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029197", "BLCT3", "BLCT", "20140511142721", "20140815150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029299", "BLCTZS", "BLCT2", "20140515114430", "20140815160400", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029190", "BLCT2", "BLCT", "20140510163259", "20140815180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029184", "BLCT2", "BLCTYD", "20140510163259", "20140815180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030271", "BLCTZS", "BLCTMS", "20140513165136", "20140815180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030273", "BLCTYD", "BLCT", "20140513165136", "20140815180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029256", "BLCT2", "BLCT", "20140514115050", "20140815180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030217", "BLCT2", "BLCT", "20140514183801", "20140815180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029271", "BLCT2", "BLCT", "20140514183801", "20140815180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029325", "BLCTZS", "BLCT3", "20140515170526", "20140815180200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029318", "BLCTZS", "BLCT2", "20140515170526", "20140815180400", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030243", "BLCTYD", "BLCT", "20140512160415", "20140815200000", "0", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039236", "BLCT3", "BLCT2", "20140512160415", "20140815200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030246", "BLCT2", "BLCT3", "20140512160415", "20140815200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030283", "BLCTYD", "BLCT", "20140513165136", "20140815200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030282", "BLCTYD", "BLCT", "20140513165136", "20140815200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030275", "BLCTYD", "BLCT", "20140513165136", "20140815200000", "7", "26
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030276", "BLCTYD", "BLCT", "20140513165136", "20140815200000", "8", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030279", "BLCT3", "BLCT", "20140513165136", "20140815200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030277", "BLCT3", "BLCT", "20140513165136", "20140815200000", "16", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029262", "BLCT2", "BLCTZS", "20140514183021", "20140815200000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030303", "BLCT2", "BLCT3", "20140514183801", "20140815200000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029272", "BLCT2", "BLCT", "20140514183801", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029269", "BLCT2", "BLCT", "20140514183801", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029268", "BLCT2", "BLCT", "20140514183801", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029273", "BLCT2", "BLCT", "20140514183801", "20140815200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029277", "BLCT2", "BLCT", "20140514183801", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029275", "BLCT2", "BLCT", "20140514183801", "20140815200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030152", "BLCT", "BLCT3", "20140514183801", "20140815200000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030290", "BLCTYD", "BLCT2", "20140514183801", "20140815200000", "2", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029263", "BLCT3", "BLCTZS", "20140514183801", "20140815200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030288", "BLCTYD", "BLCTZS", "20140514183801", "20140815200000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029279", "BLCTYD", "BLCT2", "20140514183801", "20140815200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029285", "BLCTYD", "BLCT2", "20140514183801", "20140815200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029289", "BLCTZS", "BLCT", "20140515105614", "20140815200000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029303", "BLCT3", "BLCTMS", "20140515114430", "20140815200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030162", "BLCT2", "BLCTZS", "20140509173025", "20140815200300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039238", "BLCT3", "BLCTMS", "20140512155500", "20140815200400", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029298", "BLCT", "BLCTMS", "20140515114430", "20140815200600", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029255", "BLCTYD", "BLCT2", "20140514115050", "20140815230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029309", "BLCT", "BLCT2", "20140515114430", "20140815230000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030321", "BLCTMS", "BLCT3", "20140515170526", "20140815230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030320", "BLCTMS", "BLCT3", "20140515170526", "20140815230000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030322", "BLCTMS", "BLCTZS", "20140515170526", "20140815230100", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029308", "BLCT", "BLCT2", "20140515114430", "20140815230200", "8", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029310", "BLCT", "BLCT2", "20140515114430", "20140815230500", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029286", "BLCTMS", "BLCT3", "20140514183801", "20140815230000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030257", "BLCTMS", "BLCT3", "20140513165136", "20140816080000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030258", "BLCTMS", "BLCT3", "20140513165136", "20140816080000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030259", "BLCTMS", "BLCT3", "20140513165136", "20140816080000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030260", "BLCTMS", "BLCT3", "20140513165136", "20140816080000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030261", "BLCTMS", "BLCT3", "20140513165136", "20140816080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030327", "BLCT", "BLCT3", "20140515170526", "20140816080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030324", "BLCTMS", "BLCTYD", "20140515170526", "20140816080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029320", "BLCTZS", "BLCT2", "20140515170526", "20140816080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029316", "BLCTZS", "BLCT2", "20140515170526", "20140816080000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030329", "BLCTZS", "BLCTYD", "20140515170526", "20140816080100", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030323", "BLCTMS", "BLCT3", "20140515170526", "20140816080200", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030332", "BLCTZS", "BLCTYD", "20140515170526", "20140816080200", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000156", "BLCTZS", "BLCT3", "20140515170526", "20140816080300", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030358", "BLCTZS", "BLCT2", "20140516175158", "20140816080400", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029270", "BLCT2", "BLCT", "20140514183801", "20140816120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030210", "BLCTYD", "BLCT2", "20140514183801", "20140816120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030301", "BLCT3", "BLCT2", "20140515114430", "20140816120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030295", "BLCT", "BLCT2", "20140515114430", "20140816120000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030294", "BLCT", "BLCT2", "20140515114430", "20140816120000", "0", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029354", "BLCT2", "BLCTMS", "20140516175158", "20140816120000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029331", "BLCTZS", "BLCT2", "20140516160712", "20140816120200", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030299", "BLCT3", "BLCT", "20140515114430", "20140816120300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029313", "BLCT", "BLCT3", "20140515114430", "20140816120300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000154", "BLCTZS", "BLCT3", "20140515114430", "20140816140300", "35", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029260", "BLCT3", "BLCT", "20140514115050", "20140816150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029291", "BLCTZS", "BLCT", "20140515114430", "20140816150200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029344", "BLCT2", "BLCTMS", "20140516175158", "20140816160000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029290", "BLCTZS", "BLCT3", "20140515114430", "20140816160100", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029321", "BLCTZS", "BLCT3", "20140515170526", "20140816170000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030345", "BLCTZS", "BLCTYD", "20140515170526", "20140816170000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030244", "BLCTYD", "BLCT", "20140512160415", "20140816180000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030272", "BLCT3", "BLCT", "20140513165136", "20140816180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030312", "BLCTZS", "BLCT", "20140514172641", "20140816180000", "1", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030311", "BLCT3", "BLCT", "20140514183801", "20140816180000", "2", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030313", "BLCTYD", "BLCT", "20140514183801", "20140816180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030314", "BLCT3", "BLCT", "20140514183801", "20140816180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029284", "BLCTYD", "BLCTMS", "20140514183801", "20140816180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029296", "BLCT", "BLCTYD", "20140515114430", "20140816180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030325", "BLCT", "BLCT3", "20140515170526", "20140816180000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030343", "BLCT", "BLCTMS", "20140515170526", "20140816180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030326", "BLCT", "BLCTMS", "20140515170526", "20140816180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030342", "BLCT", "BLCTMS", "20140515170526", "20140816180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030212", "BLCTMS", "BLCT2", "20140515170526", "20140816180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029322", "BLCTZS", "BLCTYD", "20140515170526", "20140816180000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039227", "BLCT2", "BLCT", "20140512155500", "20140816180100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039233", "BLCT2", "BLCTYD", "20140512155500", "20140816180300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030262", "BLCTZS", "BLCTYD", "20140513165136", "20140816200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029254", "BLCT2", "BLCTYD", "20140513165136", "20140816200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030263", "BLCTYD", "BLCTZS", "20140513165136", "20140816200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030269", "BLCT2", "BLCTZS", "20140513165136", "20140816200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030304", "BLCT", "BLCTZS", "20140514183801", "20140816200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030310", "BLCTMS", "BLCT3", "20140514183801", "20140816200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030319", "BLCTYD", "BLCTZS", "20140515114430", "20140816200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030318", "BLCTYD", "BLCTZS", "20140515114430", "20140816200000", "4", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029343", "BLCT2", "BLCT", "20140516175158", "20140816200000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030333", "BLCTYD", "BLCTMS", "20140516175158", "20140816200000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030381", "BLCT2", "BLCTZS", "20140516175158", "20140816200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029292", "BLCTZS", "BLCT", "20140515114430", "20140816200300", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030334", "BLCT", "BLCT3", "20140515170526", "20140816200300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030360", "BLCTZS", "BLCT2", "20140516175158", "20140816200500", "11", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029339", "BLCTMS", "BLCT3", "20140518073337", "20140816230000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029337", "BLCTMS", "BLCT3", "20140518073337", "20140816230000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030404", "BLCT3", "BLCTZS", "20140518164259", "20140817100000", "5", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000142", "BLCT2", "ZHCT", "20140512155500", "20140817100200", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029179", "BLCT", "BLCT2", "20140510163259", "20140817120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030179", "BLCT", "BLCTMS", "20140510163259", "20140817120000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000146", "BLCT2", "BLCT", "20140514115050", "20140817120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000147", "BLCT2", "BLCT", "20140514115050", "20140817120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000148", "BLCT3", "BLCT", "20140514115050", "20140817120000", "17", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029282", "BLCTYD", "BLCT2", "20140514183801", "20140817120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030284", "BLCT", "BLCT2", "20140515180048", "20140817120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030335", "BLCT3", "BLCTZS", "20140515180048", "20140817120000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029355", "BLCT2", "BLCT", "20140516175158", "20140817120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029350", "BLCT2", "BLCT3", "20140516175158", "20140817120000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030352", "BLCTMS", "BLCT2", "20140517121829", "20140817120000", "6", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030350", "BLCTMS", "BLCT2", "20140517121829", "20140817120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030285", "BLCT", "BLCTYD", "20140515180048", "20140817120100", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030286", "BLCT", "BLCT2", "20140515180048", "20140817120200", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030208", "BLCTMS", "BLCTZS", "20140512155500", "20140817120300", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029353", "BLCT", "BLCTZS", "20140516175158", "20140817140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029352", "BLCTYD", "BLCTZS", "20140516175158", "20140817140000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029338", "BLCTMS", "BLCTZS", "20140517192514", "20140817140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029335", "BLCTMS", "BLCT3", "20140518073337", "20140817140000", "29", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029334", "BLCTZS", "BLCTYD", "20140516160712", "20140817160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029347", "BLCT", "BLCTYD", "20140516175158", "20140817160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029341", "BLCT2", "BLCTMS", "20140516175158", "20140817160000", "2", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029340", "BLCT2", "BLCTYD", "20140516175158", "20140817160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029253", "BLCT2", "BLCT", "20140513165136", "20140817180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030216", "BLCT2", "BLCTZS", "20140514183021", "20140817180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030214", "BLCT2", "BLCTYD", "20140514183801", "20140817180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030215", "BLCT2", "BLCTYD", "20140514183801", "20140817180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030307", "BLCTYD", "BLCT", "20140514183801", "20140817180000", "8", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029306", "BLCT3", "BLCT", "20140515114430", "20140817180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029356", "BLCT2", "BLCT", "20140516175158", "20140817180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029357", "BLCT2", "BLCT", "20140516175158", "20140817180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030347", "BLCTZS", "BLCT3", "20140516175158", "20140817180000", "16", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030338", "BLCTZS", "BLCT3", "20140516175158", "20140817180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030376", "BLCT", "BLCT2", "20140518131935", "20140817180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029323", "BLCTZS", "BLCTYD", "20140515170526", "20140817180100", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029315", "BLCTZS", "BLCT", "20140515170526", "20140817180200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000158", "DXCTE", "BLCTZS", "20140516160712", "20140817180200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030192", "BLCTMS", "BLCT2", "20140509173025", "20140817200000", "33", "37
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029274", "BLCT2", "BLCT", "20140514183801", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030293", "BLCT", "BLCT2", "20140514183801", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030296", "BLCT", "BLCT2", "20140514183801", "20140817200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030317", "BLCTYD", "BLCTZS", "20140514183801", "20140817200000", "17", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029364", "BLCTZS", "BLCTYD", "20140516160712", "20140817200000", "12", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029336", "BLCT2", "BLCTYD", "20140516160712", "20140817200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030384", "BLCT3", "BLCT2", "20140516160712", "20140817200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029363", "BLCT2", "BLCT3", "20140516160712", "20140817200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029326", "BLCTYD", "BLCTMS", "20140515170526", "20140817200200", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030292", "BLCTMS", "BLCTZS", "20140515170526", "20140817200200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030242", "BLCTMS", "BLCT2", "20140512155500", "20140817200300", "105", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029167", "BLCT2", "BLCT", "20140509173025", "20140817200500", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030302", "BLCTMS", "BLCT2", "20140514183801", "20140817210000", "36", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029371", "BLCT3", "BLCTMS", "20140517164436", "20140817230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030383", "BLCT", "BLCT2", "20140516160712", "20140817230200", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030382", "BLCT", "BLCT2", "20140516160712", "20140817230200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030371", "BLCT2", "BLCTZS", "20140516160712", "20140817230200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000159", "ZHCT", "BLCT2", "20140516160712", "20140817230200", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000167", "BLCT3", "BLCT2", "20140517164436", "20140818010000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029348", "BLCTMS", "BLCTZS", "20140516175158", "20140818080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029368", "BLCT3", "BLCTZS", "20140517164436", "20140818080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039208", "BLCT2", "BLCTYD", "20140512160415", "20140818080100", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000163", "BLCT2", "BLCTZS", "20140517164436", "20140818090200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000166", "BLCT2", "BLCTYD", "20140517164436", "20140818090200", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030045", "BLCT2", "BLCTZS", "20140505164914", "20140818120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030236", "BLCT2", "BLCTYD", "20140512155500", "20140818120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030331", "BLCT", "BLCT2", "20140515114430", "20140818120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000161", "BLCTZS", "BLCT2", "20140516175158", "20140818120000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029392", "BLCT2", "BLCT3", "20140518164259", "20140818120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030300", "BLCT3", "BLCT2", "20140515114430", "20140818120100", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039224", "BLCTZS", "BLCT3", "20140512155500", "20140818120200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029330", "BLCTZS", "BLCT", "20140516160712", "20140818120200", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029312", "BLCT", "BLCT3", "20140515114430", "20140818120400", "1", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030237", "BLCT2", "BLCTZS", "20140512155500", "20140818120900", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030248", "BLCT2", "BLCTZS", "20140512155500", "20140818160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030199", "BLCT2", "BLCT", "20140513165136", "20140818160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029300", "BLCTZS", "BLCTYD", "20140515114430", "20140818160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029360", "BLCT2", "BLCT3", "20140516175158", "20140818160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029345", "BLCT2", "BLCT", "20140516175158", "20140818160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029342", "BLCT2", "BLCT", "20140516175158", "20140818160000", "18", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029370", "BLCT3", "BLCTZS", "20140517164436", "20140818160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029301", "BLCTZS", "BLCT", "20140515114430", "20140818160100", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029302", "BLCTZS", "BLCT2", "20140515114430", "20140818160200", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000157", "DXCTE", "BLCTZS", "20140516160712", "20140818160200", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000168", "BLCT2", "BLCT", "20140517164436", "20140818160200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030309", "BLCTYD", "BLCT", "20140514183801", "20140818180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030211", "BLCTMS", "BLCT2", "20140515170526", "20140818180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029324", "BLCTZS", "BLCTYD", "20140515170526", "20140818180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030336", "BLCTZS", "BLCT2", "20140516175158", "20140818180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030398", "BLCTYD", "BLCTZS", "20140518164259", "20140818180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030386", "BLCT2", "BLCTYD", "20140518164259", "20140818180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030385", "BLCT2", "BLCTZS", "20140518164259", "20140818180000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030340", "BLCTZS", "BLCTYD", "20140515170526", "20140818180400", "2", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030264", "BLCTYD", "BLCTZS", "20140513165136", "20140818200000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029361", "BLCT2", "BLCTZS", "20140516160712", "20140818200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029362", "BLCTYD", "BLCTZS", "20140516160712", "20140818200000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030355", "BLCT3", "BLCT2", "20140516175158", "20140818200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029376", "BLCT2", "BLCT", "20140517164436", "20140818200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029373", "BLCT3", "BLCTZS", "20140518164259", "20140818200000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029365", "BLCT2", "BLCTYD", "20140518164259", "20140818200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029372", "BLCT3", "BLCTZS", "20140518164259", "20140818200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029367", "BLCTZS", "BLCTYD", "20140518164259", "20140818200100", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030354", "BLCTMS", "BLCTZS", "20140515170526", "20140818200300", "23", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030353", "BLCTMS", "BLCTZS", "20140515170526", "20140818200300", "23", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030392", "BLCT3", "BLCTMS", "20140518164259", "20140818200300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030357", "BLCTYD", "BLCTZS", "20140518164259", "20140818230500", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030393", "BLCTYD", "BLCTMS", "20140518164259", "20140819080000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030394", "BLCTYD", "BLCTMS", "20140518164259", "20140819080000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030339", "BLCTZS", "BLCTYD", "20140515170526", "20140819080300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12029218", "BLCT2", "BLCTYD", "20140512185530", "20140819100000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029288", "BLCT", "BLCTYD", "20140516175158", "20140819120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029383", "BLCT2", "BLCTZS", "20140517164436", "20140819120000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029382", "BLCT2", "BLCT3", "20140517164436", "20140819120000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030403", "BLCTZS", "BLCT2", "20140518164259", "20140819120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029304", "BLCT3", "BLCTZS", "20140515114430", "20140819120100", "20", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039237", "BLCT3", "BLCTZS", "20140512155500", "20140819120500", "17", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039231", "BLCT2", "BLCTZS", "20140512155500", "20140819120600", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029349", "BLCTZS", "BLCT3", "20140516175158", "20140819150000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029346", "BLCT2", "BLCT", "20140516175158", "20140819160000", "2", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030346", "BLCTYD", "BLCTZS", "20140515170526", "20140819180000", "5", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029358", "BLCT2", "BLCT", "20140516175158", "20140819180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029369", "BLCT3", "BLCTZS", "20140517164436", "20140819180000", "10", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029378", "BLCT2", "BLCT", "20140517164436", "20140819180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029380", "BLCT2", "BLCT", "20140517164436", "20140819180000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029415", "BLCT2", "BLCTYD", "20140519164731", "20140819180000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029416", "BLCTZS", "BLCTYD", "20140519164731", "20140819180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029401", "BLCT2", "BLCTYD", "20140519164731", "20140819180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029414", "BLCTZS", "BLCTYD", "20140519164731", "20140819180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029411", "BLCT2", "BLCTYD", "20140519164731", "20140819180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030397", "BLCT3", "BLCTZS", "20140518164259", "20140819180200", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029317", "BLCTZS", "BLCT", "20140515170526", "20140819180300", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029400", "BLCT2", "BLCTZS", "20140519164731", "20140819190000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030344", "BLCT2", "BLCTYD", "20140516175158", "20140819200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030105", "BLCTMS", "BLCT2", "20140517121829", "20140819200000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030402", "BLCTYD", "BLCT2", "20140517164436", "20140819200000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030373", "BLCTMS", "BLCTZS", "20140517192514", "20140819200000", "18", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029399", "BLCT2", "BLCT3", "20140519112343", "20140819220000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029398", "BLCT2", "BLCT3", "20140519112343", "20140819220000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029397", "BLCT2", "BLCT3", "20140519112343", "20140819220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000171", "BLCT2", "BLCT", "20140519112343", "20140819230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030410", "BLCTYD", "BLCTZS", "20140519164731", "20140819230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000162", "BLCTYD", "BLCT2", "20140517164436", "20140820010000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029437", "BLCT3", "BLCTZS", "20140521121519", "20140820010000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029305", "BLCT3", "BLCTZS", "20140515114430", "20140820060000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000172", "BLCT2", "BLCT", "20140519112343", "20140820060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030405", "BLCT", "BLCT3", "20140519154246", "20140820060000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030268", "BLCTMS", "BLCTYD", "20140515170526", "20140820080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030337", "BLCTMS", "BLCT3", "20140515170526", "20140820080000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029333", "BLCT", "BLCT2", "20140516175158", "20140820080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029332", "BLCT", "BLCT2", "20140516175158", "20140820080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029395", "BLCT", "BLCT2", "20140519164731", "20140820080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030306", "BLCTMS", "BLCT", "20140515170526", "20140820080100", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030341", "BLCT3", "BLCTZS", "20140515170526", "20140820080100", "16", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029409", "BLCT3", "BLCT", "20140519154246", "20140820100000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029295", "BLCT", "BLCTYD", "20140515114430", "20140820120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030330", "BLCT", "BLCT2", "20140515114430", "20140820120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029391", "BLCTZS", "BLCT2", "20140518164259", "20140820120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029388", "BLCTZS", "BLCT2", "20140518164259", "20140820120000", "17", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030379", "BLCT2", "BLCT", "20140519164731", "20140820120000", "27", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030369", "BLCT2", "BLCT", "20140519164731", "20140820120000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030427", "BLCT", "BLCT2", "20140519164731", "20140820120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029406", "BLCT3", "BLCTMS", "20140519164731", "20140820120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029396", "BLCT2", "BLCTYD", "20140519164731", "20140820120000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030367", "BLCT2", "BLCTYD", "20140519164731", "20140820120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030380", "BLCT2", "BLCT", "20140519164731", "20140820120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029389", "BLCTZS", "BLCT2", "20140518164259", "20140820120100", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000164", "BLCT2", "BLCT", "20140517164436", "20140820120200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000165", "BLCT2", "BLCT3", "20140517164436", "20140820120200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029387", "BLCTZS", "BLCT2", "20140518164259", "20140820120200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030408", "BLCTYD", "BLCT2", "20140519164731", "20140820150000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029393", "BLCT2", "BLCT3", "20140519164731", "20140820150000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029294", "BLCTZS", "BLCTYD", "20140515114430", "20140820150400", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029297", "BLCTZS", "BLCT2", "20140515114430", "20140820160300", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029319", "BLCT2", "BLCT", "20140515170526", "20140820160400", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029264", "BLCT2", "BLCT", "20140514183801", "20140820180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029287", "BLCT2", "BLCT", "20140514183801", "20140820180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030308", "BLCTYD", "BLCT", "20140514183801", "20140820180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029307", "BLCT3", "BLCT", "20140515114430", "20140820180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029359", "BLCT2", "BLCT", "20140516175158", "20140820180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029366", "BLCT2", "BLCTYD", "20140517164436", "20140820180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029384", "BLCT2", "BLCT3", "20140517164436", "20140820180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029375", "BLCT2", "BLCT", "20140517164436", "20140820180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029374", "BLCT3", "BLCTMS", "20140517164436", "20140820180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029377", "BLCT2", "BLCT", "20140517164436", "20140820180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030412", "BLCT3", "BLCT", "20140519164731", "20140820180000", "2", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030414", "BLCT3", "BLCT", "20140519164731", "20140820180000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030413", "BLCTYD", "BLCT", "20140519164731", "20140820180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030407", "BLCTZS", "BLCTYD", "20140519164731", "20140820180000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030415", "BLCTZS", "BLCT3", "20140519164731", "20140820180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030411", "BLCTYD", "BLCT", "20140519164731", "20140820180000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000176", "BLCT2", "BLCT3", "20140519172831", "20140820180000", "16", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029435", "BLCT3", "BLCTMS", "20140521163300", "20140820180300", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000174", "BLCTZS", "BLCT2", "20140519154246", "20140820200000", "0", "18
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030372", "BLCTZS", "BLCT2", "20140519154246", "20140820200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029419", "BLCT", "BLCTZS", "20140520172237", "20140820200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029418", "BLCT", "BLCTZS", "20140520172237", "20140820200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029430", "BLCT2", "BLCT3", "20140520172237", "20140820200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029431", "BLCTZS", "BLCT3", "20140520172237", "20140820200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030359", "BLCTZS", "BLCT2", "20140516175158", "20140820200500", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030395", "BLCTZS", "BLCT2", "20140518164259", "20140820200600", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030401", "BLCTYD", "BLCT2", "20140517164436", "20140820210000", "4", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000155", "BLCTZS", "ZHCT", "20140515170526", "20140820220300", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000160", "ZHCT", "BLCT2", "20140516160712", "20140820230200", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030499", "BLCTYD", "BLCTZS", "20140522173155", "20140820230300", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000175", "BLCTZS", "DXCTE", "20140519164731", "20140821010000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030434", "BLCTMS", "BLCT3", "20140521121519", "20140821080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000169", "BLCT3", "ZHCT", "20140517164436", "20140821100000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030351", "BLCTMS", "BLCT2", "20140517121829", "20140821120000", "6", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030378", "BLCT", "BLCT2", "20140518131935", "20140821120000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029408", "BLCT3", "BLCT", "20140519154246", "20140821120000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029410", "BLCT3", "BLCT2", "20140519154246", "20140821120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000179", "BLCT", "ZHCT", "20140520172237", "20140821120000", "12", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000178", "BLCTZS", "BLCT", "20140520172237", "20140821120000", "26", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000177", "BLCT3", "BLCT", "20140520172237", "20140821120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030446", "BLCT2", "BLCT3", "20140521163300", "20140821120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030396", "BLCT3", "BLCT2", "20140518164259", "20140821121000", "9", "35
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030421", "BLCTZS", "BLCT2", "20140519164731", "20140821130000", "13", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030454", "BLCTZS", "BLCT2", "20140520172237", "20140821140000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029385", "BLCT2", "BLCT3", "20140517164436", "20140821150000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000170", "BLCTZS", "DXCTE", "20140519112343", "20140821160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029428", "BLCT", "BLCT2", "20140520172237", "20140821160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030440", "BLCT2", "BLCTZS", "20140520172237", "20140821160000", "0", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030425", "BLCT2", "BLCT3", "20140520172237", "20140821160000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029434", "BLCT3", "BLCTMS", "20140521163300", "20140821160400", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029381", "BLCT2", "BLCT", "20140517164436", "20140821180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030377", "BLCT", "BLCT2", "20140518164259", "20140821180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029449", "BLCTYD", "BLCTMS", "20140521163300", "20140821180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029314", "BLCTZS", "BLCT2", "20140515170526", "20140821180100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029452", "BLCTYD", "BLCTMS", "20140521163300", "20140821180100", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030470", "BLCT", "BLCT2", "20140521163300", "20140821180200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029451", "BLCTYD", "BLCTMS", "20140521163300", "20140821180200", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030400", "BLCT2", "BLCT3", "20140517164436", "20140821200000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030422", "BLCT", "BLCT2", "20140519154246", "20140821200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030424", "BLCT", "BLCT2", "20140519154246", "20140821200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029432", "BLCT3", "BLCT2", "20140520172237", "20140821200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000223", "DXCTE", "BLCTZS", "20140529115808", "20140821200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030428", "BLCTMS", "BLCT", "20140521163300", "20140821200400", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000185", "BLCT2", "BLCT", "20140521121519", "20140822060000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029420", "BLCT3", "BLCT2", "20140520172237", "20140822080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029422", "BLCT3", "BLCT", "20140520172237", "20140822080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029441", "BLCTZS", "BLCT3", "20140521163300", "20140822080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029443", "BLCTZS", "BLCT3", "20140521163300", "20140822080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030462", "BLCT3", "BLCTZS", "20140521163300", "20140822080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029438", "BLCT2", "BLCT", "20140521163300", "20140822080000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029439", "BLCT2", "BLCT", "20140521163300", "20140822080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029440", "BLCT2", "BLCT", "20140521163300", "20140822080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030463", "BLCTYD", "BLCTZS", "20140521163300", "20140822080100", "12", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029436", "BLCTZS", "BLCT3", "20140521163300", "20140822080100", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029433", "BLCTMS", "BLCT3", "20140521163300", "20140822080300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029457", "BLCT2", "BLCT", "20140521163300", "20140822080500", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029390", "BLCTZS", "BLCT2", "20140518164259", "20140822120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030391", "BLCT", "BLCT2", "20140519164731", "20140822120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030363", "BLCT2", "BLCT", "20140519164731", "20140822120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030442", "BLCT2", "BLCTYD", "20140520172237", "20140822120000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030426", "BLCTMS", "BLCT", "20140520172237", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030423", "BLCTMS", "BLCT", "20140520172237", "20140822120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030416", "BLCTMS", "BLCT", "20140520172237", "20140822120000", "20", "27
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030443", "BLCT2", "BLCTYD", "20140520172237", "20140822120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030444", "BLCT2", "BLCT3", "20140520172237", "20140822120000", "0", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030445", "BLCT2", "BLCT3", "20140520172237", "20140822120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030448", "BLCT2", "BLCT3", "20140520172237", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029417", "BLCT3", "BLCT", "20140520172237", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030449", "BLCT3", "BLCT2", "20140520172237", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030447", "BLCTYD", "BLCT", "20140520172237", "20140822120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000184", "BLCT2", "BLCT3", "20140521121519", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000182", "BLCT2", "BLCT3", "20140521121519", "20140822120000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029446", "BLCT2", "BLCT", "20140521163300", "20140822120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030469", "BLCT2", "BLCT", "20140521163300", "20140822120000", "2", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029445", "BLCT2", "BLCT", "20140521163300", "20140822120100", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030474", "BLCT3", "BLCT2", "20140521163300", "20140822120200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029386", "BLCT2", "BLCT", "20140517164436", "20140822150000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030200", "BLCT2", "BLCT", "20140520172237", "20140822160000", "17", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030406", "BLCT2", "BLCT", "20140520172237", "20140822160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029424", "BLCTZS", "BLCT2", "20140520172237", "20140822160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030298", "BLCTMS", "BLCT", "20140521163300", "20140822160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030418", "BLCT", "BLCT2", "20140521163300", "20140822160100", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030458", "BLCT", "BLCT2", "20140522173155", "20140822160300", "2", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029465", "BLCT2", "BLCT", "20140522173155", "20140822160300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030348", "BLCTMS", "BLCT2", "20140517121829", "20140822180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030450", "BLCTYD", "BLCT", "20140520172237", "20140822180000", "19", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030451", "BLCTYD", "BLCT", "20140520172237", "20140822180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030453", "BLCTYD", "BLCTZS", "20140520172237", "20140822180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000180", "ZHCT", "BLCTZS", "20140521121519", "20140822180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029442", "BLCTZS", "BLCT3", "20140521163300", "20140822180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029512", "BLCT2", "BLCT", "20140523173049", "20140822180000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029453", "BLCTYD", "BLCTZS", "20140521163300", "20140822180100", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030471", "BLCT", "BLCT2", "20140521163300", "20140822180200", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029455", "BLCT2", "BLCT", "20140521163300", "20140822180300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029448", "BLCTZS", "BLCTYD", "20140521163300", "20140822180400", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029456", "BLCT2", "BLCT", "20140521163300", "20140822180400", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029458", "BLCT2", "BLCT", "20140521163300", "20140822180600", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030399", "BLCT2", "BLCTYD", "20140517164436", "20140822200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029421", "BLCT3", "BLCT2", "20140520172237", "20140822200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030441", "BLCTYD", "BLCT", "20140520172237", "20140822200000", "15", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000186", "BLCT", "BLCT2", "20140521165055", "20140822200000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030482", "BLCTMS", "BLCTZS", "20140523173049", "20140822200000", "67", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030464", "BLCTYD", "BLCT2", "20140521163300", "20140822200200", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029477", "BLCT", "BLCT2", "20140522173155", "20140822200300", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029461", "BLCTYD", "BLCT2", "20140522173155", "20140822200300", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029471", "BLCTYD", "BLCTZS", "20140522173155", "20140822200300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029469", "BLCT2", "BLCTZS", "20140522173155", "20140822200300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029472", "BLCT2", "BLCTZS", "20140522173155", "20140822200300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029473", "BLCT2", "BLCTZS", "20140522173155", "20140822200300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030515", "BLCT", "BLCT2", "20140522173155", "20140822230300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030505", "BLCTYD", "BLCTZS", "20140522173155", "20140822230300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030497", "BLCTYD", "BLCTZS", "20140522173155", "20140822230300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030435", "BLCTMS", "BLCT3", "20140521121519", "20140823080000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030436", "BLCTMS", "BLCT3", "20140521121519", "20140823080000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030481", "BLCTYD", "BLCTZS", "20140521163300", "20140823080200", "10", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030455", "BLCTZS", "BLCTYD", "20140521163300", "20140823080300", "3", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000190", "BLCT2", "BLCTZS", "20140522095315", "20140823080300", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030509", "BLCT", "BLCT2", "20140522173155", "20140823100300", "26", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000173", "BLCT2", "ZHCT", "20140519154246", "20140823120000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030389", "BLCT", "BLCTMS", "20140519164731", "20140823120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030368", "BLCT2", "BLCT", "20140519164731", "20140823120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030361", "BLCT2", "BLCTYD", "20140519164731", "20140823120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030388", "BLCT", "BLCTMS", "20140519164731", "20140823120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029429", "BLCTMS", "BLCT3", "20140521163300", "20140823120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030459", "BLCT", "BLCT3", "20140521163300", "20140823120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030460", "BLCT", "BLCTMS", "20140521163300", "20140823120000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030461", "BLCT", "BLCTMS", "20140521163300", "20140823120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030468", "BLCT2", "BLCTZS", "20140521163300", "20140823120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030063", "BLCTYD", "BLCT2", "20140522082128", "20140823120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030433", "BLCTYD", "BLCT2", "20140523173049", "20140823120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029426", "BLCTMS", "BLCT3", "20140521163300", "20140823120100", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029427", "BLCTMS", "BLCT3", "20140521163300", "20140823120200", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029444", "BLCTYD", "BLCTZS", "20140521163300", "20140823120200", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030502", "BLCT3", "BLCT", "20140522173155", "20140823120300", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029481", "BLCT2", "BLCTYD", "20140522173155", "20140823120300", "18", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029480", "BLCT2", "BLCTYD", "20140522173155", "20140823120300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029479", "BLCT3", "BLCTZS", "20140522173155", "20140823120300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029478", "BLCT2", "BLCT", "20140522173155", "20140823120300", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029488", "BLCT", "BLCT2", "20140522173155", "20140823120300", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029487", "BLCT3", "BLCT2", "20140522173155", "20140823140300", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030500", "BLCT", "BLCT3", "20140522173155", "20140823140300", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030503", "BLCTYD", "BLCT", "20140522173155", "20140823140300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030498", "BLCTYD", "BLCTZS", "20140522173155", "20140823140300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029467", "BLCT2", "BLCT3", "20140522173155", "20140823150300", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029470", "BLCT2", "BLCT3", "20140522173155", "20140823150300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029476", "BLCT2", "BLCTYD", "20140522173155", "20140823150300", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030419", "BLCT", "BLCT2", "20140521163300", "20140823160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029459", "BLCT2", "BLCT", "20140521163300", "20140823160200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029460", "BLCT2", "BLCT", "20140521163300", "20140823160300", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029447", "BLCT3", "BLCTZS", "20140521163300", "20140823160300", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000191", "BLCTZS", "BLCT2", "20140522173155", "20140823160300", "26", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030512", "BLCTYD", "BLCT", "20140522173155", "20140823160300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030486", "BLCTZS", "BLCT", "20140522173155", "20140823160300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030420", "BLCT", "BLCT2", "20140522173155", "20140823160300", "43", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029500", "BLCT3", "BLCT", "20140522173155", "20140823160300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029486", "BLCT3", "BLCTMS", "20140522173155", "20140823160300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030375", "BLCT", "BLCTMS", "20140518164259", "20140823180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029413", "BLCT3", "BLCT", "20140519164731", "20140823180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030364", "BLCT2", "BLCTMS", "20140519164731", "20140823180000", "3", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030417", "BLCTZS", "BLCT", "20140519164731", "20140823180000", "3", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030437", "BLCT2", "BLCTZS", "20140520172237", "20140823180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000187", "BLCT2", "ZHCT", "20140522024836", "20140823180000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029514", "BLCT2", "BLCT", "20140523173049", "20140823180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029533", "BLCT", "BLCT3", "20140524170337", "20140823180000", "0", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030626", "BLCTYD", "BLCT", "20140528181030", "20140823180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030630", "BLCT3", "BLCT", "20140528181114", "20140823180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030628", "BLCT3", "BLCT", "20140528181114", "20140823180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030627", "BLCT3", "BLCT", "20140528181114", "20140823180000", "14", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029454", "BLCTYD", "BLCTZS", "20140521163300", "20140823180200", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030472", "BLCT", "BLCTZS", "20140521163300", "20140823180200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030488", "BLCTYD", "BLCT", "20140522173155", "20140823180300", "3", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030490", "BLCT2", "BLCT", "20140522173155", "20140823180300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030492", "BLCTYD", "BLCT", "20140522173155", "20140823180300", "3", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030491", "BLCTYD", "BLCT", "20140522173155", "20140823180300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030487", "BLCTZS", "BLCT", "20140522173155", "20140823180300", "22", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030489", "BLCTZS", "BLCT3", "20140522173155", "20140823180300", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029483", "BLCTZS", "BLCTYD", "20140522173155", "20140823180300", "7", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030466", "BLCT2", "BLCTZS", "20140522173155", "20140823180300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029492", "BLCT", "BLCTYD", "20140522173155", "20140823180300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030362", "BLCT", "BLCTMS", "20140519164731", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029504", "BLCT2", "BLCT3", "20140523173049", "20140823200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029462", "BLCT2", "BLCT3", "20140522173155", "20140823200300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029450", "BLCTZS", "BLCTYD", "20140521163300", "20140823200400", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000192", "BLCT2", "ZHCT", "20140522173155", "20140823230300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029506", "BLCT2", "BLCT", "20140523173049", "20140824060000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030529", "BLCT3", "BLCT", "20140523173049", "20140824100000", "4", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029407", "BLCT3", "BLCT2", "20140519154246", "20140824120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030365", "BLCT2", "BLCTMS", "20140519164731", "20140824120000", "16", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000183", "BLCT2", "BLCT", "20140521121519", "20140824120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030452", "BLCTZS", "BLCTYD", "20140521163300", "20140824120000", "0", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029515", "BLCT3", "BLCT2", "20140523173049", "20140824120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029522", "BLCT2", "BLCTZS", "20140523173049", "20140824120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029528", "BLCT", "BLCT2", "20140523173049", "20140824120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029527", "BLCT", "BLCT2", "20140523173049", "20140824120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029516", "BLCT", "BLCT2", "20140523173049", "20140824120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000195", "DXCTE", "BLCTZS", "20140524063647", "20140824120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030328", "BLCT3", "BLCT2", "20140515114430", "20140824120200", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030366", "BLCT", "BLCTYD", "20140522173155", "20140824120300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029499", "BLCT", "BLCT3", "20140522173155", "20140824120300", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000193", "DXCTE", "BLCT3", "20140523184708", "20140824120300", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000194", "DXCTE", "BLCT3", "20140523184708", "20140824120300", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029521", "BLCTYD", "BLCTZS", "20140523173049", "20140824140000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029519", "BLCT", "BLCTZS", "20140523173049", "20140824140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029520", "BLCT3", "BLCTZS", "20140523173049", "20140824140000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030537", "BLCT2", "BLCT3", "20140523173049", "20140824140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029394", "BLCT2", "BLCTMS", "20140519164731", "20140824150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029466", "BLCT2", "BLCT3", "20140522173155", "20140824150300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029474", "BLCT2", "BLCT", "20140522173155", "20140824150300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029475", "BLCT2", "BLCT", "20140522173155", "20140824150300", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029484", "BLCT2", "BLCT3", "20140522173155", "20140824150300", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030198", "BLCT2", "BLCTYD", "20140520172237", "20140824160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029526", "BLCT3", "BLCTMS", "20140523173049", "20140824160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030432", "BLCTYD", "BLCT2", "20140522173155", "20140824160300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030374", "BLCT", "BLCTMS", "20140518164259", "20140824180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030513", "BLCTYD", "BLCTZS", "20140523173049", "20140824180000", "5", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029505", "BLCT2", "BLCT", "20140523173049", "20140824180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029523", "BLCTZS", "BLCT3", "20140523173049", "20140824180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000196", "BLCTZS", "BLCT3", "20140524170337", "20140824180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000197", "BLCTZS", "ZHCT", "20140524170337", "20140824180000", "0", "26
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030478", "BLCTYD", "BLCT", "20140521163300", "20140824180300", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030493", "BLCTYD", "BLCT", "20140522173155", "20140824180300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029482", "BLCTZS", "BLCT3", "20140522173155", "20140824180300", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030387", "BLCTYD", "BLCT", "20140516175158", "20140824200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030516", "BLCTZS", "BLCTYD", "20140523173049", "20140824200000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000203", "BLCTYD", "BLCT2", "20140524150945", "20140824200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000204", "BLCTYD", "BLCT2", "20140524150945", "20140824200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029535", "BLCT2", "BLCT3", "20140524170337", "20140824200000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029493", "BLCT", "BLCTYD", "20140522173155", "20140824200300", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029489", "BLCT", "BLCT3", "20140522173155", "20140824200300", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029530", "BLCT", "BLCTZS", "20140524170337", "20140825060000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029518", "BLCT", "BLCTYD", "20140523173049", "20140825080000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030473", "BLCT", "BLCT2", "20140521163300", "20140825080200", "3", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030349", "BLCTMS", "BLCT2", "20140517121829", "20140825120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030390", "BLCT", "BLCT2", "20140519164731", "20140825120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030356", "BLCT", "BLCT2", "20140519164731", "20140825120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030370", "BLCT2", "BLCTYD", "20140519164731", "20140825120000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029513", "BLCT2", "BLCT", "20140523173049", "20140825120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030456", "BLCT", "BLCT2", "20140522173155", "20140825120300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029498", "BLCT", "BLCT3", "20140522173155", "20140825140300", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029468", "BLCT2", "BLCTZS", "20140522173155", "20140825150300", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030201", "BLCT2", "BLCTYD", "20140520172237", "20140825160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029423", "BLCT3", "BLCT", "20140520172237", "20140825160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029525", "BLCT3", "BLCTMS", "20140523173049", "20140825160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029538", "BLCT2", "BLCT", "20140524170337", "20140825160000", "32", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029536", "BLCT2", "BLCTYD", "20140524170337", "20140825160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030507", "BLCT2", "BLCTZS", "20140522173155", "20140825160300", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030506", "BLCT2", "BLCTZS", "20140522173155", "20140825160300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030467", "BLCTYD", "BLCT2", "20140521163300", "20140825180000", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030535", "BLCTYD", "BLCT", "20140523173049", "20140825180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029508", "BLCT2", "BLCT", "20140523173049", "20140825180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030527", "BLCT3", "BLCT2", "20140523173049", "20140825180000", "8", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029532", "BLCT", "BLCT2", "20140524170337", "20140825180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030409", "BLCTYD", "BLCTZS", "20140519164731", "20140825200000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030494", "BLCTMS", "BLCTZS", "20140523173049", "20140825200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030495", "BLCTMS", "BLCTZS", "20140523173049", "20140825200000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030485", "BLCTMS", "BLCTZS", "20140523173049", "20140825200000", "3", "27
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030526", "BLCTYD", "BLCTZS", "20140523173049", "20140825200000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030525", "BLCTYD", "BLCTZS", "20140523173049", "20140825200000", "0", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030517", "BLCT", "BLCTYD", "20140523173049", "20140825200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030483", "BLCTMS", "BLCTZS", "20140523173049", "20140825200000", "27", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000201", "BLCTZS", "ZHCT", "20140524170337", "20140825200000", "12", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000202", "BLCT3", "ZHCT", "20140524170337", "20140825200000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030484", "BLCTMS", "BLCTZS", "20140525210406", "20140825200000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029547", "BLCTZS", "BLCT", "20140525145416", "20140826040000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029548", "BLCTZS", "BLCT", "20140525145416", "20140826040000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029555", "BLCTZS", "BLCT2", "20140525145416", "20140826040000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029531", "BLCT", "BLCTZS", "20140524170337", "20140826060000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029552", "BLCTZS", "BLCT2", "20140525145416", "20140826060000", "5", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029553", "BLCTZS", "BLCT2", "20140525145416", "20140826060000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029412", "BLCT3", "BLCTZS", "20140519164731", "20140826120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000198", "BLCT2", "BLCT3", "20140524170337", "20140826120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000199", "BLCT2", "BLCTYD", "20140524170337", "20140826120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T0000200", "BLCT2", "BLCTZS", "20140524170337", "20140826120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029529", "BLCT", "BLCTZS", "20140524170337", "20140826120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029556", "BLCT2", "BLCTYD", "20140525145416", "20140826120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030431", "BLCT2", "BLCTMS", "20140525145416", "20140826120000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029537", "BLCT2", "BLCT3", "20140525145416", "20140826120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029494", "BLCT", "BLCTZS", "20140522173155", "20140826120300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029496", "BLCT", "BLCTYD", "20140522173155", "20140826140300", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029501", "BLCT", "BLCT3", "20140522173155", "20140826140300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030504", "BLCTYD", "BLCT", "20140522173155", "20140826140300", "9", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029539", "BLCT3", "BLCTZS", "20140524170337", "20140826150000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸0000201", "BLCT2", "ZHCT", "20140525145416", "20140826160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029554", "BLCTZS", "BLCT2", "20140525145416", "20140826160000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029550", "BLCTZS", "BLCT", "20140525145416", "20140826160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030476", "BLCTMS", "BLCT2", "20140527164953", "20140826160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029485", "BLCT3", "BLCTMS", "20140522173155", "20140826160300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030518", "BLCT", "BLCTYD", "20140523173049", "20140826180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029509", "BLCT2", "BLCT", "20140523173049", "20140826180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029490", "BLCT", "BLCT3", "20140522173155", "20140826180300", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030430", "BLCTMS", "BLCT3", "20140521163300", "20140826200400", "14", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030429", "BLCTMS", "BLCT2", "20140521163300", "20140826200400", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030534", "BLCTYD", "BLCT", "20140523173049", "20140826210000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029567", "BLCT2", "BLCTYD", "20140526180524", "20140826230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029565", "BLCT", "BLCT2", "20140526180524", "20140827060000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029517", "BLCT", "BLCT2", "20140523173049", "20140827080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029524", "BLCT3", "BLCTMS", "20140524170337", "20140827120000", "15", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029534", "BLCT2", "BLCTYD", "20140525145416", "20140827120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030523", "BLCT", "BLCT2", "20140525145416", "20140827120000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T0000205", "BLCT2", "BLCT", "20140526153536", "20140827120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030542", "BLCTYD", "BLCT", "20140526180524", "20140827120000", "7", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029560", "BLCT3", "BLCT2", "20140526180524", "20140827120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029562", "BLCT3", "BLCT2", "20140526180524", "20140827120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030556", "BLCT3", "BLCT2", "20140526180524", "20140827120000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030555", "BLCT2", "BLCTZS", "20140526180524", "20140827120000", "40", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029563", "BLCT3", "BLCT", "20140526180524", "20140827120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030457", "BLCT", "BLCT2", "20140522173155", "20140827120300", "2", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029463", "BLCTZS", "BLCT2", "20140522173155", "20140827120300", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029464", "BLCTZS", "BLCT2", "20140522173155", "20140827120300", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030578", "BLCTZS", "BLCTYD", "20140526180524", "20140827130000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029568", "BLCTZS", "BLCTYD", "20140526180524", "20140827140000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029569", "BLCTZS", "BLCTYD", "20140526180524", "20140827140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029502", "BLCT", "BLCT3", "20140522173155", "20140827140300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029503", "BLCT", "BLCT3", "20140522173155", "20140827140300", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030508", "BLCT3", "BLCT2", "20140522173155", "20140827140300", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029495", "BLCT", "BLCT2", "20140522173155", "20140827140300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029541", "BLCT3", "BLCT", "20140524170337", "20140827150000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029549", "BLCTZS", "BLCT", "20140525145416", "20140827160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029551", "BLCTZS", "BLCT", "20140525145416", "20140827160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029566", "BLCT3", "BLCTMS", "20140526180524", "20140827160000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029559", "BLCT2", "BLCT", "20140526180524", "20140827160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030480", "BLCTMS", "BLCT3", "20140523173049", "20140827180000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030479", "BLCTMS", "BLCT", "20140524065351", "20140827180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030553", "BLCTZS", "BLCT2", "20140524170337", "20140827180000", "7", "25
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030569", "BLCT", "BLCT3", "20140526180524", "20140827180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030577", "BLCT", "BLCTYD", "20140526180524", "20140827180000", "17", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030576", "BLCTYD", "BLCTMS", "20140526180524", "20140827180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030568", "BLCTYD", "BLCT2", "20140526180524", "20140827180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030563", "BLCTZS", "BLCT3", "20140526180524", "20140827180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030564", "BLCT3", "BLCT", "20140526180524", "20140827180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030524", "BLCTYD", "BLCTZS", "20140523173049", "20140827200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030572", "BLCT3", "BLCT2", "20140526180524", "20140827200000", "4", "42
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030566", "BLCT3", "BLCT", "20140526180524", "20140827200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029614", "BLCT2", "BLCT3", "20140527164953", "20140827200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030595", "BLCTMS", "BLCT2", "20140528181114", "20140827200000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029491", "BLCT", "BLCTYD", "20140522173155", "20140827200300", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030589", "BLCTZS", "BLCTMS", "20140527130604", "20140828070000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029561", "BLCT3", "BLCT2", "20140526180524", "20140828080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030558", "BLCT2", "BLCT3", "20140526180524", "20140828080000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030593", "BLCT", "BLCT3", "20140527164953", "20140828080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030591", "BLCT", "BLCT3", "20140527164953", "20140828080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039595", "BLCTZS", "BLCT3", "20140527164953", "20140828080000", "1", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039594", "BLCTZS", "BLCT3", "20140527164953", "20140828080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029607", "BLCT", "BLCT2", "20140527183728", "20140828090000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000210", "BLCTZS", "BLCT3", "20140527110441", "20140828100000", "27", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030587", "BLCT2", "BLCTZS", "20140527123959", "20140828100000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030581", "BLCT", "BLCTYD", "20140527130604", "20140828100000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029585", "BLCT", "BLCT2", "20140527130604", "20140828100000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029584", "BLCT", "BLCT2", "20140527130604", "20140828100000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029557", "BLCT2", "BLCTMS", "20140525145416", "20140828120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029574", "BLCT", "BLCT2", "20140527164953", "20140828120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030586", "BLCTYD", "BLCTMS", "20140527164953", "20140828120000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029608", "BLCT3", "BLCTMS", "20140527164953", "20140828120000", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029613", "BLCT3", "BLCTMS", "20140527183728", "20140828120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029597", "BLCT", "BLCT2", "20140527183728", "20140828140000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029598", "BLCT", "BLCT2", "20140527183728", "20140828140000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029497", "BLCT", "BLCT2", "20140522173155", "20140828140300", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029511", "BLCT3", "BLCT", "20140523173049", "20140828150000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029606", "BLCT", "BLCT2", "20140527164953", "20140828160000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029602", "BLCT3", "BLCT2", "20140527164953", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029601", "BLCT3", "BLCT2", "20140527164953", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029605", "BLCT", "BLCT2", "20140527164953", "20140828160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039589", "BLCT3", "BLCTZS", "20140527164953", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039590", "BLCTYD", "BLCTZS", "20140527164953", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039588", "BLCTYD", "BLCTZS", "20140527164953", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029611", "BLCT", "BLCT2", "20140527183728", "20140828160000", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029599", "BLCT3", "BLCTZS", "20140527183728", "20140828160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029600", "BLCT3", "BLCTZS", "20140527183728", "20140828160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029674", "BLCTZS", "BLCT3", "20140529181417", "20140828160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029540", "BLCT3", "BLCT2", "20140524170337", "20140828180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030559", "BLCT2", "BLCT3", "20140526180524", "20140828180000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000212", "ZHCT", "BLCTZS", "20140527164953", "20140828180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030620", "BLCTYD", "BLCT", "20140528181030", "20140828180000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030629", "BLCTYD", "BLCT", "20140528181030", "20140828180000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030616", "BLCT2", "BLCT3", "20140528181030", "20140828190000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030561", "BLCTYD", "BLCT", "20140526180524", "20140828200000", "1", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030565", "BLCTZS", "BLCT2", "20140526180524", "20140828200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030532", "BLCTMS", "BLCT", "20140527164953", "20140828200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030536", "BLCTMS", "BLCT", "20140527164953", "20140828200000", "17", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029617", "BLCT3", "BLCT", "20140528150421", "20140828200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029618", "BLCTYD", "BLCT", "20140528150421", "20140828200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030543", "BLCTYD", "BLCT", "20140528180906", "20140828200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029624", "BLCT2", "BLCT3", "20140528180906", "20140828200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029623", "BLCT2", "BLCT3", "20140528180906", "20140828200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030562", "BLCTYD", "BLCT", "20140528180906", "20140828200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029647", "BLCTZS", "BLCT2", "20140528181030", "20140828200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029692", "BLCTZS", "BLCTYD", "20140530152032", "20140828200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029579", "BLCTMS", "BLCTZS", "20140527183728", "20140829000000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000215", "BLCTZS", "BLCT", "20140528150421", "20140829040000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030582", "BLCTMS", "BLCTZS", "20140528181114", "20140829080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029640", "BLCTZS", "BLCT", "20140528181114", "20140829080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030585", "BLCTMS", "BLCT3", "20140528181114", "20140829080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000213", "BLCTZS", "BLCT", "20140527174236", "20140829100000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029558", "BLCTZS", "BLCT", "20140525145416", "20140829120000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030540", "BLCT3", "BLCT2", "20140525145416", "20140829120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030538", "BLCT3", "BLCT", "20140525145416", "20140829120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029545", "BLCT2", "BLCT", "20140525145416", "20140829120000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029544", "BLCT2", "BLCT", "20140525145416", "20140829120000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030521", "BLCT", "BLCT2", "20140525145416", "20140829120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030522", "BLCT", "BLCT2", "20140525145416", "20140829120000", "25", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030514", "BLCT2", "BLCT", "20140526180524", "20140829120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000211", "BLCT2", "ZHCT", "20140527130604", "20140829120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029603", "BLCT3", "BLCT2", "20140527164953", "20140829120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029604", "BLCT", "BLCT2", "20140527164953", "20140829120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030551", "BLCT2", "BLCT", "20140527164953", "20140829120000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000218", "BLCT2", "BLCT3", "20140528150421", "20140829120000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030511", "BLCTMS", "BLCT", "20140528180906", "20140829120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029639", "BLCTZS", "BLCT", "20140528181114", "20140829120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029642", "BLCTZS", "BLCT2", "20140528181114", "20140829140000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029641", "BLCTZS", "BLCT2", "20140528181114", "20140829140000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029644", "BLCTZS", "BLCTYD", "20140528181114", "20140829150000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030546", "BLCT", "BLCT2", "20140526180524", "20140829160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030547", "BLCT", "BLCT2", "20140526180524", "20140829160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030475", "BLCTMS", "BLCT2", "20140527164953", "20140829160000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030501", "BLCTMS", "BLCT2", "20140527164953", "20140829160000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029610", "BLCT", "BLCT2", "20140527183728", "20140829160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000216", "DXCTE", "BLCTZS", "20140528150421", "20140829160000", "6", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030596", "BLCTYD", "BLCTZS", "20140528181030", "20140829160000", "4", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029622", "BLCT", "BLCTZS", "20140528181030", "20140829160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029632", "BLCT2", "BLCT3", "20140528181114", "20140829160000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030607", "BLCT2", "BLCT", "20140529181417", "20140829160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030552", "BLCTYD", "BLCT2", "20140524170337", "20140829170000", "14", "27
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029507", "BLCT2", "BLCT", "20140523173049", "20140829180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030560", "BLCT2", "BLCTYD", "20140526180524", "20140829180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029620", "BLCT", "BLCT3", "20140528181030", "20140829180000", "45", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030610", "BLCTYD", "BLCT", "20140528181030", "20140829180000", "7", "19
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030597", "BLCTYD", "BLCTZS", "20140528181030", "20140829180000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030612", "BLCT2", "BLCT", "20140528181030", "20140829180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030624", "BLCTZS", "BLCTMS", "20140528181114", "20140829180000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030623", "BLCTZS", "BLCTMS", "20140528181114", "20140829180000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029633", "BLCT", "BLCT2", "20140528181114", "20140829180000", "31", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029571", "BLCTMS", "BLCT3", "20140528181114", "20140829180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029573", "BLCTMS", "BLCT3", "20140528181114", "20140829180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029572", "BLCTMS", "BLCT3", "20140528181114", "20140829180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029643", "BLCTZS", "BLCT", "20140528181114", "20140829180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029570", "BLCTMS", "BLCT3", "20140528181114", "20140829180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029638", "BLCTZS", "BLCT", "20140528181114", "20140829180000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030611", "BLCT3", "BLCT", "20140528181114", "20140829180000", "1", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030622", "BLCT3", "BLCT", "20140528181114", "20140829190000", "2", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030531", "BLCT3", "BLCT2", "20140523173049", "20140829200000", "5", "17
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030530", "BLCT3", "BLCT2", "20140523173049", "20140829200000", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029546", "BLCTZS", "BLCT", "20140525145416", "20140829200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030557", "BLCT2", "BLCTYD", "20140526180524", "20140829200000", "1", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030567", "BLCTZS", "BLCT", "20140526180524", "20140829200000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039586", "BLCT", "BLCTMS", "20140527164953", "20140829200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029612", "BLCT", "BLCT3", "20140527183728", "20140829200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030590", "BLCT3", "BLCT2", "20140527183728", "20140829200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000214", "BLCTZS", "BLCT", "20140528150421", "20140829200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029629", "BLCTZS", "BLCT", "20140528180906", "20140829200000", "6", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029631", "BLCT3", "BLCT2", "20140528181030", "20140829200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030625", "BLCTYD", "BLCT2", "20140528181114", "20140829200000", "18", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029645", "BLCT2", "BLCT3", "20140528181114", "20140829200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029654", "BLCT", "BLCTMS", "20140529181417", "20140829200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029680", "BLCT", "BLCTZS", "20140529181417", "20140829200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029662", "BLCTZS", "BLCTMS", "20140529181417", "20140829200000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029650", "BLCTZS", "BLCT2", "20140529181417", "20140829200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029686", "BLCT3", "BLCT2", "20140530155928", "20140829200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030635", "BLCTMS", "BLCTZS", "20140528181114", "20140829230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030649", "BLCTYD", "BLCTZS", "20140529181417", "20140830000000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029677", "BLCT", "BLCT2", "20140529181417", "20140830000000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029676", "BLCT", "BLCT2", "20140529181417", "20140830010000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030584", "BLCTMS", "BLCT3", "20140528181114", "20140830080000", "15", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029626", "BLCT2", "BLCTYD", "20140528181114", "20140830080000", "29", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030588", "BLCTMS", "BLCT3", "20140528181114", "20140830080000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029627", "BLCT2", "BLCTYD", "20140528181114", "20140830080000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000220", "BLCTZS", "BLCT3", "20140529181417", "20140830100000", "24", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030539", "BLCT3", "BLCT2", "20140525145416", "20140830120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030520", "BLCT", "BLCT2", "20140525145416", "20140830120000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030528", "BLCT2", "BLCTMS", "20140527164953", "20140830120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029596", "BLCT", "BLCTZS", "20140527164953", "20140830120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000219", "BLCT2", "BLCTYD", "20140528150421", "20140830120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030606", "BLCT2", "BLCTZS", "20140528180906", "20140830120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029628", "BLCTZS", "BLCT", "20140528180906", "20140830120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030541", "BLCTYD", "BLCT2", "20140528180906", "20140830120000", "7", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030510", "BLCTMS", "BLCT2", "20140528180906", "20140830120000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030574", "BLCTMS", "BLCT3", "20140528181030", "20140830120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030573", "BLCTMS", "BLCT3", "20140528181030", "20140830120000", "2", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030655", "BLCT", "BLCTMS", "20140529181417", "20140830120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030654", "BLCT", "BLCTMS", "20140529181417", "20140830120000", "10", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030648", "BLCT3", "BLCT", "20140529181417", "20140830120000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030656", "BLCTZS", "BLCT3", "20140529181417", "20140830120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030653", "BLCTZS", "BLCT3", "20140529181417", "20140830120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030659", "BLCTMS", "BLCTZS", "20140529181417", "20140830120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029663", "BLCTYD", "BLCT2", "20140529181417", "20140830120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000222", "BLCTZS", "BLCT2", "20140529181417", "20140830120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030661", "BLCT2", "BLCTYD", "20140529181417", "20140830120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029666", "BLCT2", "BLCT3", "20140529181417", "20140830120000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030665", "BLCT2", "BLCTZS", "20140529181417", "20140830120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030633", "BLCT2", "BLCT", "20140529181417", "20140830120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029564", "BLCT2", "BLCTYD", "20140526180524", "20140830140000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029542", "BLCT3", "BLCT", "20140524170337", "20140830150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029646", "BLCT2", "BLCT", "20140528181114", "20140830150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030548", "BLCT", "BLCTMS", "20140526180524", "20140830160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029592", "BLCTMS", "BLCTZS", "20140528181114", "20140830160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029591", "BLCTMS", "BLCTZS", "20140528181114", "20140830160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029679", "BLCTZS", "BLCT2", "20140529181417", "20140830160000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029687", "BLCT", "BLCT2", "20140530152032", "20140830160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029685", "BLCT", "BLCTYD", "20140530152032", "20140830160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029684", "BLCT", "BLCT3", "20140530152032", "20140830160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029691", "BLCT3", "BLCTZS", "20140530191750", "20140830160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039587", "BLCT", "BLCTYD", "20140527164953", "20140830180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030645", "BLCT2", "BLCTZS", "20140528181030", "20140830180000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030618", "BLCTZS", "BLCT2", "20140528181030", "20140830180000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030641", "BLCTZS", "BLCT3", "20140528181114", "20140830180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030615", "BLCTZS", "BLCT3", "20140528181114", "20140830180000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029637", "BLCTZS", "BLCT2", "20140528181114", "20140830180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030583", "BLCTMS", "BLCT3", "20140528181114", "20140830180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029681", "BLCT3", "BLCT", "20140529181417", "20140830180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029671", "BLCTYD", "BLCTZS", "20140529181417", "20140830180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029673", "BLCT3", "BLCTZS", "20140529181417", "20140830180000", "6", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029672", "BLCT3", "BLCTZS", "20140529181417", "20140830180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030664", "BLCT2", "BLCTZS", "20140529181417", "20140830180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030600", "BLCTMS", "BLCTZS", "20140528181114", "20140830200000", "19", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030594", "BLCTMS", "BLCT2", "20140528181114", "20140830200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029682", "BLCT", "BLCT3", "20140530120551", "20140830200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029689", "BLCT3", "BLCTZS", "20140530120907", "20140830200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030692", "BLCT2", "BLCT3", "20140530155928", "20140830200000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000221", "BLCTZS", "BLCT2", "20140529181417", "20140830230000", "31", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030667", "BLCTZS", "BLCTYD", "20140531174702", "20140831080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030668", "BLCTZS", "BLCTYD", "20140531174702", "20140831080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030669", "BLCTZS", "BLCTYD", "20140531174702", "20140831080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029699", "BLCTZS", "BLCT2", "20140530155928", "20140831100000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029694", "BLCT3", "BLCTZS", "20140530171339", "20140831100000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029695", "BLCTYD", "BLCTZS", "20140530171339", "20140831100000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029543", "BLCT2", "BLCTMS", "20140525145416", "20140831120000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030519", "BLCT", "BLCTMS", "20140525145416", "20140831120000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030550", "BLCT2", "BLCT", "20140527164953", "20140831120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030549", "BLCT2", "BLCT", "20140527164953", "20140831120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000217", "BLCTZS", "ZHCT", "20140528150421", "20140831120000", "14", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029630", "BLCTMS", "BLCT3", "20140528180906", "20140831120000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030657", "BLCTMS", "BLCT3", "20140529181417", "20140831120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030658", "BLCTMS", "BLCT3", "20140529181417", "20140831120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029668", "BLCTMS", "BLCTZS", "20140529181417", "20140831120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029667", "BLCTMS", "BLCTZS", "20140529181417", "20140831120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029649", "BLCTZS", "BLCT3", "20140529181417", "20140831120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029697", "BLCT", "BLCT2", "20140530155928", "20140831120000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029696", "BLCT", "BLCT2", "20140530155928", "20140831120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029698", "BLCTZS", "BLCT2", "20140530155928", "20140831120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029729", "BLCT3", "BLCTZS", "20140501182448", "20140831120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029580", "BLCT3", "BLCT2", "20140527183728", "20140831150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030592", "BLCTYD", "BLCT2", "20140527183728", "20140831150000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030544", "BLCT", "BLCTMS", "20140526180524", "20140831160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029593", "BLCTMS", "BLCTZS", "20140528181114", "20140831160000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029634", "BLCT3", "BLCTMS", "20140528181114", "20140831160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030580", "BLCT2", "BLCTMS", "20140529181417", "20140831160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029657", "BLCT2", "BLCTYD", "20140529181417", "20140831160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029658", "BLCT2", "BLCT3", "20140529181417", "20140831160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030608", "BLCTMS", "BLCT", "20140530152032", "20140831160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030670", "BLCT2", "BLCTZS", "20140530155928", "20140831160000", "5", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030570", "BLCT", "BLCT2", "20140530155928", "20140831160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030671", "BLCT2", "BLCTZS", "20140531174702", "20140831160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029710", "BLCTYD", "BLCT2", "20140531174702", "20140831160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029609", "BLCT3", "BLCTMS", "20140527164953", "20140831180000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029619", "BLCT", "BLCT3", "20140528181030", "20140831180000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030617", "BLCTZS", "BLCT2", "20140528181030", "20140831180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030651", "BLCT", "BLCT3", "20140529181417", "20140831180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029700", "BLCT", "BLCT2", "20140530171339", "20140831180000", "9", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030575", "BLCT", "BLCT2", "20140526180524", "20140831200000", "36", "41
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030554", "BLCTMS", "BLCTZS", "20140528181114", "20140831200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030533", "BLCTMS", "BLCT", "20140528181114", "20140831200000", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030660", "BLCTMS", "BLCT", "20140529181417", "20140831200000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030652", "BLCT", "BLCTYD", "20140530152032", "20140831200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030678", "BLCTYD", "BLCTZS", "20140530155928", "20140831200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029702", "BLCT2", "BLCTYD", "20140530155928", "20140831200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029703", "BLCT2", "BLCT3", "20140530155928", "20140831200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030679", "BLCTYD", "BLCTZS", "20140530155928", "20140831200000", "4", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030681", "BLCTYD", "BLCTZS", "20140530155928", "20140831200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030680", "BLCTYD", "BLCTZS", "20140530155928", "20140831200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030676", "BLCT2", "BLCTYD", "20140530171339", "20140831200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029704", "BLCTZS", "BLCT", "20140531174702", "20140831200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030717", "BLCTYD", "BLCT", "20140531174702", "20140831200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030634", "BLCT3", "BLCTZS", "20140529181417", "20140831230000", "39", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030647", "BLCTMS", "BLCTYD", "20140529181417", "20140831230000", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029715", "BLCT", "BLCT3", "20140531140717", "20140831230000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029713", "BLCT2", "BLCT3", "20140531174702", "20140831230000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030702", "BLCTMS", "BLCT3", "20140531174702", "20140831230000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030701", "BLCTMS", "BLCT3", "20140531174702", "20140831230000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030699", "BLCT", "BLCT3", "20140531174702", "20140831230000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030638", "BLCTMS", "BLCT2", "20140530152032", "20140831233000", "0", "24
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030619", "BLCTZS", "BLCTMS", "20140528181030", "20140801120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000227", "BLCT3", "BLCT", "20140531174702", "20140801120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000229", "BLCT3", "BLCT2", "20140531174702", "20140801120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000228", "BLCT3", "BLCTMS", "20140531174702", "20140801120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040035", "BLCTYD", "BLCTZS", "20140503153442", "20140801120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040031", "BLCTYD", "BLCTZS", "20140503153442", "20140801120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029725", "BLCT3", "BLCT2", "20140531174702", "20140801150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030545", "BLCT", "BLCT2", "20140526180524", "20140801160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029665", "BLCTZS", "BLCT", "20140529181417", "20140801160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029661", "BLCTZS", "BLCTMS", "20140529181417", "20140801160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029678", "BLCTZS", "BLCT2", "20140529181417", "20140801160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029660", "BLCT2", "BLCT3", "20140529181417", "20140801160000", "40", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029655", "BLCT2", "BLCT", "20140529181417", "20140801160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030636", "BLCTMS", "BLCT2", "20140530152032", "20140801160000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029683", "BLCT3", "BLCTMS", "20140530152032", "20140801160000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030571", "BLCT", "BLCT2", "20140530155928", "20140801160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030716", "BLCTZS", "BLCT2", "20140531150006", "20140801160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000226", "BLCT", "BLCT3", "20140531174702", "20140801160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030725", "BLCTYD", "BLCT", "20140531174702", "20140801160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029621", "BLCT", "BLCT2", "20140528181114", "20140801180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029635", "BLCT3", "BLCTMS", "20140528181114", "20140801180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030736", "BLCT3", "BLCT2", "20140531174702", "20140801180000", "11", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000225", "BLCT2", "BLCTZS", "20140530152242", "20140801200000", "8", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030687", "BLCT2", "BLCT3", "20140530155928", "20140801200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030672", "BLCT", "BLCTMS", "20140530161932", "20140801200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029752", "BLCT2", "BLCTYD", "20140501182448", "20140801200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029753", "BLCT2", "BLCTYD", "20140501182448", "20140801200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029721", "BLCT2", "BLCTZS", "20140531174702", "20140801230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029718", "BLCT2", "BLCTZS", "20140531174702", "20140801230000", "56", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029719", "BLCT2", "BLCT", "20140531174702", "20140801230000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030643", "BLCTMS", "BLCTYD", "20140528181114", "20140802080000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029669", "BLCT", "BLCT2", "20140529181417", "20140802080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029670", "BLCT", "BLCT2", "20140529181417", "20140802080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029705", "BLCTZS", "BLCT", "20140531174702", "20140802080000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030728", "BLCTYD", "BLCTMS", "20140531174702", "20140802080000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029708", "BLCTYD", "BLCT2", "20140531174702", "20140802080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030730", "BLCT", "BLCT3", "20140531174702", "20140802080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000235", "ZHCT", "BLCT", "20140501182448", "20140802100000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029576", "BLCT", "BLCT3", "20140527164953", "20140802120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029578", "BLCT", "BLCT3", "20140527164953", "20140802120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029577", "BLCT", "BLCT3", "20140527164953", "20140802120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029575", "BLCT", "BLCT3", "20140527164953", "20140802120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029648", "BLCT", "BLCTZS", "20140529181417", "20140802120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030631", "BLCTMS", "BLCT", "20140529181417", "20140802120000", "102", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029744", "BLCTZS", "BLCT2", "20140501182448", "20140802120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000232", "BLCTZS", "BLCT2", "20140501182448", "20140802120000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029727", "BLCT3", "BLCTZS", "20140531174702", "20140802150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030666", "BLCT2", "BLCTYD", "20140530120551", "20140802160000", "38", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030637", "BLCTMS", "BLCT2", "20140530152032", "20140802160000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029690", "BLCT3", "BLCTZS", "20140530152032", "20140802160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029688", "BLCT", "BLCT2", "20140530152032", "20140802160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029748", "BLCT", "BLCT2", "20140501182448", "20140802180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029741", "BLCT", "BLCT2", "20140501182448", "20140802180000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030640", "BLCTMS", "BLCT3", "20140528181114", "20140802200000", "41", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030598", "BLCTMS", "BLCTZS", "20140528181114", "20140802200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030605", "BLCTMS", "BLCTZS", "20140528181114", "20140802200000", "15", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030604", "BLCTMS", "BLCTZS", "20140528181114", "20140802200000", "3", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030601", "BLCTMS", "BLCTZS", "20140528181114", "20140802200000", "5", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030689", "BLCT2", "BLCT3", "20140530155928", "20140802200000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030690", "BLCT2", "BLCT3", "20140530155928", "20140802200000", "8", "34
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030696", "BLCT", "BLCTYD", "20140530155928", "20140802200000", "1", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030673", "BLCTYD", "BLCTMS", "20140531174702", "20140802200000", "1", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029711", "BLCTYD", "BLCT", "20140531174702", "20140802200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029769", "BLCT2", "BLCTYD", "20140501182448", "20140802200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030735", "BLCT3", "BLCT", "20140531174702", "20140802230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029739", "BLCT", "BLCT2", "20140501182448", "20140802230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029743", "BLCT", "BLCT2", "20140501182448", "20140802230000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029747", "BLCT", "BLCT2", "20140501182448", "20140802230000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029761", "BLCT2", "BLCT", "20140501182448", "20140802230000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029764", "BLCT2", "BLCT", "20140501214443", "20140802230000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12039583", "BLCT", "BLCTZS", "20140527164953", "20140803000000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030737", "BLCTZS", "BLCTYD", "20140531174702", "20140803080000", "14", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030707", "BLCTMS", "BLCT3", "20140531174702", "20140803080000", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030706", "BLCTMS", "BLCT3", "20140531174702", "20140803080000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029757", "BLCT3", "BLCT2", "20140501182448", "20140803080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029754", "BLCT3", "BLCT2", "20140501182448", "20140803080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030613", "BLCTMS", "BLCT2", "20140528180906", "20140803120000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030614", "BLCTMS", "BLCT2", "20140528180906", "20140803120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029616", "BLCT", "BLCT3", "20140528181114", "20140803120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029615", "BLCT", "BLCT3", "20140528181114", "20140803120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029714", "BLCT3", "BLCTMS", "20140531174702", "20140803120000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029742", "BLCTZS", "BLCT3", "20140501182448", "20140803120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029728", "BLCT3", "BLCT2", "20140531174702", "20140803150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029652", "BLCT3", "BLCTMS", "20140529181417", "20140803160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029664", "BLCTZS", "BLCT2", "20140529181417", "20140803160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029656", "BLCT2", "BLCT3", "20140529181417", "20140803160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030639", "BLCTMS", "BLCT2", "20140530152032", "20140803160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030677", "BLCT3", "BLCT", "20140530155928", "20140803160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040009", "BLCT3", "BLCT", "20140501182448", "20140803180000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030743", "BLCTMS", "BLCT2", "20140501182448", "20140803180000", "5", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029768", "BLCT3", "BLCT", "20140501182448", "20140803180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000233", "ZHCT", "BLCT2", "20140501182448", "20140803180000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000236", "ZHCT", "BLCT2", "20140501182448", "20140803180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040007", "BLCTYD", "BLCT2", "20140501182448", "20140803180000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030603", "BLCTMS", "BLCTZS", "20140528181114", "20140803200000", "7", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029706", "BLCTZS", "BLCT", "20140531174702", "20140803200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030726", "BLCTYD", "BLCT", "20140531174702", "20140803200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030684", "BLCT", "BLCT2", "20140501171903", "20140803200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040001", "BLCT3", "BLCT2", "20140501182448", "20140803200000", "6", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029772", "BLCT2", "BLCTYD", "20140502180437", "20140803200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029770", "BLCT", "BLCT2", "20140502180437", "20140803200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029675", "BLCTZS", "BLCT3", "20140529181417", "20140804080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040026", "BLCTYD", "BLCTZS", "20140502180437", "20140804080000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040025", "BLCTYD", "BLCTZS", "20140502180437", "20140804080000", "9", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000239", "BLCTZS", "BLCT2", "20140503153442", "20140804080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040042", "BLCT3", "BLCT", "20140503153442", "20140804100000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029651", "BLCTZS", "BLCTMS", "20140529181417", "20140804120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030632", "BLCTMS", "BLCT", "20140529181417", "20140804120000", "47", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029659", "BLCT2", "BLCT", "20140529181417", "20140804120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029717", "BLCT2", "BLCT3", "20140531174702", "20140804120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030703", "BLCT2", "BLCT3", "20140531174702", "20140804120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030698", "BLCT2", "BLCT3", "20140531174702", "20140804120000", "8", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029756", "BLCT3", "BLCT2", "20140501182448", "20140804120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029763", "BLCT3", "BLCTMS", "20140501182448", "20140804120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029759", "BLCT3", "BLCT2", "20140501182448", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029790", "BLCT2", "BLCT", "20140503153442", "20140804120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040043", "BLCT2", "BLCT", "20140503153442", "20140804120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030708", "BLCT", "BLCT2", "20140503153442", "20140804120000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040011", "BLCT", "BLCT2", "20140503153442", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040014", "BLCT", "BLCT2", "20140503153442", "20140804120000", "24", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040034", "BLCT", "BLCT3", "20140503153442", "20140804120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040033", "BLCT", "BLCT3", "20140503153442", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040015", "BLCT", "BLCTMS", "20140503153442", "20140804120000", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029788", "BLCTZS", "BLCT2", "20140503153442", "20140804120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029785", "BLCTZS", "BLCT2", "20140503153442", "20140804120000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029784", "BLCTZS", "BLCT2", "20140503153442", "20140804120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040044", "BLCTZS", "BLCT2", "20140503153442", "20140804120000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029783", "BLCTZS", "BLCT2", "20140503153442", "20140804120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029777", "BLCTZS", "BLCT", "20140503153442", "20140804120000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029780", "BLCTZS", "BLCT", "20140503153442", "20140804120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029778", "BLCTZS", "BLCT", "20140503153442", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029779", "BLCTZS", "BLCT", "20140503153442", "20140804120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040045", "BLCTZS", "BLCTYD", "20140503153442", "20140804120000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029782", "BLCT3", "BLCTMS", "20140503153442", "20140804120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040036", "BLCTYD", "BLCT2", "20140503153442", "20140804120000", "13", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030642", "BLCTYD", "BLCT2", "20140503153442", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040023", "BLCTYD", "BLCT", "20140503153442", "20140804120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040037", "BLCTYD", "BLCT", "20140503153442", "20140804120000", "23", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029736", "BLCT3", "BLCT2", "20140501182448", "20140804150000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029751", "BLCT3", "BLCT2", "20140501182448", "20140804150000", "2", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029758", "BLCT3", "BLCT2", "20140501182448", "20140804150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040003", "BLCT2", "BLCTYD", "20140501182448", "20140804160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030599", "BLCTMS", "BLCT3", "20140528181114", "20140804200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030646", "BLCTMS", "BLCT3", "20140529181417", "20140804200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030674", "BLCT2", "BLCT3", "20140530171339", "20140804200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030734", "BLCT2", "BLCT3", "20140531174702", "20140804200000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030715", "BLCT2", "BLCT3", "20140531174702", "20140804200000", "1", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030729", "BLCT3", "BLCTZS", "20140531174702", "20140804200000", "0", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040027", "BLCT2", "BLCTZS", "20140503153442", "20140804200000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029826", "BLCT", "BLCT3", "20140505174258", "20140804200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029745", "BLCT", "BLCT2", "20140501164606", "20140804230000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000249", "BLCT2", "BLCT", "20140505133643", "20140805050000", "34", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030721", "BLCTMS", "BLCT3", "20140531174702", "20140805080000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030720", "BLCTMS", "BLCT3", "20140531174702", "20140805080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030723", "BLCTMS", "BLCT3", "20140531174702", "20140805080000", "8", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030722", "BLCTMS", "BLCT3", "20140531174702", "20140805080000", "4", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029740", "BLCTYD", "BLCT", "20140501182448", "20140805080000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029760", "BLCT3", "BLCT", "20140501182448", "20140805080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000230", "BLCTZS", "BLCT", "20140501182448", "20140805080000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000252", "ZHCT", "BLCTZS", "20140505210515", "20140805080000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030602", "BLCT", "BLCT2", "20140529181417", "20140805120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029724", "BLCT2", "BLCT", "20140531174702", "20140805120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000231", "BLCT3", "ZHCT", "20140501182448", "20140805120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000243", "BLCT2", "BLCT", "20140502180437", "20140805120000", "62", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000244", "BLCT2", "BLCT3", "20140504193905", "20140805120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000245", "BLCT2", "BLCTZS", "20140504193905", "20140805120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029746", "BLCTMS", "BLCT3", "20140506155932", "20140805120000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029771", "BLCT2", "BLCTMS", "20140502180437", "20140805150000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030609", "BLCTMS", "BLCT2", "20140530152032", "20140805160000", "0", "20
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029701", "BLCT2", "BLCT", "20140530171339", "20140805160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029735", "BLCT3", "BLCTMS", "20140501182448", "20140805160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030718", "BLCT3", "BLCT", "20140501182448", "20140805160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030719", "BLCT3", "BLCT2", "20140501182448", "20140805160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030686", "BLCT", "BLCT2", "20140501182448", "20140805160000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030685", "BLCT", "BLCT2", "20140501182448", "20140805160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029791", "BLCT2", "BLCT", "20140503153442", "20140805160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030714", "BLCT", "BLCT2", "20140503153442", "20140805160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030713", "BLCT", "BLCT2", "20140503153442", "20140805160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030712", "BLCT", "BLCT2", "20140503153442", "20140805160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030709", "BLCT", "BLCTMS", "20140503153442", "20140805160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029786", "BLCTZS", "BLCT2", "20140503153442", "20140805160000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000240", "BLCTZS", "BLCT", "20140503153442", "20140805160000", "3", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029781", "BLCTZS", "BLCT", "20140503153442", "20140805160000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040040", "BLCT3", "BLCT", "20140503153442", "20140805160000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040039", "BLCTYD", "BLCT", "20140503153442", "20140805160000", "5", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040038", "BLCTYD", "BLCT", "20140503153442", "20140805160000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030740", "BLCT2", "BLCTYD", "20140505174258", "20140805160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040081", "BLCT2", "BLCT3", "20140505174258", "20140805160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030738", "BLCT2", "BLCT3", "20140505174258", "20140805160000", "4", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029816", "BLCT", "BLCT3", "20140505174258", "20140805160000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029815", "BLCT", "BLCTZS", "20140505174258", "20140805160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029828", "BLCT", "BLCT2", "20140505174258", "20140805160000", "10", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029813", "BLCT", "BLCT2", "20140505174258", "20140805160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040053", "BLCTMS", "BLCTZS", "20140505174258", "20140805160000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040032", "BLCTYD", "BLCTZS", "20140505174258", "20140805160000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040054", "BLCTZS", "BLCTMS", "20140505174258", "20140805160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040069", "BLCTZS", "BLCT", "20140505174258", "20140805160000", "2", "21
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029822", "BLCTZS", "BLCT", "20140505174258", "20140805160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029823", "BLCT2", "BLCT", "20140505174258", "20140805160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030705", "BLCT", "BLCT2", "20140505174258", "20140805160000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040010", "BLCTYD", "BLCTMS", "20140501182448", "20140805180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029734", "BLCT3", "BLCTMS", "20140501182448", "20140805180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029733", "BLCT3", "BLCTMS", "20140501182448", "20140805180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029749", "BLCT3", "BLCT", "20140501182448", "20140805180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029776", "BLCT2", "BLCTMS", "20140502180437", "20140805180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029775", "BLCT3", "BLCTMS", "20140502180437", "20140805180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029774", "BLCT3", "BLCTMS", "20140502180437", "20140805180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000246", "BLCT2", "BLCT", "20140504193905", "20140805180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029625", "BLCT", "BLCTYD", "20140528181030", "20140805200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030727", "BLCTYD", "BLCT", "20140531174702", "20140805200000", "6", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030675", "BLCTYD", "BLCT", "20140531174702", "20140805200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029712", "BLCTYD", "BLCT", "20140531174702", "20140805200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029731", "BLCT3", "BLCTMS", "20140501182448", "20140805200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029732", "BLCT3", "BLCTMS", "20140501182448", "20140805200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030733", "BLCTMS", "BLCT", "20140501182448", "20140805200000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030732", "BLCTMS", "BLCT", "20140501182448", "20140805200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030693", "BLCTMS", "BLCT", "20140501182448", "20140805200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030694", "BLCTMS", "BLCT", "20140501182448", "20140805200000", "17", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040006", "BLCTYD", "BLCTZS", "20140501182448", "20140805200000", "8", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040021", "BLCT2", "BLCTZS", "20140503153442", "20140805200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040022", "BLCT2", "BLCTZS", "20140503153442", "20140805200000", "8", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029801", "BLCT", "BLCTMS", "20140505133643", "20140805200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029808", "BLCT", "BLCTMS", "20140505133643", "20140805200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029803", "BLCT", "BLCTYD", "20140505133643", "20140805200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029793", "BLCT3", "BLCT", "20140505133643", "20140805200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029800", "BLCT", "BLCT2", "20140505133643", "20140805200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029817", "BLCT", "BLCT3", "20140505174258", "20140805200000", "20", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029722", "BLCT2", "BLCTMS", "20140531174702", "20140806120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029716", "BLCT2", "BLCTYD", "20140531174702", "20140806120000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029723", "BLCT2", "BLCTMS", "20140531174702", "20140806120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030744", "BLCTMS", "BLCT3", "20140501182448", "20140806120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029755", "BLCT3", "BLCT2", "20140501182448", "20140806120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000241", "BLCT2", "DXCTE", "20140503153442", "20140806120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000242", "BLCT2", "DXCTE", "20140503153442", "20140806120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030710", "BLCT", "BLCTMS", "20140503153442", "20140806120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040088", "BLCT2", "BLCTZS", "20140506155932", "20140806120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040087", "BLCT2", "BLCTZS", "20140506155932", "20140806120000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040086", "BLCT2", "BLCTZS", "20140506155932", "20140806120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040118", "BLCT3", "BLCT2", "20140506155932", "20140806120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029765", "BLCT3", "BLCT2", "20140501182448", "20140806150000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029773", "BLCT2", "BLCTZS", "20140502180437", "20140806150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040004", "BLCT2", "BLCTYD", "20140501182448", "20140806160000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029737", "BLCT2", "BLCTYD", "20140501182448", "20140806160000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030704", "BLCT", "BLCT3", "20140501182448", "20140806160000", "38", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029738", "BLCTYD", "BLCT", "20140501182448", "20140806180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000237", "BLCT2", "ZHCT", "20140501182448", "20140806180000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040049", "BLCT", "BLCTMS", "20140504191930", "20140806180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029830", "BLCT", "BLCT2", "20140505174258", "20140806180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029797", "BLCT2", "BLCTYD", "20140505174258", "20140806180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029824", "BLCT3", "BLCT", "20140505174258", "20140806180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040085", "BLCTZS", "BLCT3", "20140505174258", "20140806180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029829", "BLCT", "BLCT2", "20140505174258", "20140806180000", "13", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029827", "BLCT", "BLCT2", "20140505174258", "20140806180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040099", "BLCTYD", "BLCT", "20140506155932", "20140806180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030621", "BLCTMS", "BLCT2", "20140528181030", "20140806200000", "12", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029693", "BLCT", "BLCT3", "20140530155928", "20140806200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029730", "BLCT3", "BLCTMS", "20140501182448", "20140806200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040029", "BLCTZS", "BLCTYD", "20140502180437", "20140806200000", "20", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000247", "BLCT2", "BLCT", "20140505104445", "20140806200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029806", "BLCT", "BLCT3", "20140505133643", "20140806200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040057", "BLCT", "BLCTMS", "20140505133643", "20140806200000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040019", "BLCT2", "BLCTMS", "20140506113911", "20140806200000", "6", "37
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040115", "BLCT", "BLCTMS", "20140506165934", "20140806200000", "31", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040064", "BLCTMS", "BLCT2", "20140506165934", "20140806200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040016", "BLCT3", "BLCT2", "20140501182448", "20140806230000", "13", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000238", "ZHCT", "BLCTZS", "20140501182448", "20140807000000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029707", "BLCTYD", "BLCTZS", "20140531174702", "20140807000100", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030644", "BLCTMS", "BLCTYD", "20140528181114", "20140807080000", "7", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040028", "BLCTZS", "BLCTYD", "20140502180437", "20140807080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029837", "BLCT2", "BLCTMS", "20140506122407", "20140807080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040102", "BLCTMS", "BLCTYD", "20140506122407", "20140807080000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040124", "BLCTZS", "BLCTYD", "20140506165934", "20140807080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040123", "BLCT2", "BLCTYD", "20140506165934", "20140807080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029750", "BLCT2", "BLCTYD", "20140501182448", "20140807120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029836", "BLCT", "BLCTZS", "20140506165934", "20140807120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029835", "BLCTZS", "BLCT3", "20140506165934", "20140807120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029851", "BLCTYD", "BLCTZS", "20140506165934", "20140807120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029852", "BLCTYD", "BLCTZS", "20140506165934", "20140807120000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029848", "BLCTYD", "BLCTZS", "20140506165934", "20140807120000", "3", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029846", "BLCT", "BLCT2", "20140506165934", "20140807120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029847", "BLCT", "BLCT2", "20140506165934", "20140807120000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029849", "BLCT", "BLCT2", "20140506165934", "20140807120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029844", "BLCTYD", "BLCTZS", "20140506165934", "20140807120000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029845", "BLCT3", "BLCTZS", "20140506165934", "20140807120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029858", "BLCTZS", "BLCT", "20140506165934", "20140807120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029834", "BLCTZS", "BLCT3", "20140506165934", "20140807120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029832", "BLCTZS", "BLCT3", "20140506165934", "20140807120000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029833", "BLCTZS", "BLCT3", "20140506165934", "20140807120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029856", "BLCT", "BLCT2", "20140506165934", "20140807130000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029726", "BLCT3", "BLCT", "20140531174702", "20140807150000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029767", "BLCT3", "BLCT", "20140501182448", "20140807150000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029766", "BLCT3", "BLCTMS", "20140501182448", "20140807150000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030682", "BLCT", "BLCTMS", "20140501182448", "20140807160000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030683", "BLCT", "BLCTYD", "20140501182448", "20140807160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040092", "BLCT", "BLCT2", "20140506113911", "20140807160000", "0", "32
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040018", "BLCT2", "BLCTMS", "20140506122407", "20140807160000", "56", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040047", "BLCT2", "BLCTMS", "20140506122407", "20140807160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040046", "BLCT2", "BLCTYD", "20140506122407", "20140807160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040067", "BLCT2", "BLCTYD", "20140506122407", "20140807160000", "76", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029861", "BLCT", "BLCT2", "20140506165934", "20140807160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040063", "BLCTMS", "BLCT2", "20140506165934", "20140807160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040066", "BLCTMS", "BLCT2", "20140506165934", "20140807160000", "5", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040127", "BLCTZS", "BLCTYD", "20140507170807", "20140807160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029762", "BLCT3", "BLCT", "20140501182448", "20140807180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040013", "BLCTZS", "BLCT", "20140504191930", "20140807180000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040084", "BLCTMS", "BLCT3", "20140506122407", "20140807180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040083", "BLCTMS", "BLCT3", "20140506122407", "20140807180000", "24", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040078", "BLCTMS", "BLCTZS", "20140506155932", "20140807180000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029843", "BLCTYD", "BLCTZS", "20140506165934", "20140807180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029860", "BLCT", "BLCT2", "20140506165934", "20140807180000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040110", "BLCTYD", "BLCTZS", "20140506165934", "20140807180000", "3", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030731", "BLCTMS", "BLCTZS", "20140501182448", "20140807200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030724", "BLCTMS", "BLCTZS", "20140501182448", "20140807200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040005", "BLCTYD", "BLCTZS", "20140501182448", "20140807200000", "9", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030663", "BLCT2", "BLCT", "20140503153442", "20140807200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040002", "BLCT2", "BLCTYD", "20140503153442", "20140807200000", "32", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040012", "BLCT2", "BLCTYD", "20140503153442", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030662", "BLCT2", "BLCTMS", "20140503153442", "20140807200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040041", "BLCT", "BLCTZS", "20140503153442", "20140807200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030711", "BLCT", "BLCTYD", "20140503153442", "20140807200000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029789", "BLCTZS", "BLCT2", "20140503153442", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029812", "BLCT", "BLCT3", "20140505133643", "20140807200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029807", "BLCT", "BLCT3", "20140505133643", "20140807200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040059", "BLCTZS", "BLCT2", "20140505133643", "20140807200000", "13", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030739", "BLCT2", "BLCT", "20140505174258", "20140807200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030742", "BLCT2", "BLCTYD", "20140505174258", "20140807200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029796", "BLCT2", "BLCT3", "20140505174258", "20140807200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040051", "BLCT", "BLCT2", "20140505174258", "20140807200000", "13", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040058", "BLCTMS", "BLCTYD", "20140505174258", "20140807200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040050", "BLCT3", "BLCT2", "20140505174258", "20140807200000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029825", "BLCTZS", "BLCT", "20140505174258", "20140807200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000250", "DXCTE", "BLCTZS", "20140505210515", "20140807200000", "14", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029831", "BLCTZS", "BLCTYD", "20140506165934", "20140807200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029839", "BLCT", "BLCTYD", "20140506165934", "20140807200000", "18", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029838", "BLCT", "BLCT3", "20140506165934", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029855", "BLCT2", "BLCT3", "20140506165934", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029862", "BLCT", "BLCTZS", "20140506165934", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029840", "BLCTMS", "BLCTZS", "20140506174255", "20140807200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029842", "BLCTMS", "BLCTZS", "20140506174255", "20140807200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029841", "BLCTMS", "BLCTZS", "20140506174255", "20140807200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029871", "BLCT2", "BLCT", "20140507170807", "20140807200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040120", "BLCT3", "BLCT2", "20140507170807", "20140807200000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029887", "BLCT2", "BLCTMS", "20140508170935", "20140807200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029893", "BLCT2", "BLCTYD", "20140509164006", "20140807200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040139", "BLCTZS", "BLCTYD", "20140507170807", "20140807230000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040138", "BLCTZS", "BLCTYD", "20140507170807", "20140807230000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000253", "DXCTE", "BLCTZS", "20140506122407", "20140808000000", "0", "20
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000234", "ZHCT", "BLCT2", "20140501182448", "20140808100000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040030", "BLCTZS", "BLCTMS", "20140504185623", "20140808100000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040020", "BLCT2", "BLCT", "20140506122407", "20140808120000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040024", "BLCT2", "BLCT", "20140506122407", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040113", "BLCT2", "BLCTZS", "20140506155932", "20140808120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040114", "BLCT2", "BLCTZS", "20140506155932", "20140808120000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040112", "BLCT2", "BLCTZS", "20140506155932", "20140808120000", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029850", "BLCT", "BLCT2", "20140506165934", "20140808120000", "2", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000254", "BLCT2", "BLCTYD", "20140507170807", "20140808120000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040096", "BLCT", "BLCT2", "20140507170807", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029865", "BLCT2", "BLCTYD", "20140507170807", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000256", "BLCTZS", "BLCTYD", "20140507170807", "20140808120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000255", "BLCTZS", "BLCT3", "20140507170807", "20140808120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040062", "BLCTMS", "BLCT2", "20140506165934", "20140808160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029863", "BLCT2", "BLCT", "20140507170807", "20140808160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029709", "BLCTYD", "BLCT", "20140531174702", "20140808180000", "2", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040082", "BLCTMS", "BLCTZS", "20140506155932", "20140808180000", "49", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040074", "BLCTMS", "BLCTZS", "20140506155932", "20140808180000", "6", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040111", "BLCTYD", "BLCTZS", "20140506165934", "20140808180000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029888", "BLCT2", "BLCTYD", "20140508170935", "20140808180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030700", "BLCTMS", "BLCTZS", "20140501182448", "20140808200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029811", "BLCT", "BLCT2", "20140505133643", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029810", "BLCT", "BLCT3", "20140505133643", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029809", "BLCT", "BLCT3", "20140505133643", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029805", "BLCT", "BLCT3", "20140505133643", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029804", "BLCT", "BLCT3", "20140505133643", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029798", "BLCT3", "BLCT2", "20140505133643", "20140808200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040060", "BLCTZS", "BLCTMS", "20140505133643", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029792", "BLCTZS", "BLCT2", "20140505133643", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029802", "BLCT", "BLCTZS", "20140505133643", "20140808200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029799", "BLCT", "BLCT2", "20140505133643", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040052", "BLCT", "BLCT2", "20140505174258", "20140808200000", "18", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029794", "BLCT2", "BLCT3", "20140505174258", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029820", "BLCT", "BLCT3", "20140505174258", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029795", "BLCT2", "BLCTYD", "20140505174258", "20140808200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030697", "BLCTMS", "BLCT2", "20140505174258", "20140808200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029819", "BLCT3", "BLCTMS", "20140505174258", "20140808200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030691", "BLCTZS", "BLCT", "20140505174258", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029814", "BLCT", "BLCTYD", "20140505174258", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029818", "BLCT", "BLCTYD", "20140505174258", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040104", "BLCT", "BLCT3", "20140506165934", "20140808200000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029873", "BLCTZS", "BLCT3", "20140507170807", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029879", "BLCT2", "BLCTYD", "20140507170807", "20140808200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029880", "BLCT2", "BLCTYD", "20140507170807", "20140808200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029872", "BLCTZS", "BLCTYD", "20140507170807", "20140808200000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029866", "BLCT2", "BLCT3", "20140507170807", "20140808200000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000258", "BLCTZS", "ZHCT", "20140507170807", "20140809080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000248", "BLCT2", "ZHCT", "20140505133643", "20140809120000", "5", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040107", "BLCT2", "BLCTMS", "20140506155932", "20140809120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040108", "BLCTZS", "BLCT2", "20140506155932", "20140809120000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040135", "BLCT", "BLCTYD", "20140507170807", "20140809120000", "0", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030650", "BLCTMS", "BLCT2", "20140530152032", "20140809160000", "19", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029854", "BLCTZS", "BLCT", "20140506165934", "20140809160000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029853", "BLCT2", "BLCT", "20140506165934", "20140809160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040126", "BLCTZS", "BLCTYD", "20140507170807", "20140809160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040125", "BLCTZS", "BLCTYD", "20140507170807", "20140809160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12030579", "BLCTZS", "BLCTYD", "20140507170807", "20140809160000", "34", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029867", "BLCT2", "BLCT3", "20140507170807", "20140809160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000261", "BLCTZS", "DXCTE", "20140508111917", "20140809160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040017", "BLCTYD", "BLCT", "20140504191930", "20140809180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040048", "BLCTYD", "BLCT", "20140504191930", "20140809180000", "6", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040072", "BLCTMS", "BLCTZS", "20140506155932", "20140809180000", "14", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040073", "BLCTMS", "BLCTZS", "20140506155932", "20140809180000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040076", "BLCTMS", "BLCT3", "20140506155932", "20140809180000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040079", "BLCTMS", "BLCT2", "20140506155932", "20140809180000", "7", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040147", "BLCT2", "BLCTZS", "20140509164006", "20140809180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040158", "BLCT", "BLCT2", "20140509164006", "20140809180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029891", "BLCTZS", "BLCT3", "20140509164006", "20140809180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000251", "BLCTZS", "BLCT2", "20140505210515", "20140809200000", "17", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040100", "BLCTYD", "BLCTMS", "20140506155932", "20140809200000", "4", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029890", "BLCT3", "BLCT", "20140509164006", "20140809200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029910", "BLCT2", "BLCTYD", "20140509164006", "20140809200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040122", "BLCT2", "BLCTZS", "20140509164006", "20140809200000", "0", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029896", "BLCT2", "BLCTYD", "20140509164006", "20140809200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029895", "BLCT2", "BLCTYD", "20140509164006", "20140809200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029894", "BLCT2", "BLCTYD", "20140509164006", "20140809200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029919", "BLCT2", "BLCTZS", "20140510164503", "20140809200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040140", "BLCTZS", "BLCTYD", "20140507170807", "20140809230000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040155", "BLCT", "BLCTYD", "20140509164006", "20140809230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029902", "BLCT", "BLCTMS", "20140509164006", "20140810000000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040133", "BLCT", "BLCTMS", "20140509164006", "20140810000000", "4", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040103", "BLCTMS", "BLCT3", "20140506122407", "20140810080000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029886", "BLCT2", "BLCTMS", "20140508170935", "20140810080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040141", "BLCT2", "BLCTZS", "20140508170935", "20140810080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040091", "BLCT2", "BLCTMS", "20140508170935", "20140810080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040183", "BLCTZS", "BLCTYD", "20140509164006", "20140810080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040145", "BLCTYD", "BLCTMS", "20140509164006", "20140810080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040182", "BLCTZS", "BLCTYD", "20140509164006", "20140810080000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040142", "BLCT2", "BLCT3", "20140508170935", "20140810080100", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029874", "BLCTZS", "BLCT", "20140508170935", "20140810080200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029875", "BLCTZS", "BLCT2", "20140508170935", "20140810080300", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029885", "BLCT2", "BLCTYD", "20140508170935", "20140810080400", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029884", "BLCT2", "BLCTYD", "20140508170935", "20140810080500", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029883", "BLCT2", "BLCTMS", "20140508170935", "20140810080600", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029882", "BLCT2", "BLCTMS", "20140508170935", "20140810080700", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029868", "BLCT2", "BLCTYD", "20140508170935", "20140810080800", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029878", "BLCTZS", "BLCTYD", "20140508170935", "20140810080900", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029876", "BLCTZS", "BLCT3", "20140508170935", "20140810081000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029881", "BLCT2", "BLCTMS", "20140508170935", "20140810081200", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000262", "BLCT3", "BLCT2", "20140508170935", "20140810081200", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029901", "BLCTMS", "BLCT3", "20140509164006", "20140810090000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029900", "BLCT", "BLCTZS", "20140509170339", "20140810090000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029899", "BLCT", "BLCTZS", "20140509170339", "20140810090000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040128", "BLCT2", "BLCTZS", "20140506155932", "20140810120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000257", "BLCTZS", "BLCT", "20140507170807", "20140810120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029903", "BLCTMS", "BLCT3", "20140509170339", "20140810140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029897", "BLCTMS", "BLCTZS", "20140509170339", "20140810140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040207", "BLCTZS", "BLCTYD", "20140510164503", "20140810140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029870", "BLCT2", "BLCT3", "20140507170807", "20140810150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029859", "BLCT2", "BLCT", "20140506165934", "20140810160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12010093", "BLCT", "BLCT2", "20140507170807", "20140810160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029869", "BLCT2", "BLCT", "20140507170807", "20140810160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040121", "BLCT3", "BLCT2", "20140507170807", "20140810160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040206", "BLCT3", "BLCT", "20140510164503", "20140810170000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040075", "BLCTMS", "BLCT2", "20140506155932", "20140810180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040174", "BLCTMS", "BLCT", "20140509164006", "20140810180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040080", "BLCT", "BLCT2", "20140505162512", "20140810200000", "13", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029821", "BLCT", "BLCT3", "20140505174258", "20140810200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040070", "BLCT", "BLCT2", "20140505174258", "20140810200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040055", "BLCTMS", "BLCT3", "20140505174258", "20140810200000", "2", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040056", "BLCTMS", "BLCT", "20140505174258", "20140810200000", "21", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040101", "BLCT2", "BLCTYD", "20140506155932", "20140810200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040168", "BLCTMS", "BLCT", "20140509164006", "20140810200000", "4", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040180", "BLCTYD", "BLCT", "20140509164006", "20140810200000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029857", "BLCTMS", "BLCT3", "20140509164006", "20140810200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029904", "BLCT", "BLCT2", "20140509164006", "20140810200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029911", "BLCT", "BLCT2", "20140509170339", "20140810200000", "0", "17
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029892", "BLCT2", "BLCTMS", "20140509170339", "20140810200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029913", "BLCT", "BLCT2", "20140509170339", "20140810200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040146", "BLCTMS", "BLCT2", "20140509170339", "20140810200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040148", "BLCTMS", "BLCT2", "20140509170339", "20140810200000", "4", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040176", "BLCTYD", "BLCT2", "20140510164503", "20140810200000", "2", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040185", "BLCTYD", "BLCT", "20140510164503", "20140810200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040186", "BLCTYD", "BLCT", "20140510164503", "20140810200000", "3", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040105", "BLCTMS", "BLCT3", "20140506122407", "20140811080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040106", "BLCTMS", "BLCT3", "20140506122407", "20140811080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040741", "BLCT2", "BLCT", "20140506113911", "20140811120000", "18", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000259", "BLCT2", "DXCTE", "20140508111917", "20140811120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000260", "BLCT2", "DXCTE", "20140508111917", "20140811120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040181", "BLCT3", "BLCT2", "20140509170339", "20140811120000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040089", "BLCT2", "BLCT", "20140506122407", "20140811160000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040065", "BLCTMS", "BLCT2", "20140506165934", "20140811160000", "7", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040061", "BLCTMS", "BLCT2", "20140506165934", "20140811160000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029926", "BLCT2", "BLCT", "20140511122928", "20140811180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029929", "BLCT2", "BLCT", "20140511155036", "20140811180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040204", "BLCTYD", "BLCT", "20140510164503", "20140811190000", "14", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040068", "BLCTMS", "BLCT", "20140505174258", "20140811200000", "50", "29
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040175", "BLCTYD", "BLCT2", "20140509164006", "20140811200000", "13", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040177", "BLCTMS", "BLCT", "20140509164006", "20140811200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040179", "BLCTMS", "BLCT", "20140509164006", "20140811200000", "18", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040178", "BLCTMS", "BLCT", "20140509164006", "20140811200000", "32", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040167", "BLCT2", "BLCT3", "20140509164006", "20140811200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040160", "BLCT2", "BLCT", "20140509164006", "20140811200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029908", "BLCT", "BLCT3", "20140509164006", "20140811200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040153", "BLCTZS", "BLCTYD", "20140509170339", "20140811200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040144", "BLCTMS", "BLCT2", "20140509170339", "20140811200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040156", "BLCTYD", "BLCT", "20140510115906", "20140811200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040157", "BLCTYD", "BLCTMS", "20140510115906", "20140811200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040209", "BLCT2", "BLCT3", "20140510164503", "20140811200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029921", "BLCT", "BLCT2", "20140510164503", "20140811200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029924", "BLCT", "BLCTYD", "20140510164503", "20140811200000", "5", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029918", "BLCTZS", "BLCTYD", "20140510164503", "20140811200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040192", "BLCTMS", "BLCT3", "20140510164503", "20140811200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040211", "BLCTYD", "BLCT2", "20140510164503", "20140811200000", "11", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040205", "BLCTYD", "BLCT", "20140510164503", "20140811200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040187", "BLCTMS", "BLCTYD", "20140510164503", "20140811200000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029916", "BLCTMS", "BLCTYD", "20140510164503", "20140811200000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040196", "BLCTMS", "BLCT3", "20140510164503", "20140811200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040195", "BLCTMS", "BLCT3", "20140510164503", "20140811200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029915", "BLCT3", "BLCTZS", "20140510164503", "20140811200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029917", "BLCTZS", "BLCTYD", "20140510164503", "20140811200000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029923", "BLCTZS", "BLCTYD", "20140510164503", "20140811200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040203", "BLCT", "BLCTYD", "20140510164503", "20140811200000", "27", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029920", "BLCT", "BLCT2", "20140510164503", "20140811200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029922", "BLCT2", "BLCTYD", "20140510164503", "20140811200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040190", "BLCT", "BLCT3", "20140511122928", "20140811200000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029940", "BLCT3", "BLCTZS", "20140511164049", "20140812000000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029934", "BLCT3", "BLCTMS", "20140511155036", "20140812080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040213", "BLCTYD", "BLCTZS", "20140511155036", "20140812080000", "5", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040223", "BLCTYD", "BLCTZS", "20140511155036", "20140812080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040224", "BLCTYD", "BLCTZS", "20140511155036", "20140812080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029939", "BLCT", "BLCT2", "20140511155036", "20140812080000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029936", "BLCT", "BLCTYD", "20140511155036", "20140812080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029938", "BLCT", "BLCT2", "20140511155036", "20140812080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029937", "BLCT", "BLCTYD", "20140511155036", "20140812080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029933", "BLCT", "BLCT3", "20140511172401", "20140812080000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029932", "BLCTMS", "BLCT3", "20140511172401", "20140812080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040220", "BLCTMS", "BLCTYD", "20140511172401", "20140812080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029931", "BLCT2", "BLCT", "20140511155036", "20140812120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029941", "BLCTZS", "BLCT", "20140511162939", "20140812120000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029949", "BLCT", "BLCT2", "20140511164049", "20140812120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029943", "BLCTZS", "BLCT", "20140511164049", "20140812120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029942", "BLCTZS", "BLCT", "20140511164049", "20140812120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029946", "BLCT3", "BLCTMS", "20140511164049", "20140812120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000274", "BLCT2", "BLCT3", "20140512095907", "20140812120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000273", "BLCTZS", "BLCT2", "20140512095907", "20140812120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000272", "BLCTZS", "BLCT3", "20140512095907", "20140812120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000271", "BLCT2", "BLCT", "20140512095907", "20140812120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040159", "BLCT", "BLCT2", "20140512111036", "20140812120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029898", "BLCTMS", "BLCTZS", "20140509170339", "20140812140000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040201", "BLCT", "BLCT2", "20140510150210", "20140812140000", "28", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040090", "BLCTMS", "BLCT", "20140506165934", "20140812160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040119", "BLCT3", "BLCT", "20140507170807", "20140812160000", "9", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029864", "BLCT2", "BLCTMS", "20140507170807", "20140812160000", "12", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040130", "BLCT2", "BLCT", "20140508170935", "20140812160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040097", "BLCT2", "BLCTMS", "20140508170935", "20140812160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040098", "BLCT2", "BLCTMS", "20140508170935", "20140812160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040129", "BLCT2", "BLCT3", "20140508170935", "20140812160000", "15", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040131", "BLCT", "BLCT2", "20140509164006", "20140812160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029945", "BLCT3", "BLCTMS", "20140511164049", "20140812160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029947", "BLCT3", "BLCTMS", "20140511164049", "20140812160000", "0", "18
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000284", "BLCT2", "BLCTZS", "20140513095722", "20140812160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040117", "BLCTMS", "BLCTZS", "20140506155932", "20140812180000", "9", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040077", "BLCTMS", "BLCT2", "20140506155932", "20140812180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029925", "BLCT2", "BLCT", "20140511122928", "20140812180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029927", "BLCT2", "BLCT", "20140511122928", "20140812180000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029928", "BLCT2", "BLCT", "20140511122928", "20140812180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029930", "BLCT2", "BLCT", "20140511122928", "20140812180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040218", "BLCTYD", "BLCT", "20140511155036", "20140812180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040219", "BLCTYD", "BLCTMS", "20140511155036", "20140812180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029944", "BLCTZS", "BLCTMS", "20140511164049", "20140812180000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040152", "BLCTMS", "BLCT2", "20140509164006", "20140812200000", "52", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040150", "BLCTMS", "BLCTZS", "20140509164006", "20140812200000", "17", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040151", "BLCTMS", "BLCTZS", "20140509164006", "20140812200000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040162", "BLCTMS", "BLCT", "20140509164006", "20140812200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040169", "BLCTMS", "BLCT", "20140509164006", "20140812200000", "1", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040134", "BLCT2", "BLCT", "20140510115906", "20140812200000", "24", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040210", "BLCT2", "BLCTZS", "20140510164503", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040143", "BLCT2", "BLCT3", "20140510164503", "20140812200000", "14", "41
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040202", "BLCTYD", "BLCT2", "20140510164503", "20140812200000", "9", "39
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029914", "BLCT3", "BLCTMS", "20140510164503", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040191", "BLCTZS", "BLCTMS", "20140510164503", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040241", "BLCTYD", "BLCT", "20140511155036", "20140812200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040239", "BLCTYD", "BLCT", "20140511155036", "20140812200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040246", "BLCTMS", "BLCT2", "20140512172148", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040184", "BLCTMS", "BLCT", "20140512172148", "20140812200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040261", "BLCTMS", "BLCT", "20140512172148", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029997", "BLCT2", "BLCT", "20140513124710", "20140812200000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029999", "BLCT2", "BLCTMS", "20140513124710", "20140812200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029951", "BLCTZS", "BLCT", "20140511164049", "20140812220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029952", "BLCTZS", "BLCT", "20140511164049", "20140812220000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040226", "BLCTZS", "BLCT3", "20140511155036", "20140813080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029992", "BLCT2", "BLCTYD", "20140513180150", "20140813080000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029889", "BLCTZS", "BLCTYD", "20140508170935", "20140813081100", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000269", "BLCT2", "ZHCT", "20140511135736", "20140813120000", "22", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000270", "BLCTZS", "BLCT3", "20140511135736", "20140813120000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029935", "BLCTMS", "BLCTZS", "20140511155036", "20140813120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040094", "BLCT", "BLCTMS", "20140507170807", "20140813160000", "3", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040116", "BLCTMS", "BLCT2", "20140509164006", "20140813160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040193", "BLCTMS", "BLCT3", "20140510164503", "20140813160000", "12", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040199", "BLCTYD", "BLCTZS", "20140510164503", "20140813160000", "12", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040198", "BLCTMS", "BLCT3", "20140510164503", "20140813160000", "7", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040194", "BLCTMS", "BLCT3", "20140510164503", "20140813160000", "5", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040237", "BLCTMS", "BLCTZS", "20140511155036", "20140813160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000275", "ZHCT", "BLCT3", "20140512111036", "20140813160000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029955", "BLCT2", "BLCT3", "20140512111036", "20140813160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040260", "BLCT2", "BLCTZS", "20140512125553", "20140813160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029960", "BLCT", "BLCT2", "20140512172148", "20140813160000", "39", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029961", "BLCT", "BLCT2", "20140512172148", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029962", "BLCT", "BLCT2", "20140512172148", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029970", "BLCT", "BLCTZS", "20140512172148", "20140813160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029969", "BLCT", "BLCTYD", "20140512172148", "20140813160000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040230", "BLCTMS", "BLCTYD", "20140512172148", "20140813160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040225", "BLCTMS", "BLCTZS", "20140512172148", "20140813160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040217", "BLCTMS", "BLCTYD", "20140512172148", "20140813160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029948", "BLCT3", "BLCTZS", "20140512172148", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040231", "BLCT3", "BLCT2", "20140512172148", "20140813160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029975", "BLCT2", "BLCTYD", "20140512172148", "20140813160000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029967", "BLCT2", "BLCTYD", "20140512172148", "20140813160000", "13", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029959", "BLCT2", "BLCTYD", "20140512172148", "20140813160000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029971", "BLCT2", "BLCT3", "20140512172148", "20140813160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029972", "BLCTZS", "BLCT3", "20140512172148", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029973", "BLCTZS", "BLCT3", "20140512172148", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029974", "BLCTYD", "BLCT2", "20140512172148", "20140813160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029966", "BLCT3", "BLCTMS", "20140512172148", "20140813160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000277", "DXCTE", "BLCTZS", "20140512191100", "20140813160000", "17", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029984", "BLCT3", "BLCT", "20140513162243", "20140813160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029968", "BLCT", "BLCTYD", "20140512172148", "20140813170000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040228", "BLCTYD", "BLCT", "20140511155036", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029994", "BLCT2", "BLCT", "20140513124710", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029995", "BLCT2", "BLCT", "20140513124710", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029988", "BLCT2", "BLCTYD", "20140513124710", "20140813180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029986", "BLCT2", "BLCT3", "20140513124710", "20140813180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029998", "BLCT2", "BLCT", "20140513124710", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029977", "BLCT2", "BLCTYD", "20140513162243", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040290", "BLCT", "BLCT3", "20140513180150", "20140813180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040172", "BLCTMS", "BLCT3", "20140509164006", "20140813200000", "14", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040173", "BLCTMS", "BLCT3", "20140509164006", "20140813200000", "8", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040171", "BLCTMS", "BLCT3", "20140509164006", "20140813200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040132", "BLCT", "BLCTMS", "20140509164006", "20140813200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029906", "BLCT", "BLCTZS", "20140509164006", "20140813200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029907", "BLCT", "BLCT3", "20140509164006", "20140813200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040232", "BLCT", "BLCT2", "20140511155036", "20140813200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040233", "BLCT2", "BLCTMS", "20140511155036", "20140813200000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040234", "BLCT", "BLCTMS", "20140511155036", "20140813200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040170", "BLCTMS", "BLCT3", "20140511155036", "20140813200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029981", "BLCTZS", "BLCTYD", "20140513141850", "20140813200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000280", "BLCTZS", "BLCTYD", "20140512191100", "20140813230000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029989", "BLCT3", "BLCT", "20140513141850", "20140813230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029982", "BLCT3", "BLCTZS", "20140513141850", "20140813230000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029980", "BLCT2", "BLCTYD", "20140513141850", "20140814060000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040270", "BLCTZS", "BLCTYD", "20140513162243", "20140814080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040227", "BLCT2", "BLCT", "20140513162243", "20140814080000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040269", "BLCTZS", "BLCTYD", "20140513162243", "20140814080000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030007", "BLCT", "BLCTZS", "20140513180150", "20140814080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030008", "BLCT", "BLCTZS", "20140513180150", "20140814080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000264", "BLCT3", "BLCT", "20140509095545", "20140814081100", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029905", "BLCT", "BLCT2", "20140509164006", "20140814110000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029953", "BLCTZS", "BLCT", "20140511164049", "20140814120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030002", "BLCTMS", "BLCTZS", "20140513162243", "20140814120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99039985", "BLCT3", "BLCTMS", "20140513183304", "20140814120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000263", "BLCTZS", "BLCT2", "20140508193534", "20140814130000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029979", "BLCT2", "BLCT3", "20140513162243", "20140814150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000285", "BLCT3", "BLCT2", "20140513095722", "20140814160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029996", "BLCT", "BLCT2", "20140513162243", "20140814160000", "37", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040216", "BLCT2", "BLCTYD", "20140513162243", "20140814160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040307", "BLCT", "BLCTMS", "20140515153833", "20140814160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040299", "BLCT2", "BLCTMS", "20140515153833", "20140814160000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000265", "DXCTE", "BLCTZS", "20140509182402", "20140814170000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030001", "BLCT2", "BLCTZS", "20140513124710", "20140814180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040277", "BLCTYD", "BLCTZS", "20140513162243", "20140814180000", "3", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030006", "BLCT2", "BLCT3", "20140513162243", "20140814180000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040276", "BLCTYD", "BLCTZS", "20140513162243", "20140814180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040278", "BLCTYD", "BLCTZS", "20140513162243", "20140814180000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040252", "BLCTMS", "BLCT2", "20140513162243", "20140814180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040154", "BLCTMS", "BLCTZS", "20140509164006", "20140814200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040149", "BLCT", "BLCT2", "20140510115906", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040109", "BLCT2", "BLCTMS", "20140510115906", "20140814200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000267", "BLCT2", "BLCT", "20140510150210", "20140814200000", "48", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040208", "BLCTYD", "BLCT", "20140510164503", "20140814200000", "7", "78
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029950", "BLCT3", "BLCTMS", "20140511164049", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040236", "BLCT3", "BLCT2", "20140512125553", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040262", "BLCTZS", "BLCT2", "20140512125553", "20140814200000", "23", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040266", "BLCTMS", "BLCTZS", "20140512172148", "20140814200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040197", "BLCTMS", "BLCT3", "20140512172148", "20140814200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040212", "BLCTMS", "BLCT3", "20140512172148", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040265", "BLCTMS", "BLCT3", "20140512172148", "20140814200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040243", "BLCTMS", "BLCTYD", "20140512172148", "20140814200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029965", "BLCT2", "BLCTYD", "20140512172148", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029957", "BLCT2", "BLCT3", "20140512172148", "20140814200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040263", "BLCTZS", "BLCTMS", "20140512172148", "20140814200000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029954", "BLCTZS", "BLCTYD", "20140512172148", "20140814200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029963", "BLCTZS", "BLCTYD", "20140512172148", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029964", "BLCTZS", "BLCT3", "20140512172148", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000276", "BLCT", "BLCT2", "20140512191100", "20140814200000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000278", "BLCT3", "BLCT", "20140512191100", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000279", "BLCT2", "BLCTYD", "20140512191100", "20140814200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000281", "BLCT3", "BLCT", "20140512191100", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000282", "BLCTZS", "BLCT3", "20140512191100", "20140814200000", "23", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029985", "BLCT2", "BLCT3", "20140513124710", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029983", "BLCT3", "BLCTZS", "20140513141850", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000286", "BLCT", "ZHCT", "20140513150703", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030003", "BLCT2", "BLCT3", "20140513162243", "20140814200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030004", "BLCT2", "BLCT3", "20140513162243", "20140814200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030005", "BLCTZS", "BLCT3", "20140513162243", "20140814200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030020", "BLCT2", "BLCTZS", "20140514165943", "20140814200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030025", "BLCT3", "BLCT2", "20140514165943", "20140814200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030024", "BLCT3", "BLCT", "20140514165943", "20140814200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040318", "BLCT3", "BLCTMS", "20140515113943", "20140814200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040317", "BLCTZS", "BLCT3", "20140514165943", "20140814220000", "3", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000287", "ZHCT", "BLCTZS", "20140513150703", "20140814230000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000288", "ZHCT", "BLCTZS", "20140513150703", "20140814230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000291", "BLCT3", "BLCTZS", "20140514165943", "20140815120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000292", "BLCT3", "ZHCT", "20140514165943", "20140815120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030017", "BLCT2", "BLCTZS", "20140514165943", "20140815120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030018", "BLCT", "BLCT2", "20140514165943", "20140815120000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030016", "BLCT3", "BLCT2", "20140514165943", "20140815120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040297", "BLCT3", "BLCT2", "20140515153833", "20140815120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030026", "BLCTYD", "BLCT2", "20140514165943", "20140815140000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040314", "BLCTYD", "BLCTMS", "20140514165943", "20140815140000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040163", "BLCT2", "BLCTZS", "20140509164006", "20140815160000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040165", "BLCT2", "BLCTZS", "20140509164006", "20140815160000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040136", "BLCT2", "BLCT3", "20140510115906", "20140815160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040164", "BLCT2", "BLCTZS", "20140510115906", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040222", "BLCT", "BLCT2", "20140511155036", "20140815160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040238", "BLCTYD", "BLCT", "20140511155036", "20140815160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040258", "BLCT2", "BLCT", "20140512111036", "20140815160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040188", "BLCTZS", "BLCTMS", "20140512125553", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040244", "BLCTMS", "BLCT2", "20140512172148", "20140815160000", "10", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040247", "BLCTMS", "BLCT2", "20140512172148", "20140815160000", "10", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040245", "BLCTMS", "BLCT2", "20140512172148", "20140815160000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040268", "BLCTMS", "BLCT", "20140512172148", "20140815160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040251", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "5", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040255", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "0", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040256", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "21", "33
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040250", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040248", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040249", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040254", "BLCTMS", "BLCTZS", "20140512172148", "20140815160000", "48", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040264", "BLCTMS", "BLCT3", "20140512172148", "20140815160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040229", "BLCTMS", "BLCTYD", "20140512172148", "20140815160000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029958", "BLCT2", "BLCT3", "20140512172148", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040242", "BLCTZS", "BLCT3", "20140512172148", "20140815160000", "4", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029956", "BLCTZS", "BLCT3", "20140512172148", "20140815160000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040215", "BLCT2", "BLCTMS", "20140513162243", "20140815160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040214", "BLCT2", "BLCTMS", "20140513162243", "20140815160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030010", "BLCT2", "BLCT", "20140513180150", "20140815160000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030011", "BLCT2", "BLCT", "20140513180150", "20140815160000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030022", "BLCT", "BLCT3", "20140514165943", "20140815160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030021", "BLCT", "BLCT2", "20140514165943", "20140815160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040296", "BLCT3", "BLCT2", "20140515153833", "20140815160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040257", "BLCTMS", "BLCTZS", "20140512172148", "20140815170000", "11", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040315", "BLCTYD", "BLCTMS", "20140514165943", "20140815180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040283", "BLCT", "BLCTYD", "20140514165943", "20140815180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029912", "BLCTZS", "BLCT2", "20140509170339", "20140815200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029993", "BLCT2", "BLCT", "20140513124710", "20140815200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029991", "BLCT2", "BLCT", "20140513124710", "20140815200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040320", "BLCTYD", "BLCT", "20140515113943", "20140815200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030037", "BLCTZS", "BLCT2", "20140515143002", "20140815200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030038", "BLCTZS", "BLCT2", "20140515143002", "20140815200000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030033", "BLCTZS", "BLCT", "20140515143002", "20140815200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030030", "BLCTZS", "BLCT", "20140515143002", "20140815200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030036", "BLCTZS", "BLCT2", "20140515153833", "20140815200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000290", "BLCTYD", "BLCT2", "20140513162243", "20140815230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000295", "BLCTZS", "BLCT", "20140515115637", "20140815230000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029976", "BLCT2", "BLCTYD", "20140513162243", "20140816120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030039", "BLCT3", "BLCT2", "20140515150043", "20140816140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040253", "BLCTMS", "BLCTZS", "20140512172148", "20140816160000", "40", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030046", "BLCTZS", "BLCT", "20140515153833", "20140816160000", "25", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040306", "BLCT", "BLCT2", "20140516174621", "20140816160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030078", "BLCT2", "BLCTYD", "20140516174621", "20140816160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030077", "BLCT2", "BLCTYD", "20140516174621", "20140816160000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040161", "BLCT", "BLCT2", "20140509164006", "20140816200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040166", "BLCT", "BLCT2", "20140509164006", "20140816200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040240", "BLCTMS", "BLCTYD", "20140511155036", "20140816200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030019", "BLCT2", "BLCTZS", "20140514165943", "20140816200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030035", "BLCTZS", "BLCT", "20140515153833", "20140816200000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040288", "BLCT2", "BLCTZS", "20140515153833", "20140816200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040300", "BLCT2", "BLCTZS", "20140515153833", "20140816200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030049", "BLCT3", "BLCTMS", "20140515153833", "20140816200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030050", "BLCT3", "BLCTMS", "20140515153833", "20140816200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030060", "BLCT2", "BLCTYD", "20140516174621", "20140816200000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000297", "BLCT2", "DXCTE", "20140516174621", "20140816230000", "0", "24
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030070", "BLCT", "BLCT2", "20140516174621", "20140817080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000268", "BLCT2", "ZHCT", "20140511122928", "20140817120000", "17", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000293", "BLCT2", "BLCTYD", "20140515115637", "20140817120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000294", "BLCT2", "BLCT3", "20140515115637", "20140817120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040298", "BLCT3", "BLCT2", "20140515153833", "20140817120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000296", "DXCTE", "BLCTZS", "20140516174621", "20140817120000", "26", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040313", "BLCT", "BLCT2", "20140516174621", "20140817120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040349", "BLCT", "BLCTYD", "20140516174621", "20140817120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030062", "BLCT", "BLCT3", "20140516174621", "20140817120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040289", "BLCTYD", "BLCTMS", "20140516174621", "20140817120000", "3", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030063", "BLCT", "BLCT3", "20140516174621", "20140817120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030045", "BLCT3", "BLCT2", "20140515150043", "20140817140000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030042", "BLCT3", "BLCT", "20140515150043", "20140817140000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029877", "BLCTZS", "BLCT3", "20140509170339", "20140817150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030013", "BLCTZS", "BLCT", "20140514165943", "20140817150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030014", "BLCTZS", "BLCT", "20140514165943", "20140817150000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030058", "BLCT3", "BLCT", "20140516174621", "20140817170000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040352", "BLCT3", "BLCT", "20140516174621", "20140817170000", "4", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040285", "BLCTMS", "BLCT2", "20140513162243", "20140817180000", "5", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040275", "BLCTYD", "BLCT2", "20140513162243", "20140817180000", "6", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040292", "BLCTZS", "BLCT2", "20140513180150", "20140817180000", "10", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040284", "BLCTZS", "BLCTYD", "20140513180150", "20140817180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040350", "BLCT3", "BLCT", "20140516174621", "20140817180000", "30", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040272", "BLCTYD", "BLCT2", "20140516174621", "20140817180000", "2", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030086", "BLCT", "BLCT2", "20140517105539", "20140817180000", "9", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030084", "BLCT", "BLCT2", "20140517105539", "20140817180000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030083", "BLCT", "BLCT2", "20140517105539", "20140817180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030091", "BLCT", "BLCT2", "20140517105539", "20140817180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030074", "BLCTMS", "BLCT3", "20140517105539", "20140817180000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030072", "BLCTMS", "BLCT3", "20140517105539", "20140817180000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030073", "BLCTMS", "BLCT3", "20140517105539", "20140817180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030088", "BLCT", "BLCT2", "20140517105539", "20140817180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030080", "BLCT", "BLCT2", "20140517155419", "20140817180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030101", "BLCT3", "BLCTZS", "20140517184531", "20140817180000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040377", "BLCTZS", "BLCT3", "20140517184531", "20140817180000", "6", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030120", "BLCT2", "BLCT", "20140517184531", "20140817180000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030109", "BLCTYD", "BLCTZS", "20140517184531", "20140817180000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040336", "BLCTMS", "BLCTYD", "20140517184531", "20140817180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029990", "BLCT2", "BLCT", "20140513124710", "20140817200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030000", "BLCT2", "BLCTMS", "20140513124710", "20140817200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040340", "BLCT3", "BLCT2", "20140516174621", "20140817200000", "13", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040333", "BLCTMS", "BLCT2", "20140516174621", "20140817200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030052", "BLCTYD", "BLCT2", "20140516174621", "20140817200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000289", "BLCTZS", "ZHCT", "20140513150703", "20140817230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030031", "BLCTZS", "BLCT", "20140515153833", "20140817230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030034", "BLCTZS", "BLCT", "20140515153833", "20140817230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040338", "BLCT2", "BLCT3", "20140516174621", "20140818120000", "3", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030061", "BLCTYD", "BLCT", "20140516174621", "20140818120000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030044", "BLCT3", "BLCT2", "20140515150043", "20140818140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030040", "BLCT3", "BLCT2", "20140515150043", "20140818140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040200", "BLCTYD", "BLCTZS", "20140510164503", "20140818160000", "20", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040189", "BLCT", "BLCTMS", "20140513162243", "20140818160000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030009", "BLCT2", "BLCT", "20140514165943", "20140818160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040301", "BLCTMS", "BLCT2", "20140516174621", "20140818160000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040274", "BLCTMS", "BLCT2", "20140516174621", "20140818160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040273", "BLCTMS", "BLCT2", "20140516174621", "20140818160000", "0", "17
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040221", "BLCTYD", "BLCT2", "20140516174621", "20140818160000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030076", "BLCTYD", "BLCT", "20140516174621", "20140818160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030093", "BLCTZS", "BLCT3", "20140517105539", "20140818160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040326", "BLCTMS", "BLCTZS", "20140517105539", "20140818160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040327", "BLCTMS", "BLCT2", "20140517105539", "20140818160000", "32", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040324", "BLCTMS", "BLCT2", "20140517105539", "20140818160000", "91", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030094", "BLCTZS", "BLCT3", "20140517105539", "20140818160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030092", "BLCTZS", "BLCT3", "20140517105539", "20140818160000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000300", "BLCT2", "BLCT", "20140517134555", "20140818160000", "34", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000301", "BLCTZS", "BLCT2", "20140517155419", "20140818160000", "10", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000299", "BLCT2", "BLCT", "20140517155419", "20140818160000", "2", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040364", "BLCT3", "BLCT", "20140517155419", "20140818160000", "0", "25
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040411", "BLCTMS", "BLCT2", "20140520171518", "20140818160000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040291", "BLCTZS", "BLCT2", "20140513180150", "20140818180000", "70", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040316", "BLCT2", "BLCT3", "20140514165943", "20140818200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030032", "BLCTZS", "BLCT", "20140515150043", "20140818200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040339", "BLCTZS", "BLCTYD", "20140516174621", "20140818200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040329", "BLCTMS", "BLCT2", "20140516174621", "20140818200000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040330", "BLCTMS", "BLCT2", "20140516174621", "20140818200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040331", "BLCTMS", "BLCT2", "20140516174621", "20140818200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030075", "BLCTYD", "BLCTZS", "20140516174621", "20140818200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000298", "ZHCT", "BLCTZS", "20140517105539", "20140818200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030126", "BLCT3", "BLCTZS", "20140518170502", "20140818200000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030131", "BLCT2", "BLCT3", "20140518170502", "20140818200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030130", "BLCT2", "BLCT3", "20140518170502", "20140818200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030132", "BLCT2", "BLCT3", "20140518170502", "20140818200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030137", "BLCT3", "BLCTZS", "20140518222010", "20140818200000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030071", "BLCT", "BLCT3", "20140516174621", "20140818220000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040376", "BLCT", "BLCT3", "20140517155419", "20140818220000", "4", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030087", "BLCTMS", "BLCTZS", "20140517155419", "20140818220000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030089", "BLCTMS", "BLCTZS", "20140517155419", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030114", "BLCTMS", "BLCT2", "20140517155419", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030113", "BLCTMS", "BLCT2", "20140517155419", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030112", "BLCTMS", "BLCT2", "20140517155419", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030110", "BLCTMS", "BLCT2", "20140517155419", "20140818220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030111", "BLCTMS", "BLCT3", "20140517155419", "20140818220000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030085", "BLCTMS", "BLCTZS", "20140517155419", "20140818220000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040375", "BLCT2", "BLCT3", "20140517155419", "20140818220000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030100", "BLCT3", "BLCTZS", "20140517184531", "20140818220000", "9", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030099", "BLCT3", "BLCTZS", "20140517184531", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030096", "BLCT3", "BLCTZS", "20140517184531", "20140818220000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030108", "BLCT3", "BLCTZS", "20140517184531", "20140818220000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000302", "BLCTZS", "BLCT", "20140517184531", "20140818220000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030116", "BLCTZS", "BLCT2", "20140517184531", "20140818220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040356", "BLCT2", "BLCTZS", "20140517184531", "20140818220000", "2", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040355", "BLCT2", "BLCTZS", "20140517184531", "20140818220000", "13", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040358", "BLCTYD", "BLCT", "20140517184531", "20140818220000", "16", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040354", "BLCTYD", "BLCT2", "20140517184531", "20140818220000", "4", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040353", "BLCTYD", "BLCT2", "20140517184531", "20140818220000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030104", "BLCTYD", "BLCTZS", "20140517184531", "20140818220000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030105", "BLCTYD", "BLCTZS", "20140517184531", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030097", "BLCTYD", "BLCTZS", "20140517184531", "20140818220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040279", "BLCTMS", "BLCT3", "20140517184531", "20140818220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040280", "BLCTMS", "BLCT3", "20140517184531", "20140818220000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040281", "BLCTMS", "BLCT3", "20140517184531", "20140818220000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040282", "BLCTMS", "BLCTYD", "20140517184531", "20140818220000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030103", "BLCTMS", "BLCTYD", "20140517184531", "20140818220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040267", "BLCTMS", "BLCT2", "20140513162243", "20140818230000", "10", "22
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030028", "BLCTZS", "BLCT", "20140515153833", "20140818230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030029", "BLCTZS", "BLCT", "20140515153833", "20140818230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030135", "BLCTZS", "BLCT3", "20140518170502", "20140819040000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030134", "BLCTZS", "BLCT3", "20140518170502", "20140819040000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030136", "BLCT2", "BLCT3", "20140518170502", "20140819040000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000304", "BLCT3", "BLCT", "20140518170502", "20140819090000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000306", "DXCTE", "BLCTZS", "20140518170502", "20140819100000", "9", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029978", "BLCT2", "BLCT3", "20140513162243", "20140819120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040293", "BLCT3", "BLCT", "20140515153833", "20140819120000", "13", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040308", "BLCT3", "BLCTMS", "20140515153833", "20140819120000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040295", "BLCT3", "BLCT2", "20140515153833", "20140819120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040305", "BLCT", "BLCT2", "20140516174621", "20140819120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040312", "BLCT", "BLCT2", "20140516174621", "20140819120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030055", "BLCT3", "BLCT2", "20140516174621", "20140819120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030142", "BLCT3", "BLCT2", "20140518170502", "20140819120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030138", "BLCT", "BLCT2", "20140518170502", "20140819120000", "1", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030140", "BLCT", "BLCT2", "20140518170502", "20140819120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030139", "BLCT", "BLCT2", "20140518170502", "20140819120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030150", "BLCT3", "BLCTMS", "20140519161420", "20140819120000", "36", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030166", "BLCT2", "BLCT", "20140519161420", "20140819120000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000308", "BLCT3", "BLCT", "20140518192704", "20140819140000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000309", "BLCT3", "BLCT2", "20140518192704", "20140819140000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000305", "ZHCT", "BLCT3", "20140518170502", "20140819150000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030143", "BLCT3", "BLCT2", "20140518170502", "20140819150000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000303", "BLCT2", "BLCT", "20140518170502", "20140819150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030125", "BLCT2", "BLCT", "20140518170502", "20140819160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040383", "BLCT2", "BLCT", "20140518170502", "20140819160000", "17", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040388", "BLCTMS", "BLCT2", "20140519164917", "20140819160000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040423", "BLCT2", "BLCT3", "20140519172249", "20140819160000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040335", "BLCT", "BLCTYD", "20140516174621", "20140819180000", "38", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030090", "BLCT", "BLCT2", "20140517105539", "20140819180000", "17", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040286", "BLCTYD", "BLCTZS", "20140518170502", "20140819180000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040382", "BLCTYD", "BLCTZS", "20140518170502", "20140819180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040381", "BLCTYD", "BLCTZS", "20140518170502", "20140819180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99029987", "BLCT2", "BLCTMS", "20140513124710", "20140819200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030047", "BLCT3", "BLCTMS", "20140515153833", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030048", "BLCT3", "BLCTMS", "20140515153833", "20140819200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030051", "BLCT3", "BLCTMS", "20140515153833", "20140819200000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030043", "BLCT3", "BLCTMS", "20140515153833", "20140819200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030059", "BLCT3", "BLCT", "20140516174621", "20140819200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030079", "BLCTYD", "BLCTMS", "20140516174621", "20140819200000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040323", "BLCTYD", "BLCT2", "20140516174621", "20140819200000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030069", "BLCTYD", "BLCT", "20140516174621", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040337", "BLCTYD", "BLCT", "20140516174621", "20140819200000", "1", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040322", "BLCTYD", "BLCT", "20140516174621", "20140819200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030144", "BLCT3", "BLCT2", "20140518170502", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040287", "BLCTYD", "BLCTZS", "20140518170502", "20140819200000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030147", "BLCT3", "BLCT2", "20140518170502", "20140819200000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030128", "BLCT", "BLCT2", "20140518170502", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030145", "BLCT2", "BLCT", "20140518170502", "20140819200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030159", "BLCT3", "BLCT", "20140519161420", "20140819200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040368", "BLCT2", "BLCT", "20140519161420", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030151", "BLCT3", "BLCTZS", "20140519161420", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030169", "BLCT2", "BLCT3", "20140519161420", "20140819200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030167", "BLCT3", "BLCT2", "20140519161420", "20140819200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030273", "BLCTZS", "BLCT", "20140523124259", "20140819200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040363", "BLCTMS", "BLCT2", "20140517155419", "20140819220000", "50", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030095", "BLCT", "BLCTZS", "20140517184531", "20140819220000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030115", "BLCT", "BLCT2", "20140517184531", "20140819220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030121", "BLCT2", "BLCT", "20140517184531", "20140819220000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030081", "BLCT2", "BLCT", "20140517184531", "20140819220000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030118", "BLCT2", "BLCT", "20140517184531", "20140819220000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040369", "BLCTYD", "BLCT", "20140517184531", "20140819220000", "0", "26
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040370", "BLCTYD", "BLCT2", "20140517184531", "20140819220000", "9", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030107", "BLCTYD", "BLCTZS", "20140517184531", "20140819220000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030098", "BLCTYD", "BLCTZS", "20140517184531", "20140819220000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040418", "BLCTZS", "BLCT", "20140519161420", "20140819220000", "26", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040371", "BLCTMS", "BLCT", "20140517155419", "20140819230000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030102", "BLCTMS", "BLCTYD", "20140517184531", "20140819230000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030155", "BLCT2", "BLCT", "20140519161420", "20140819230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030153", "BLCT2", "BLCT", "20140519161420", "20140819230000", "1", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030154", "BLCT2", "BLCT", "20140519161420", "20140819230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030156", "BLCT2", "BLCT", "20140519161420", "20140819230000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030165", "BLCT3", "BLCTMS", "20140519161420", "20140820060000", "2", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040416", "BLCTZS", "BLCTYD", "20140520135352", "20140820080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030172", "BLCTZS", "BLCT2", "20140519172249", "20140820110000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040303", "BLCT", "BLCTMS", "20140516174621", "20140820120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040302", "BLCT", "BLCTMS", "20140516174621", "20140820120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030054", "BLCTZS", "BLCT2", "20140516174621", "20140820120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040259", "BLCT3", "BLCT2", "20140516174621", "20140820120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040384", "BLCT3", "BLCT2", "20140519161420", "20140820120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040424", "BLCTZS", "BLCT", "20140519161420", "20140820120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030170", "BLCT", "BLCT3", "20140519161420", "20140820120000", "1", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030168", "BLCT", "BLCT2", "20140519161420", "20140820120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030163", "BLCT2", "BLCT", "20140519161420", "20140820120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030161", "BLCT3", "BLCTMS", "20140519161420", "20140820120000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030041", "BLCT3", "BLCT", "20140515150043", "20140820140000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030184", "BLCT2", "BLCT", "20140520171518", "20140820140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030127", "BLCT3", "BLCTZS", "20140518170502", "20140820160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040408", "BLCTMS", "BLCTZS", "20140520171518", "20140820160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030202", "BLCT2", "BLCT3", "20140520171518", "20140820160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040399", "BLCT3", "BLCT2", "20140520171518", "20140820160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040395", "BLCTYD", "BLCT", "20140518170502", "20140820170000", "24", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000313", "BLCTZS", "BLCT3", "20140520105120", "20140820173000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030066", "BLCTYD", "BLCT", "20140516174621", "20140820180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040394", "BLCT3", "BLCT", "20140518170502", "20140820180000", "2", "39
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030123", "BLCTZS", "BLCT3", "20140518170502", "20140820180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040397", "BLCT2", "BLCT", "20140518170502", "20140820180000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040396", "BLCT", "BLCT3", "20140518170502", "20140820180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030124", "BLCT2", "BLCT3", "20140518170502", "20140820180000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030190", "BLCT3", "BLCTMS", "20140520114337", "20140820180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040095", "BLCT", "BLCT3", "20140507170807", "20140820200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040321", "BLCTYD", "BLCT", "20140515113943", "20140820200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040332", "BLCTMS", "BLCTZS", "20140516174621", "20140820200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040343", "BLCTMS", "BLCT", "20140517105539", "20140820200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040341", "BLCTMS", "BLCT", "20140517105539", "20140820200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040373", "BLCTMS", "BLCT2", "20140517155419", "20140820200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040365", "BLCTYD", "BLCT", "20140517155419", "20140820200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040407", "BLCTZS", "BLCTMS", "20140519161420", "20140820200000", "2", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040415", "BLCTMS", "BLCTZS", "20140519164917", "20140820200000", "34", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040414", "BLCTMS", "BLCTYD", "20140519164917", "20140820200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030171", "BLCT", "BLCT2", "20140519172249", "20140820230000", "3", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030182", "BLCT3", "BLCTMS", "20140520132001", "20140820230000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030181", "BLCT3", "BLCTMS", "20140520132001", "20140820230000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030178", "BLCT2", "BLCT", "20140520132001", "20140820230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030179", "BLCT2", "BLCT", "20140520132001", "20140820230000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040427", "BLCTMS", "BLCT2", "20140520171518", "20140820230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040406", "BLCTZS", "BLCTMS", "20140519161420", "20140821080000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000310", "BLCTZS", "BLCT3", "20140520105120", "20140821080000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030186", "BLCT", "BLCT3", "20140520132001", "20140821080000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040440", "BLCTYD", "BLCTZS", "20140520135352", "20140821080000", "3", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040445", "BLCTZS", "BLCTYD", "20140520135352", "20140821080000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040441", "BLCTYD", "BLCTZS", "20140520135352", "20140821080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040444", "BLCT", "BLCT3", "20140520171518", "20140821080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040447", "BLCTMS", "BLCTYD", "20140520171518", "20140821080000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040439", "BLCT3", "BLCTZS", "20140520172402", "20140821080000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040311", "BLCT", "BLCT2", "20140516174621", "20140821120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040310", "BLCT", "BLCTMS", "20140516174621", "20140821120000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040309", "BLCT", "BLCTYD", "20140516174621", "20140821120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030056", "BLCT3", "BLCT2", "20140516174621", "20140821120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040334", "BLCT", "BLCT2", "20140518170502", "20140821120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030160", "BLCT3", "BLCTMS", "20140519161420", "20140821120000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040367", "BLCT2", "BLCTMS", "20140519161420", "20140821120000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040428", "BLCTZS", "BLCT3", "20140519161420", "20140821120000", "17", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000312", "BLCTZS", "BLCTYD", "20140520105120", "20140821120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000314", "BLCTZS", "BLCTMS", "20140520105120", "20140821120000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000311", "BLCTZS", "BLCT2", "20140520105120", "20140821120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030180", "BLCT2", "BLCT", "20140520171518", "20140821120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000323", "BLCT2", "BLCTZS", "20140521074755", "20140821120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030206", "BLCT2", "BLCTZS", "20140521152142", "20140821120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030207", "BLCT2", "BLCTZS", "20140521152142", "20140821120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030192", "BLCTMS", "BLCTZS", "20140520171518", "20140821140000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030191", "BLCTZS", "BLCT3", "20140520171518", "20140821150000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030023", "BLCT2", "BLCTMS", "20140514165943", "20140821160000", "5", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040294", "BLCT3", "BLCT2", "20140515153833", "20140821160000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040392", "BLCT2", "BLCTZS", "20140518170502", "20140821160000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040391", "BLCT2", "BLCTZS", "20140518170502", "20140821160000", "0", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040393", "BLCT2", "BLCTZS", "20140518170502", "20140821160000", "0", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000319", "BLCTZS", "BLCT2", "20140520171518", "20140821160000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030185", "BLCTZS", "BLCT3", "20140520171518", "20140821160000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030187", "BLCTZS", "BLCT3", "20140520171518", "20140821160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030189", "BLCTZS", "BLCT3", "20140520171518", "20140821160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040456", "BLCTZS", "BLCTYD", "20140520171518", "20140821160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040413", "BLCTMS", "BLCT2", "20140520171518", "20140821160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040433", "BLCTZS", "BLCTMS", "20140519161420", "20140821180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030174", "BLCT", "BLCTYD", "20140520135352", "20140821180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030197", "BLCT2", "BLCT", "20140520171518", "20140821180000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030213", "BLCT2", "BLCTYD", "20140521175629", "20140821180000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030209", "BLCT2", "BLCTYD", "20140521175629", "20140821180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040344", "BLCTMS", "BLCT3", "20140517105539", "20140821200000", "4", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040342", "BLCTMS", "BLCT3", "20140517105539", "20140821200000", "3", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040357", "BLCTMS", "BLCTZS", "20140517155419", "20140821200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040351", "BLCT", "BLCT3", "20140517155419", "20140821200000", "14", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030106", "BLCT3", "BLCT2", "20140517184531", "20140821200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040366", "BLCT", "BLCTMS", "20140517184531", "20140821200000", "68", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040372", "BLCT2", "BLCTMS", "20140517184531", "20140821200000", "32", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040347", "BLCT2", "BLCTYD", "20140517184531", "20140821200000", "9", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040348", "BLCT2", "BLCT3", "20140517184531", "20140821200000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030119", "BLCT2", "BLCT", "20140517184531", "20140821200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040346", "BLCT2", "BLCT", "20140517184531", "20140821200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040360", "BLCTMS", "BLCT3", "20140517184531", "20140821200000", "2", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040361", "BLCTMS", "BLCT3", "20140517184531", "20140821200000", "1", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040359", "BLCTMS", "BLCTYD", "20140517184531", "20140821200000", "20", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040362", "BLCTMS", "BLCTYD", "20140517184531", "20140821200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030129", "BLCTZS", "BLCT3", "20140518170502", "20140821200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040379", "BLCT", "BLCT3", "20140518170502", "20140821200000", "5", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040380", "BLCT2", "BLCT3", "20140518170502", "20140821200000", "4", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030133", "BLCTYD", "BLCT", "20140518170502", "20140821200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030157", "BLCT2", "BLCT", "20140519161420", "20140821200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030158", "BLCT3", "BLCTMS", "20140519161420", "20140821200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030176", "BLCT", "BLCTZS", "20140520135352", "20140821200000", "11", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030194", "BLCT2", "BLCT", "20140520171518", "20140821200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030200", "BLCT3", "BLCTMS", "20140520171518", "20140821200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030211", "BLCT2", "BLCTYD", "20140521152142", "20140821200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030212", "BLCT2", "BLCTYD", "20140521175629", "20140821200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030216", "BLCT2", "BLCTYD", "20140521175629", "20140821200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000321", "BLCT2", "BLCTYD", "20140521074755", "20140821230000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030224", "BLCT2", "BLCT", "20140521175629", "20140821230000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030223", "BLCT2", "BLCT", "20140521175629", "20140821230000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000316", "ZHCT", "BLCTZS", "20140520114337", "20140822000000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040304", "BLCT", "BLCT2", "20140516174621", "20140822120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030141", "BLCT3", "BLCT2", "20140518170502", "20140822120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000315", "BLCTZS", "BLCT2", "20140520114337", "20140822120000", "16", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000318", "BLCT2", "BLCTZS", "20140520171518", "20140822120000", "9", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030183", "BLCTZS", "BLCT2", "20140520171518", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040421", "BLCT2", "BLCTZS", "20140520171518", "20140822120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000320", "ZHCT", "BLCTZS", "20140521074755", "20140822120000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000322", "ZHCT", "BLCTZS", "20140521074755", "20140822120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030226", "BLCT2", "BLCTZS", "20140521175629", "20140822120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030225", "BLCT2", "BLCT3", "20140521175629", "20140822120000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030208", "BLCT2", "BLCTZS", "20140521175629", "20140822120000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030146", "BLCT2", "BLCT", "20140518170502", "20140822150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040389", "BLCTMS", "BLCT2", "20140519164917", "20140822160000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040410", "BLCTMS", "BLCT2", "20140520171518", "20140822160000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030067", "BLCTYD", "BLCT", "20140516174621", "20140822180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030065", "BLCTYD", "BLCT", "20140516174621", "20140822180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030162", "BLCT3", "BLCT", "20140519161420", "20140822180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030164", "BLCT3", "BLCT", "20140519161420", "20140822180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040387", "BLCTYD", "BLCT", "20140519161420", "20140822180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000317", "DXCTE", "BLCTZS", "20140520114337", "20140822180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030177", "BLCT", "BLCTYD", "20140520135352", "20140822180000", "38", "13
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030210", "BLCT2", "BLCT", "20140521152142", "20140822180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030219", "BLCT3", "BLCT2", "20140521175629", "20140822180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030245", "BLCT3", "BLCT2", "20140521175629", "20140822180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030244", "BLCT3", "BLCT2", "20140521175629", "20140822180000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030238", "BLCT2", "BLCTYD", "20140521175629", "20140822180000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040451", "BLCTZS", "BLCT2", "20140521175629", "20140822180000", "66", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040461", "BLCT3", "BLCT2", "20140521175629", "20140822180000", "16", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030082", "BLCTYD", "BLCT", "20140517105539", "20140822200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040271", "BLCTMS", "BLCTYD", "20140517155419", "20140822200000", "24", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040374", "BLCTMS", "BLCT3", "20140517155419", "20140822200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030117", "BLCT2", "BLCT", "20140517184531", "20140822200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040405", "BLCTMS", "BLCTZS", "20140519164917", "20140822200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040404", "BLCTMS", "BLCTZS", "20140519164917", "20140822200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040403", "BLCTMS", "BLCTZS", "20140519164917", "20140822200000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040402", "BLCTMS", "BLCTZS", "20140519164917", "20140822200000", "5", "28
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040398", "BLCTMS", "BLCTZS", "20140519164917", "20140822200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030193", "BLCT2", "BLCT3", "20140520171518", "20140822200000", "26", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040438", "BLCTMS", "BLCTZS", "20140520171518", "20140822200000", "14", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000324", "BLCT2", "DXCTE", "20140522113143", "20140822200000", "8", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030256", "BLCT2", "BLCTYD", "20140522113143", "20140822200000", "0", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030255", "BLCT2", "BLCTYD", "20140522113143", "20140822200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030276", "BLCT3", "BLCT", "20140523124259", "20140822200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030272", "BLCTZS", "BLCT", "20140523124259", "20140822200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030122", "BLCT", "BLCT3", "20140517184531", "20140822220000", "6", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030233", "BLCT2", "BLCT", "20140521175629", "20140822230000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000327", "BLCT", "BLCT3", "20140522113143", "20140823060000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040425", "BLCT2", "BLCTMS", "20140519161420", "20140823120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040446", "BLCTMS", "BLCTYD", "20140520171518", "20140823120000", "13", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000328", "BLCTYD", "BLCTZS", "20140522113143", "20140823140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000329", "BLCTYD", "BLCT2", "20140522113143", "20140823140000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040486", "BLCT", "BLCTMS", "20140522154835", "20140823140000", "2", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030281", "BLCT2", "BLCT", "20140523172453", "20140823140000", "0", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030148", "BLCT2", "BLCTYD", "20140518170502", "20140823150000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030204", "BLCT2", "BLCTYD", "20140520171518", "20140823150000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030220", "BLCT3", "BLCT2", "20140521175629", "20140823150000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040412", "BLCTMS", "BLCT2", "20140520171518", "20140823160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040435", "BLCT2", "BLCT3", "20140520171518", "20140823160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040455", "BLCT2", "BLCTZS", "20140521152142", "20140823160000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030240", "BLCT3", "BLCTMS", "20140521175629", "20140823160000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030242", "BLCT3", "BLCT2", "20140521175629", "20140823160000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030278", "BLCT2", "BLCTYD", "20140523172453", "20140823160000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030270", "BLCT2", "BLCTYD", "20140523172453", "20140823160000", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040432", "BLCTZS", "BLCT3", "20140519161420", "20140823180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040430", "BLCTMS", "BLCT3", "20140519164917", "20140823180000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040442", "BLCTYD", "BLCT2", "20140520135352", "20140823180000", "5", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040422", "BLCTYD", "BLCTMS", "20140519161420", "20140823200000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030152", "BLCT2", "BLCT", "20140519172249", "20140823200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030198", "BLCT2", "BLCT", "20140520171518", "20140823200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040448", "BLCTYD", "BLCTMS", "20140520171518", "20140823200000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040419", "BLCTYD", "BLCTMS", "20140520171518", "20140823200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030234", "BLCT2", "BLCT", "20140521175629", "20140823200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030205", "BLCT2", "BLCTYD", "20140521175629", "20140823200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000325", "BLCTZS", "ZHCT", "20140522113143", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000326", "BLCTZS", "ZHCT", "20140522113143", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030261", "BLCT", "BLCT2", "20140523124259", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030259", "BLCT", "BLCTZS", "20140523124259", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030268", "BLCT3", "BLCT2", "20140523124259", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000332", "DXCTE", "BLCTZS", "20140523124259", "20140823200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040489", "BLCTMS", "BLCT2", "20140523172453", "20140823200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030227", "BLCT2", "BLCTZS", "20140521175629", "20140823230000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040488", "BLCTYD", "BLCTMS", "20140523172453", "20140824080000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040426", "BLCTZS", "BLCT2", "20140519161420", "20140824120000", "5", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040460", "BLCT3", "BLCT2", "20140521175629", "20140824120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040480", "BLCT", "BLCT2", "20140521175629", "20140824120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040473", "BLCT", "BLCT2", "20140523154537", "20140824120000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030266", "BLCT3", "BLCT2", "20140523154537", "20140824120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030264", "BLCT2", "BLCTYD", "20140523154537", "20140824120000", "1", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000335", "BLCT2", "BLCTYD", "20140523154537", "20140824120000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000334", "BLCT2", "BLCT", "20140523154537", "20140824120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000333", "BLCT2", "BLCT3", "20140523154537", "20140824120000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030283", "BLCT2", "BLCT", "20140523172453", "20140824120000", "0", "20
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030285", "BLCT2", "BLCT3", "20140523172453", "20140824120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030201", "BLCTMS", "BLCTZS", "20140520171518", "20140824140000", "1", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000330", "BLCTYD", "BLCT", "20140522113143", "20140824140000", "0", "7
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040457", "BLCT2", "BLCTMS", "20140522154835", "20140824140000", "4", "11
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030246", "BLCT3", "BLCT2", "20140522154835", "20140824140000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040390", "BLCT2", "BLCTZS", "20140518170502", "20140824160000", "0", "23
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸T000307", "BLCT", "BLCT3", "20140518174316", "20140824160000", "0", "26
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040400", "BLCT2", "BLCT", "20140519161420", "20140824160000", "14", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030175", "BLCT3", "BLCTZS", "20140520132001", "20140824160000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040409", "BLCTMS", "BLCTZS", "20140520171518", "20140824160000", "1", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030228", "BLCT2", "BLCTZS", "20140521175629", "20140824160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030068", "BLCTYD", "BLCT", "20140516174621", "20140824180000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040386", "BLCTYD", "BLCT", "20140519161420", "20140824180000", "21", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040401", "BLCTMS", "BLCT", "20140519164917", "20140824180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040431", "BLCTMS", "BLCT3", "20140519164917", "20140824180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030221", "BLCT2", "BLCT", "20140521175629", "20140824180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030231", "BLCT2", "BLCT", "20140521175629", "20140824180000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030230", "BLCT2", "BLCT", "20140521175629", "20140824180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030229", "BLCT2", "BLCT", "20140521175629", "20140824180000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030280", "BLCT3", "BLCT2", "20140523172453", "20140824180000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040378", "BLCT", "BLCTYD", "20140518170502", "20140824200000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030188", "BLCT2", "BLCT", "20140520171518", "20140824200000", "6", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030196", "BLCT2", "BLCT", "20140520171518", "20140824200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030236", "BLCT2", "BLCTYD", "20140521175629", "20140824200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030235", "BLCT2", "BLCTYD", "20140521175629", "20140824200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030232", "BLCT2", "BLCT", "20140521175629", "20140824200000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030271", "BLCTZS", "BLCT3", "20140523124259", "20140824200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030275", "BLCT3", "BLCT", "20140523124259", "20140824200000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040496", "BLCTZS", "BLCTYD", "20140523165329", "20140824200000", "22", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040494", "BLCT3", "BLCT2", "20140523172453", "20140824200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030279", "BLCT3", "BLCT2", "20140523172453", "20140824200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040515", "BLCT3", "BLCT2", "20140524133224", "20140824200000", "1", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040497", "BLCTZS", "BLCTYD", "20140523172453", "20140824210000", "41", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040512", "BLCT2", "BLCT3", "20140524133224", "20140824220000", "21", "9
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040443", "BLCTZS", "BLCT2", "20140520172402", "20140825080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030296", "BLCT3", "BLCT2", "20140524133224", "20140825080000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040429", "BLCT2", "BLCT3", "20140519161420", "20140825120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040467", "BLCT2", "BLCT3", "20140523154537", "20140825120000", "11", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030277", "BLCT3", "BLCTMS", "20140523165650", "20140825120000", "40", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040498", "BLCT", "BLCT2", "20140524133224", "20140825120000", "12", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040518", "BLCT3", "BLCT2", "20140524133224", "20140825120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040520", "BLCT3", "BLCT2", "20140524133224", "20140825120000", "0", "36
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030239", "BLCT3", "BLCTMS", "20140521175629", "20140825140000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030258", "BLCTZS", "BLCT3", "20140522154835", "20140825140000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030249", "BLCT3", "BLCT2", "20140522154835", "20140825140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030247", "BLCT3", "BLCT2", "20140522154835", "20140825140000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030251", "BLCTYD", "BLCT2", "20140522154835", "20140825140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030250", "BLCTYD", "BLCT", "20140522154835", "20140825140000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040453", "BLCTMS", "BLCT", "20140520171518", "20140825160000", "75", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030203", "BLCT2", "BLCT3", "20140520171518", "20140825160000", "0", "12
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040470", "BLCT", "BLCTYD", "20140521152142", "20140825160000", "8", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030222", "BLCT2", "BLCT", "20140521175629", "20140825160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040476", "BLCT", "BLCT2", "20140524133224", "20140825160000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030195", "BLCT2", "BLCT", "20140520171518", "20140825180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030243", "BLCT3", "BLCT2", "20140521175629", "20140825180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030269", "BLCT3", "BLCTMS", "20140523172453", "20140825180000", "0", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040490", "BLCTMS", "BLCT2", "20140523172453", "20140825180000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030286", "BLCT2", "BLCTYD", "20140523172453", "20140825200000", "16", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030284", "BLCT2", "BLCTYD", "20140523172453", "20140825200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030265", "BLCT3", "BLCTMS", "20140523172453", "20140825200000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040511", "BLCTYD", "BLCT", "20140523154537", "20140826000000", "24", "30
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030173", "BLCT3", "BLCT", "20140520171518", "20140826080000", "0", "4
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040510", "BLCT2", "BLCT3", "20140523154537", "20140826080000", "3", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030260", "BLCT2", "BLCT", "20140523154537", "20140826080000", "0", "5
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030290", "BLCTZS", "BLCT", "20140524133224", "20140826080000", "0", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040449", "BLCTZS", "BLCT", "20140521175629", "20140826120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040459", "BLCT3", "BLCT", "20140521175629", "20140826120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040478", "BLCT", "BLCT2", "20140521175629", "20140826120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040474", "BLCT", "BLCT2", "20140523154537", "20140826120000", "7", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030267", "BLCT3", "BLCT2", "20140523154537", "20140826120000", "0", "15
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040466", "BLCT2", "BLCT", "20140523154537", "20140826120000", "5", "16
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030263", "BLCT2", "BLCT", "20140523154537", "20140826120000", "6", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030262", "BLCT3", "BLCT", "20140523154537", "20140826120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040504", "BLCT", "BLCT2", "20140524133224", "20140826120000", "41", "42
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040521", "BLCT3", "BLCT", "20140524133224", "20140826120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040519", "BLCT3", "BLCT", "20140524133224", "20140826120000", "0", "34
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040484", "BLCT2", "BLCT3", "20140522154835", "20140826140000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030248", "BLCT3", "BLCT2", "20140522154835", "20140826140000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030253", "BLCTYD", "BLCTMS", "20140522154835", "20140826140000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030257", "BLCT3", "BLCTMS", "20140522154835", "20140826140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030254", "BLCTYD", "BLCT", "20140522154835", "20140826140000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030282", "BLCT3", "BLCT", "20140523172453", "20140826150000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030217", "BLCT2", "BLCTMS", "20140521175629", "20140826160000", "13", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030218", "BLCT2", "BLCT", "20140521175629", "20140826160000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030149", "BLCTYD", "BLCT", "20140519161420", "20140826180000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040462", "BLCT3", "BLCT2", "20140521175629", "20140826180000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040420", "BLCTYD", "BLCT", "20140520171518", "20140826200000", "4", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030199", "BLCT3", "BLCTMS", "20140520171518", "20140826200000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040485", "BLCT2", "BLCT3", "20140521175629", "20140826200000", "18", "25
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030292", "BLCT3", "BLCT", "20140524133224", "20140826200000", "3", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040385", "BLCTYD", "BLCT", "20140519161420", "20140826210000", "22", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040482", "BLCT", "BLCTMS", "20140521175629", "20140827120000", "2", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030237", "BLCT2", "BLCTYD", "20140521175629", "20140827120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040450", "BLCTZS", "BLCT2", "20140521175629", "20140827120000", "0", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040463", "BLCT3", "BLCTMS", "20140521175629", "20140827120000", "2", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040464", "BLCT3", "BLCTMS", "20140521175629", "20140827120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000331", "BLCT2", "ZHCT", "20140523092347", "20140827120000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040465", "BLCT2", "BLCTYD", "20140523154537", "20140827120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040436", "BLCT3", "BLCT", "20140523154537", "20140827120000", "11", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040468", "BLCT2", "BLCTMS", "20140523154537", "20140827120000", "8", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040469", "BLCT2", "BLCTMS", "20140523154537", "20140827120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030291", "BLCT3", "BLCT2", "20140524133224", "20140827120000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030252", "BLCTYD", "BLCT2", "20140522154835", "20140827140000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030294", "BLCTZS", "BLCTYD", "20140524133224", "20140827180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030293", "BLCT3", "BLCT", "20140524133224", "20140827180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040507", "BLCTMS", "BLCT3", "20140523165650", "20140827200000", "4", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040505", "BLCTMS", "BLCT3", "20140523165650", "20140827200000", "4", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040491", "BLCTMS", "BLCT2", "20140523165650", "20140827200000", "75", "18
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040492", "BLCTMS", "BLCT2", "20140523172453", "20140827200000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040477", "BLCT", "BLCTYD", "20140521175629", "20140828120000", "0", "3
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040479", "BLCT", "BLCTMS", "20140521175629", "20140828120000", "3", "8
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040434", "BLCT2", "BLCTMS", "20140523154537", "20140828120000", "0", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040437", "BLCT3", "BLCT2", "20140523154537", "20140828120000", "0", "29
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040471", "BLCT", "BLCTYD", "20140523154537", "20140828120000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("内贸000336", "BLCT2", "DXCTE", "20140523154537", "20140828120000", "0", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040472", "BLCT", "BLCTMS", "20140523154537", "20140828120000", "8", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030214", "BLCT2", "BLCT", "20140521175629", "20140828160000", "1", "6
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030215", "BLCT2", "BLCTMS", "20140521175629", "20140828160000", "4", "10
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040522", "BLCT2", "BLCTMS", "20140524133224", "20140828160000", "57", "14
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040523", "BLCT2", "BLCTMS", "20140524133224", "20140828160000", "59", "25
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030274", "BLCT3", "BLCT", "20140523124259", "20140828200000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040517", "BLCT3", "BLCTMS", "20140524133224", "20140828200000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040483", "BLCT", "BLCT2", "20140521175629", "20140829120000", "2", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040475", "BLCT", "BLCT2", "20140523154537", "20140829120000", "0", "2
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040524", "BLCT2", "BLCT", "20140524133224", "20140829120000", "19", "29
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T12040481", "BLCT", "BLCT2", "20140521175629", "20140829130000", "1", "1
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030295", "BLCTZS", "BLCT3", "20140524133224", "20140829180000", "1", "0
");

INSERT INTO Commodity ( id, source, destination, available_t, deadline, small, large) VALUES ("T99030299", "BLCT3", "BLCTZS", "20140524133224", "20140830060000", "0", "1
");

INSERT INTO Task (commodity_id, route_id, sequence_num, planned_start, planned_finish, small, large) VALUES ("内贸000004", "1", "1", "20140721160000", "20140721190500", "15", "11");
INSERT INTO Task (commodity_id, route_id, sequence_num, planned_start, planned_finish, small, large) VALUES ("T12020086", "2", "1", "20140721170600", "20140721201400", "2", "0");
INSERT INTO Task (commodity_id, route_id, sequence_num, planned_start, planned_finish, small, large) VALUES ("T99028412", "3", "1", "20140721165500", "20140721235500","8", "1");