DROP TABLE IF EXISTS Task,Route,Commodity,Ports,Gps,Login,Session,Vehicle,Driver;

CREATE TABLE Driver (

	driverName VARCHAR(100) NOT NULL,
	password VARCHAR(100) NOT NULL,
	
	CONSTRAINT driverPK PRIMARY KEY (driverName)
);

CREATE TABLE Vehicle (

	vehicleID VARCHAR(100) NOT NULL,
	
	CONSTRAINT vehiclePK PRIMARY KEY (vehicleID)
);

CREATE TABLE Session (

	sessionID INT AUTO_INCREMENT,
	session_driverName VARCHAR(100),
	session_vehicleID VARCHAR(100),
	startTime VARCHAR(25), 
	expireTime VARCHAR(25),
    
	CONSTRAINT sessionPK PRIMARY KEY (sessionID),
	CONSTRAINT session_driverFK FOREIGN KEY(session_driverName) REFERENCES Driver(driverName),
    CONSTRAINT session_vehicleFK FOREIGN KEY(session_vehicleID) REFERENCES Vehicle(vehicleID),
    CONSTRAINT session_unique UNIQUE(session_driverName,session_vehicleID,startTime,expireTime)
);

CREATE TABLE Login (

	loginID INT NOT NULL AUTO_INCREMENT,
	login_driverName VARCHAR(100),
	login_vehicleID VARCHAR(100),
	login_password VARCHAR(100),
	login_success TINYINT(1),
	login_time VARCHAR(25),
	
	CONSTRAINT loginPK PRIMARY KEY (loginID)
);


CREATE TABLE Gps (
	gpsID INT NOT NULL AUTO_INCREMENT,
	gps_sessionID INT NOT NULL,
	longitude DOUBLE,
	latitude DOUBLE,
	gps_time VARCHAR(25),
	
	CONSTRAINT gpsPK PRIMARY KEY (gpsID),
	CONSTRAINT gps_sessionFK FOREIGN KEY(gps_sessionID) REFERENCES Session(sessionID)
);



CREATE TABLE Ports (

	name VARCHAR(20) NOT NULL,
	gps_l DOUBLE NOT NULL,
	gps_a DOUBLE NOT NULL,

	CONSTRAINT portsPK PRIMARY KEY (name)
);

CREATE TABLE Commodity (

	id VARCHAR(50) NOT NULL,
	source VARCHAR(20) NOT NULL,
	destination VARCHAR(20) NOT NULL,
	available_t DATETIME NOT NULL,
	deadline DATETIME NOT NULL,
	small INT NOT NULL,
	large INT NOT NULL,
	finished_small INT NOT NULL DEFAULT 0,
	finished_large INT NOT NULL DEFAULT 0,

	CONSTRAINT commPK PRIMARY KEY (id),
	CONSTRAINT commPortsFK1 FOREIGN KEY (source) REFERENCES Ports (name),
	CONSTRAINT commPortsFK2 FOREIGN KEY (destination) REFERENCES Ports (name)
);

CREATE TABLE Route (

    id INT AUTO_INCREMENT,
	sessionID INT,
    shift_normal_start_time DATETIME NOT NULL,
    route_num INT NOT NULL,
	status INT NOT NULL DEFAULT 0,

	CONSTRAINT routePK PRIMARY KEY (id),
    CONSTRAINT shift_route UNIQUE (shift_normal_start_time, route_num),
    CONSTRAINT rouSessFK FOREIGN KEY (sessionID) REFERENCES Session (sessionID)
);

CREATE TABLE Task (

    id INT AUTO_INCREMENT,
	commodity_id VARCHAR(50) NOT NULL,
	route_id INT,
	sequence_num INT NOT NULL,
    small INT NOT NULL,
    large INT NOT NULL,
    planned_start DATETIME,
    planned_finish DATETIME,
	actual_start DATETIME,
	actual_finish DATETIME,
	status INT NOT NULL DEFAULT 0,

	CONSTRAINT taskPK PRIMARY KEY (id),
	CONSTRAINT taskCommFK FOREIGN KEY (commodity_id) REFERENCES Commodity (id),
	CONSTRAINT taskRouteFK FOREIGN KEY (route_id) REFERENCES Route (id)
);