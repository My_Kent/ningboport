/**
 * Implement and provides interface to access local data, e.g., session and tasks.
 * Implement and provides interface to handle network communications.
 * 
 *  @author Jiaqi LI
 */
package ac.uk.nottingham.ningboport.controller;