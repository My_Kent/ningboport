/**
 * Provides methods to access and modify configurations for the application. <br />
 * Use Preference Data to place all configurations.<br /> 
 * please see Configuration.java for usage. <br />
 * 
 * @see ac.uk.nottingham.ningboport.conf.Configuration
 * @author Jiaqi LI
 */
package ac.uk.nottingham.ningboport.conf;