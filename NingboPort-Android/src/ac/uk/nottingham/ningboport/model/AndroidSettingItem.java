package ac.uk.nottingham.ningboport.model;

/**
 * This is the view object for settings' list
 * @author Jiaqi LI
 *
 */
public class AndroidSettingItem {

	private String title;
	private String content;
	
	public AndroidSettingItem(String t, String c){
		title = t;
		content =c;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
