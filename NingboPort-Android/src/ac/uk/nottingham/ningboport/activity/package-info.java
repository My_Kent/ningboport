/**
 * Provides activities (UI) for the application. <br />
 * <ul>
 * <li>Login activity for startup</li>
 * <li>Settings activity for displaying and changing configurations</li>
 * <li>About activity for displaying credits<li>
 * <li>TaskList activity for displaying the route (task list)</li>
 * <li>MapRookie activity for showing map</li>
 * </ul>
 * 
 * 
 * @author Jiaqi LI
 */
package ac.uk.nottingham.ningboport.activity;