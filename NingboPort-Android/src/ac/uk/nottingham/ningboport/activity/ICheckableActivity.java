package ac.uk.nottingham.ningboport.activity;

/**
 * Any activity implements this interface will provide a method which should
 * determine whether this activity is running or not.
 * 
 * @author Jiaqi LI
 * 
 */
public interface ICheckableActivity {

	/**
	 * Determine whether an activity is running or not.
	 * 
	 * @return true is the activity is running, false otherwise.
	 */
	public boolean isRunning();
}
