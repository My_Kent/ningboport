package ac.uk.nottingham.ningboport.activity;

import ac.uk.nottingham.ningboport.controller.PeriodicUpdater;
import android.app.Application;
import android.app.Service;
import android.os.Vibrator;
import android.util.Log;



import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.GeofenceClient;
import com.baidu.location.LocationClient;

public class LocationApplication extends Application{
	public LocationClient mLocationClient;
	public GeofenceClient mGeofenceClient;
	public MyLocationListener mMyLocationListener;
	
//	public TextView mLocationResult,logMsg;
//	public TextView trigger,exit;
	public Vibrator mVibrator;
	
	public PeriodicUpdater mPeriodicUpdater;
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d("TEST", "2");
		mLocationClient = new LocationClient(this.getApplicationContext());
		mMyLocationListener = new MyLocationListener();
		Log.d("TEST", "3");
		mLocationClient.registerLocationListener(mMyLocationListener);
	    mGeofenceClient = new GeofenceClient(getApplicationContext());
		Log.d("TEST", "4");
		
		mVibrator =(Vibrator)getApplicationContext().getSystemService(Service.VIBRATOR_SERVICE);
		
	}

	
	/**
	 */
	public class MyLocationListener implements BDLocationListener {
		
		@Override
		public void onReceiveLocation(BDLocation location) {
			//Receive Location 
//			Log.d("TEST", "5");
//			StringBuffer sb = new StringBuffer(256);
//			sb.append("time : ");
//			sb.append(location.getTime());
//			Double d1 = location.getLatitude();
//			String s1 = d1.toString();
//			Log.d("TEST", s1);
//			
//			Double d2 = location.getLongitude();
//			String s2 = d2.toString();
//			Log.d("TEST",s2);
//			
//			sb.append("\nerror code : ");
//			sb.append(location.getLocType());
//			sb.append("\nlatitude : ");
//			sb.append(location.getLatitude());
//			sb.append("\nlontitude : ");
//			sb.append(location.getLongitude());
//			sb.append("\nradius : ");
//			sb.append(location.getRadius());
//			if (location.getLocType() == BDLocation.TypeGpsLocation){
//				sb.append("\nspeed : ");
//				sb.append(location.getSpeed());
//				sb.append("\nsatellite : ");
//				sb.append(location.getSatelliteNumber());
//				sb.append("\ndirection : ");
//				sb.append("\naddr : ");
//				sb.append(location.getAddrStr());
//				sb.append(location.getDirection());
//			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation){
//				sb.append("\naddr : ");
//				sb.append(location.getAddrStr());
//
//				sb.append("\noperationers : ");
//				sb.append(location.getOperators());
//			}
//			Log.d("TEST", "6");
//			logMsg(sb.toString());
//			Log.d("TEST", "7");
//			Log.i("BaiduLocationApiDem", sb.toString());
			
			mPeriodicUpdater.setLatitude(location.getLatitude());
			mPeriodicUpdater.setLongitude(location.getLongitude());
		}


	}
	
	
//	/**
//	 * @param str
//	 */
//	public void logMsg(String str) {
//		try {
//			if (mLocationResult != null)
//			{
//				mLocationResult.setText(str);
//			}
//			else{
//				Log.d("TEST", "81");
//			}
//		} catch (Exception e) {
//			Log.d("TEST", "82");
//			e.printStackTrace();
//		}
//	}

	
}
