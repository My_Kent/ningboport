package ac.uk.nottingham.ningboport.activity;

import android.app.Application;

import com.baidu.mapapi.SDKInitializer;

public class DemoApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		// 在使用 SDK �?�组间之�?�?始化 context 信�?�，传入 ApplicationContext
		SDKInitializer.initialize(this);
	}

}