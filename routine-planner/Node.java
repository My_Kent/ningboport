package uk.ac.nottingham.ningboport.planner;

import java.util.Vector;

public class Node
{
  public int index;
  public String id;
  public int loadTime;
  public int unloadTime;

  public Node(String id, int loadTime, int unloadTime)
  {
    this.id = id;
    this.loadTime = loadTime;
    this.unloadTime = unloadTime;
  }

  public static Node findByID(String id, Network nw) {
    for (int i = 0; i < nw.nodes.size(); ++i)
      if (((Node)nw.nodes.elementAt(i)).id.equals(id))
        return ((Node)nw.nodes.elementAt(i));

    RoutingPlanner.errormsg(id + " not found in the network. check your commodity file");
    System.exit(0);
    return null;
  }

  public String toString() {
    return this.id;
  }
}