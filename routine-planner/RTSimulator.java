package uk.ac.nottingham.ningboport.planner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Vector;
import uk.ac.nottingham.ningboport.planner.algorithms.Insertion;

public class RTSimulator
{
  public Vector<Task> ta = new Vector();
  public Vector<Vector<Commodity>> mandatoryCommodities;
  public Vector<Vector<Commodity>> optionalCommodities;
  private Network nw;
  private Calendar eventTime;
  private Vector<Route> delayedRoute = new Vector();
  private Vector<Integer> delayedAmount = new Vector();

  public void startRoutingSimulation()
  {
    System.out.println("----------------RT EVENT@-------------------!");
    this.nw.setFinishedTask(this.eventTime);
    int numberOfShifts = this.nw.getNumberOfShifts();
    for (int i = 0; i < numberOfShifts; ++i) {
      this.nw.removeUnfinishedTask(i);
    }

    this.nw.taskSet.addAll(this.ta);
    for (i = 0; i < this.mandatoryCommodities.size(); ++i) {
      ((Vector)this.nw.mandatoryCommodities.get(i)).addAll((Collection)this.mandatoryCommodities.get(i));
      ((Vector)this.nw.optionalCommodities.get(i)).addAll((Collection)this.optionalCommodities.get(i));
    }

    for (i = 0; i < this.delayedRoute.size(); ++i)
    {
      ((Route)this.delayedRoute.get(i)).setRouteDelay(((Integer)this.delayedAmount.get(i)).intValue());
    }

    Insertion ins2 = new Insertion(this.nw, true, false);
    ins2.run();
  }

  public void readFile(String path, Network network)
    throws IOException
  {
    String lineStr;
    this.nw = network;
    int numberOfShifts = this.nw.getNumberOfShifts();
    this.mandatoryCommodities = new Vector();
    this.optionalCommodities = new Vector();
    for (int i = 0; i < numberOfShifts; ++i) {
      this.mandatoryCommodities.add(new Vector());
      this.optionalCommodities.add(new Vector());
    }

    BufferedReader br = new BufferedReader(new FileReader(path));
    Vector rtFileSV = new Vector();

    while ((lineStr = br.readLine()) != null)
      rtFileSV.add(lineStr);

    br.close();
    parseRTFile(rtFileSV);
  }

  private void parseRTFile(Vector<String> rtFileSV) {
    boolean parsingDelays = false;
    Vector delayLines = new Vector();
    boolean parsingCommodities = false;
    Vector commodityLines = new Vector();
    for (int i = 0; i < rtFileSV.size(); ++i) {
      String[] as = ((String)rtFileSV.get(i)).split("\t| ");
      if (as[0].equals("#event_time")) {
        this.eventTime = RoutingPlanner.readCalString((String)rtFileSV.get(i + 1));
      } else if (as[0].equals("#delays")) {
        parsingDelays = true;
        parsingCommodities = false;
      } else if (as[0].equals("#additional")) {
        parsingCommodities = true;
        parsingDelays = false;
      }
      else if (parsingDelays) {
        delayLines.add((String)rtFileSV.get(i));
      } else if (parsingCommodities) {
        commodityLines.add((String)rtFileSV.get(i));
      }

    }

    parseCommodity(commodityLines);
    parseRouteDelay(delayLines);
  }

  private int eventInPeriod(Calendar eventTime) {
    int numberOfShifts = this.nw.getNumberOfShifts();
    for (int i = 0; i < numberOfShifts; ++i)
      if ((eventTime.after(this.nw.periodStartTimes[i])) && 
        (eventTime.before(this.nw.periodEndTimes[i])))
        return i;


    return -1;
  }

  private void parseRouteDelay(Vector<String> delayLines) {
    int eventPeriodIndex = eventInPeriod(this.eventTime);

    if (eventPeriodIndex < 0)
      return;
    Vector eventPeriod = (Vector)this.nw.routesOfShifts.get(eventPeriodIndex);
    for (int i = 0; i < delayLines.size(); ++i) {
      String[] s = ((String)delayLines.get(i)).split("\t| ");
      if (s.length != 2) {
        RoutingPlanner.errormsg("Error in Route delay definition: \n\t" + ((String)delayLines.get(i)));
      }
      else
      {
        this.delayedRoute.add((Route)eventPeriod.get(new Integer(s[0]).intValue()));
        this.delayedAmount.add(new Integer(s[1])); }
    }
  }

  private void parseCommodity(Vector<String> cdata) {
    Calendar allPeriodsStartTime = this.nw.periodStartTimes[0];
    Calendar allPeriodsDeadline = this.nw.periodEndTimes[(this.nw.periodEndTimes.length - 1)];
    cdata.remove(0);
    for (int i = 0; i < cdata.size(); ++i) {
      String[] ts = ((String)cdata.elementAt(i)).split("\t");

      if (ts.length != 7) {
        RoutingPlanner.errormsg("Wrong RTdata file! " + ts.length);
        System.exit(0);
      }

      Node from = Node.findByID(ts[1], this.nw);
      int fromI = this.nw.nodes.indexOf(from);
      Node to = Node.findByID(ts[2], this.nw);
      int toI = this.nw.nodes.indexOf(to);
      double fuel = this.nw.travelingDistances[fromI][toI] / 100.0D;
      double time = this.nw.travelingTimesWithQueue[fromI][toI];

      Calendar commodityAvailTime = RoutingPlanner.readCalString(ts[3]);

      if (commodityAvailTime.before(allPeriodsStartTime))
        commodityAvailTime = (Calendar)allPeriodsStartTime.clone();

      if (commodityAvailTime.after(allPeriodsDeadline))
        continue;

      Calendar commodityDeadline = RoutingPlanner.readCalString(ts[4]);

      if (commodityDeadline.before(allPeriodsStartTime)) {
        continue;
      }

      Calendar latestStartTime = (Calendar)commodityDeadline.clone();
      latestStartTime.add(12, 
        -(int)(time + this.nw.travelingTimes[this.nw.depot.index][fromI]));

      if (commodityAvailTime.after(latestStartTime)) {
        RoutingPlanner.errormsg("Error: commodity is available after emergency time (no time to finish)! Delete this entry!" + 
          i);
        System.out.println((String)cdata.elementAt(i));
      }
      else
      {
        if (latestStartTime.compareTo(allPeriodsStartTime) < 0)
          continue;

        Calendar earliestFinishTime = (Calendar)commodityAvailTime.clone();
        earliestFinishTime.add(12, 
          (int)time + (int)this.nw.travelingTimes[this.nw.depot.index][fromI]);

        if (earliestFinishTime.after(commodityDeadline))
          continue;

        Commodity c = new Commodity(ts[0], 
          commodityAvailTime, commodityDeadline, 
          from, to, Integer.parseInt(ts[5]), Integer.parseInt(ts[6]));
        this.nw.commodities.add(c);
        int total = c.small + c.large;
        for (int j = 0; j < total; ++j) {
          Task t = new Task();
          if (j >= c.small) t.size = 2;
          else t.size = 1;
          t.fuel = fuel;

          t.cmdt = c;

          this.ta.add(t);
        }

        int numberOfShifts = this.nw.getNumberOfShifts();
        for (int j = 0; j < numberOfShifts; ++j) {
          Calendar currentPeriodS = this.nw.periodStartTimes[j];
          Calendar currentPeriodE = this.nw.periodEndTimes[j];
          if (earliestFinishTime.before(currentPeriodE))
            if ((latestStartTime.before(currentPeriodE)) && (latestStartTime.after(currentPeriodS))) {
              c.latestPeriod = j;
              ((Vector)this.mandatoryCommodities.elementAt(j)).add(c);
            } else if (latestStartTime.after(currentPeriodE)) {
              c.completableShifts.add(new Integer(j));
              ((Vector)this.optionalCommodities.elementAt(j)).add(c);
            }
        }
      }
    }
  }
}