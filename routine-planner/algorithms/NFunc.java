package uk.ac.nottingham.ningboport.planner.algorithms;

import java.io.PrintStream;
import java.util.Vector;
import uk.ac.nottingham.ningboport.planner.Network;
import uk.ac.nottingham.ningboport.planner.Route;
import uk.ac.nottingham.ningboport.planner.RoutingPlanner;
import uk.ac.nottingham.ningboport.planner.Task;

public class NFunc
{
  private Network nw;

  NFunc(Network nw)
  {
    this.nw = nw;
  }

  public boolean moveSingleRoute(Route a, int index, int slot, boolean execute)
  {
    Vector taskSet = a.taskSet;

    Vector taskSetBak = (Vector)a.taskSet.clone();
    if ((index == slot) || (slot == index + 1))
      return false;

    if ((index >= taskSet.size()) || (index < 0)) {
      RoutingPlanner.errormsg("Error moving from node index " + index + " from route " + a);
      return false;
    }

    if ((slot > taskSet.size()) || (slot < 0)) {
      RoutingPlanner.errormsg("Error moving to node index " + slot + " of route " + a);
      return false;
    }

    Task t = (Task)taskSet.elementAt(index);
    taskSet.add(slot, t);
    if (taskSet.get(index) == t)
      taskSet.remove(index);
    else {
      taskSet.remove(index + 1);
    }

    if (a.check() > 0) {
      if (!(execute))
        a.taskSet = taskSetBak;

      return true;
    }
    a.taskSet = taskSetBak;
    return false;
  }

  public boolean moveTask(Route a, int index_a, Route b, int index_b, boolean execute)
  {
    if (a.equals(b)) {
      return moveSingleRoute(a, index_a, index_b, execute);
    }

    if (index_a >= a.taskSet.size()) {
      RoutingPlanner.errormsg("Error moving from node index " + a + " from route " + a);
      return false;
    }

    if (index_b > b.taskSet.size()) {
      RoutingPlanner.errormsg("Error moving to node index " + b + " of route " + b);
      return false;
    }

    Task t = (Task)a.taskSet.elementAt(index_a);
    b.taskSet.insertElementAt(t, index_b);
    a.taskSet.remove(index_a);

    if ((a.check() > 0) && (b.check() > 0))
    {
      if (!(execute)) {
        a.taskSet.insertElementAt(t, index_a);
        b.taskSet.remove(index_b);
      }
      return true;
    }
    a.taskSet.insertElementAt(t, index_a);
    b.taskSet.remove(index_b);

    return false;
  }

  private boolean swapTaskSingleRoute(Route a, int index_a, int index_b, boolean execute)
  {
    if (index_a == index_b)
      return false;

    if (index_a >= a.taskSet.size()) {
      RoutingPlanner.errormsg("Single: Error swapping node index_a " + index_a + " from route " + a);
      return false;
    }

    if (index_b >= a.taskSet.size()) {
      RoutingPlanner.errormsg("Single: Error swapping to node index_b " + index_b + " of route " + a);
      return false;
    }

    Task t1 = (Task)a.taskSet.elementAt(index_a);
    Task t2 = (Task)a.taskSet.elementAt(index_b);

    a.taskSet.set(index_a, t2);
    a.taskSet.set(index_b, t1);

    if (a.check() > 0) {
      if (!(execute)) {
        a.taskSet.set(index_a, t1);
        a.taskSet.set(index_b, t2);
      }
      return true;
    }
    a.taskSet.set(index_a, t1);
    a.taskSet.set(index_b, t2);
    return false;
  }

  public boolean swapTask(Route a, int index_a, Route b, int index_b, boolean execute)
  {
    if (a.equals(b))
      return swapTaskSingleRoute(a, index_a, index_b, execute);

    if (index_a >= a.taskSet.size()) {
      RoutingPlanner.errormsg("Error swapping node index(a side) " + index_a + " from route " + a);
      return false;
    }

    if (index_b >= b.taskSet.size()) {
      RoutingPlanner.errormsg("Error swapping to node index(b side) " + index_b + " of route " + b);
      return false;
    }

    Task t1 = (Task)a.taskSet.elementAt(index_a);
    Task t2 = (Task)b.taskSet.elementAt(index_b);

    a.taskSet.set(index_a, t2);
    b.taskSet.set(index_b, t1);

    if ((a.check() > 0) && (b.check() > 0)) {
      if (!(execute)) {
        a.taskSet.set(index_a, t1);
        b.taskSet.set(index_b, t2);
      }
      return true;
    }
    a.taskSet.set(index_a, t1);
    b.taskSet.set(index_b, t2);
    return false;
  }

  public int swapTask(Route r, int index, Vector<Task> tasks, int tIndex, boolean execute)
  {
    Task newOnRoute = (Task)tasks.get(tIndex);
    Vector old = (Vector)r.taskSet.clone();

    Task previousOnRoute = (Task)r.taskSet.set(index, newOnRoute);
    int res = r.check();
    if (res > 0)
      if (execute) {
        tasks.remove(tIndex);
        tasks.add(previousOnRoute);
      } else {
        r.taskSet = old;
      }

    else {
      r.taskSet = old;
    }

    return res;
  }

  public boolean twoOpt(Route a, int slotA, Route b, int slotB, boolean execute)
  {
    if (a.equals(b)) {
      return false;
    }

    Vector oldA = (Vector)a.taskSet.clone();
    Vector oldB = (Vector)b.taskSet.clone();

    Vector newA = new Vector();
    Vector newB = new Vector();

    newA.addAll(a.taskSet.subList(0, slotA + 1));
    newA.addAll(b.taskSet.subList(slotB + 1, b.taskSet.size()));
    newB.addAll(b.taskSet.subList(0, slotB + 1));
    newB.addAll(a.taskSet.subList(slotA + 1, a.taskSet.size()));
    a.taskSet = newA;
    b.taskSet = newB;

    if ((a.check() > 0) && (b.check() > 0)) {
      if (!(execute)) {
        a.taskSet = oldA;
        b.taskSet = oldB;
      }

      return true;
    }
    a.taskSet = oldA;
    b.taskSet = oldB;
    return false;
  }

  public void optimizeAllRoutes()
  {
    int numberOfShifts = this.nw.getNumberOfShifts();
    for (int i = 0; i < numberOfShifts; ++i)
      optimizeRouteInPeriod(i);
  }

  public void optimizeRouteInPeriod(int periodIndex)
  {
    Vector routesOfPeriod = (Vector)this.nw.routesOfShifts.get(periodIndex);
    for (int i = 0; i < routesOfPeriod.size(); ++i)
      optimizeRoute((Route)routesOfPeriod.get(i));
  }

  public void optimizeRoute(Route r)
  {
    if (r.check() < 1)
      System.out.println("Route optimizer: You gave me a bad route to optimize");

    Route empty = r.clone();
    empty.taskSet.clear();
    findFullRoutePerm(empty, r.taskSet, r);
  }

  private void findFullRoutePerm(Route r, Vector<Task> taskSet, Route bestR)
  {
    for (int i = 0; i < taskSet.size(); ++i) {
      Route nextRoute = r.clone();
      Vector nextTaskSet = (Vector)taskSet.clone();
      nextRoute.taskSet.add((Task)nextTaskSet.remove(i));

      if (nextRoute.check() > 0)
        if (nextTaskSet.size() == 0)
          if (Double.compare(nextRoute.emptyDistance, bestR.emptyDistance) < 0) {
            bestR.taskSet = nextRoute.taskSet;
            bestR.emptyDistance = nextRoute.emptyDistance;
          }

        else
          findFullRoutePerm(nextRoute, nextTaskSet, bestR);
    }
  }

  public void optimizePartialRoute(Route r, int start, int end)
  {
    if (r.check() < 1) {
      System.out.println("Route optimizer: You gave me a bad route to optimize");
      return;
    }

    int r_size = r.taskSet.size();

    if (r_size == 0)
      return;

    if ((start < 0) || (start > r_size) || 
      (end < 0) || (end > r_size) || (start > end)) {
      System.out.println("optimize partial route: bad index");
      return;
    }

    Route initial = r.clone();

    Vector taskSet = new Vector();
    Vector tail = new Vector();

    for (int i = r_size - 1; i > end; --i) {
      tail.add((Task)initial.taskSet.remove(i));
    }

    for (i = start; i <= end; ++i) {
      taskSet.add((Task)initial.taskSet.remove(start));
    }

    findRoutePerm(initial, taskSet, tail, r);
  }

  private void findRoutePerm(Route initial, Vector<Task> taskSet, Vector<Task> tail, Route bestR)
  {
    for (int i = 0; i < taskSet.size(); ++i) {
      Route testRoute = initial.clone(); Route nextTestRoute = initial.clone();
      Vector nextTaskSet = (Vector)taskSet.clone();
      nextTestRoute.taskSet.add((Task)nextTaskSet.get(i));
      testRoute.taskSet.add((Task)nextTaskSet.remove(i));
      if (tail != null) {
        testRoute.taskSet.addAll(tail);
      }

      if (testRoute.check() > 0)
        if (nextTaskSet.size() == 0)
          if (Double.compare(testRoute.emptyDistance, bestR.emptyDistance) < 0) {
            bestR.taskSet = testRoute.taskSet;
            bestR.emptyDistance = testRoute.emptyDistance;
          }

        else
          findRoutePerm(nextTestRoute, nextTaskSet, tail, bestR);
    }
  }

  public boolean cross(Route a, int startA, int lengthA, Route b, int startB, int lengthB, boolean executeCross)
  {
    if (a == b) { return false;
    }

    Vector taskSetA = a.taskSet; Vector taskSetB = b.taskSet;
    Vector backUpA = (Vector)taskSetA.clone(); Vector backUpB = (Vector)taskSetB.clone();

    Vector aMid = new Vector();
    Vector bMid = new Vector();
    for (int i = 0; i < lengthA; ++i) {
      aMid.add((Task)taskSetA.remove(startA));
    }

    for (i = 0; i < lengthB; ++i) {
      bMid.add((Task)taskSetB.remove(startB));
    }

    taskSetA.addAll(startA, bMid);

    taskSetB.addAll(startB, aMid);

    if ((a.check() > 0) && (b.check() > 0)) {
      if (!(executeCross)) {
        a.taskSet = backUpA;
        b.taskSet = backUpB;
        return true;
      }

      return true;
    }
    a.taskSet = backUpA;
    b.taskSet = backUpB;
    return false;
  }
}