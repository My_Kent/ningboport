package uk.ac.nottingham.ningboport.planner.algorithms;

import uk.ac.nottingham.ningboport.planner.Route;
import uk.ac.nottingham.ningboport.planner.Task;

class InsertAction
  implements Comparable<InsertAction>
{
  public int slot;
  public Route r;
  public Task t;
  public double score;

  public InsertAction(Route r, Task t)
  {
    this.r = r;
    this.t = t;
    this.score = -1.0D;
    this.slot = -1;
  }

  public int compareTo(InsertAction o) {
    if (this.score > o.score)
      return 1;

    if (this.score < o.score)
      return -1;

    return 0;
  }
}