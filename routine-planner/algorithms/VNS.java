package uk.ac.nottingham.ningboport.planner.algorithms;

import java.io.PrintStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import uk.ac.nottingham.ningboport.planner.Commodity;
import uk.ac.nottingham.ningboport.planner.Network;
import uk.ac.nottingham.ningboport.planner.Route;
import uk.ac.nottingham.ningboport.planner.RoutingPlanner;
import uk.ac.nottingham.ningboport.planner.Solution;
import uk.ac.nottingham.ningboport.planner.Task;

public class VNS extends NFunc
{
  private Network nw;
  private Solution bestSolution;
  private int tabuLength = 7;
  private boolean tabuEnabled = false;
  private Queue<Task> tabuList = new ArrayBlockingQueue(this.tabuLength);
  private Vector<Route> currentShift;
  private int currentShiftIndex;
  private int twoOpt = 0;
  private int move = 0;
  private int cross = 0;
  private int swap = 0;
  private int shakeI;

  public VNS(Network nw)
  {
    super(nw);
    this.nw = nw;
  }

  private boolean isTabu(Task o)
  {
    if (!(this.tabuEnabled))
      return false;

    Iterator it = this.tabuList.iterator();
    while (it.hasNext())
      if (((Task)it.next()).cmdt.id.equals(o.cmdt.id))
        return true;


    return false;
  }

  private void addToTabu(Task o)
  {
    if ((this.tabuLength == 0) || (!(this.tabuEnabled)))
      return;

    if (isTabu(o))
      return;

    if (this.tabuList.size() == this.tabuLength)
      this.tabuList.remove();

    this.tabuList.offer(o);
  }

  public void run(int maximumSeconds, int maximumShakeTime, int shakeBase, boolean tabuEnabled, boolean firstImprove)
  {
    System.out.println("=== TS Started ===");
    RoutingPlanner.addToOutput("sec:" + maximumSeconds + " mshaketime: " + maximumShakeTime + " shakebase: " + shakeBase + " ");
    RoutingPlanner.addToOutput(" tabu: " + tabuEnabled + " firstImprove: " + firstImprove + "\n");
    this.tabuEnabled = tabuEnabled;
    Calendar eTime = Calendar.getInstance();
    for (int i = 0; i < this.nw.routesOfShifts.size(); ++i) {
      this.currentShiftIndex = i;
      this.currentShift = ((Vector)this.nw.routesOfShifts.elementAt(i));

      eTime.add(13, maximumSeconds);
      int shakeTime = shakeBase;

      Vector mandatory = this.nw.getPeriodTaskSet(this.currentShiftIndex, true);
      Vector mandatory2 = new Vector();
      if (i != this.nw.routesOfShifts.size() - 1)
        mandatory2 = this.nw.getPeriodTaskSet(this.currentShiftIndex + 1, true);

      Vector optional = this.nw.getPeriodTaskSet(this.currentShiftIndex, false);

      int k = 0; int shakeBonus = 0;
      while ((Calendar.getInstance().before(eTime)) && (shakeBase + shakeBonus <= maximumShakeTime))
      {
        switch (k)
        {
        case 0:
          if (Double.compare(swap(true, firstImprove), 0D) <= 0) {
            ++k;
          } else {
            k = 0;
            this.swap += 1;
          }
          break;
        case 1:
          if (Double.compare(move(true, firstImprove), 0D) <= 0) {
            ++k;
          } else {
            k = 0;
            this.move += 1;
          }
          break;
        case 2:
          if (!(insertFromUnassigned(mandatory, firstImprove)))
            ++k;
          else
            k = 0;

          break;
        case 3:
          if (!(insertFromUnassigned(mandatory2, firstImprove)))
            ++k;
          else
            k = 0;

          break;
        case 4:
          if (!(insertFromUnassigned(optional, firstImprove)))
            ++k;
          else
            k = 0;

          break;
        case 5:
          if (Double.compare(twoOpt(true, firstImprove), 0D) <= 0) {
            ++k;
          } else {
            this.twoOpt += 1;
            k = 0;
          }
          break;
        case 6:
          if (Double.compare(crossExchange(true, firstImprove), 0D) <= 0) {
            ++k;
          } else {
            k = 0;
            this.cross += 1;
          }
          break;
        default:
          if (saveBetterSolution(mandatory, mandatory2, optional)) {
            this.shakeI = 0;
            shakeBonus = 0;
          }
          else {
            if (maximumShakeTime != shakeBase) {
              ++shakeBonus;
            }

            this.shakeI += 1;
            if (this.shakeI > 2)
              this.shakeI = 0;
            this.tabuLength += 1;
          }

          shakeTime = shakeBase + shakeBonus;
          for (int j = 0; j < shakeTime; ++j)
            shake(mandatory, mandatory2, optional);

          k = 0;
        }
      }

      this.bestSolution.restore();
      optimizeAllRoutes();
      mandatory = this.nw.getPeriodTaskSet(this.currentShiftIndex, true);
      System.out.print("m: " + mandatory.size());
      System.out.println(" OV: " + this.nw.getShiftObjectiveValues(i));
      this.nw.returnPeriodTasks(mandatory);
    }

    this.bestSolution.restore();
    optimizeAllRoutes();

    RoutingPlanner.addToOutput("finish time: " + Calendar.getInstance().getTime() + "\n");
    RoutingPlanner.addToOutput("twoOpt" + this.twoOpt + " move " + this.move + " cross " + this.cross + " swap " + this.swap);
  }

  public void run2(int maximumSeconds, int maximumShakeTime, int shakeBase, boolean tabuEnabled, boolean firstImprove)
  {
    System.out.println("=== TS(Test)Started ===");
    this.tabuEnabled = tabuEnabled;
    for (int i = 0; i < this.nw.routesOfShifts.size(); ++i) {
      this.currentShiftIndex = i;
      this.currentShift = ((Vector)this.nw.routesOfShifts.elementAt(i));

      Vector mandatory = this.nw.getPeriodTaskSet(this.currentShiftIndex, true);
      Vector mandatory2 = new Vector();
      if (i != this.nw.routesOfShifts.size() - 1)
        mandatory2 = this.nw.getPeriodTaskSet(this.currentShiftIndex + 1, true);

      Vector optional = this.nw.getPeriodTaskSet(this.currentShiftIndex, false);

      int k = 0;
      boolean run = true;
      while (run)
        switch (k)
        {
        case 0:
          if (Double.compare(twoOpt(true, firstImprove), 0D) <= 0)
            ++k;
          else
            k = 0;

          break;
        case 1:
          if (Double.compare(crossExchange(true, firstImprove), 0D) <= 0)
            ++k;
          else
            k = 0;

          break;
        case 2:
          if (Double.compare(swap(true, firstImprove), 0D) <= 0)
            ++k;
          else
            k = 0;

          break;
        case 3:
          if (Double.compare(move(true, firstImprove), 0D) <= 0)
            ++k;
          else
            k = 0;

          break;
        case 4:
          if (!(insertFromUnassigned(mandatory, firstImprove)))
            ++k;
          else
            k = 0;

          break;
        case 5:
          if (!(insertFromUnassigned(mandatory2, firstImprove)))
            ++k;
          else
            k = 0;

          break;
        case 6:
          if (!(insertFromUnassigned(optional, firstImprove)))
            ++k;
          else
            k = 0;

          break;
        default:
          saveBetterSolution(mandatory, mandatory2, optional);
          run = false;
        }


      this.bestSolution.restore();
      optimizeAllRoutes();
      mandatory = this.nw.getPeriodTaskSet(this.currentShiftIndex, true);
      System.out.print("m: " + mandatory.size());
      this.nw.returnPeriodTasks(mandatory);
      System.out.println(" OV: " + this.nw.getShiftObjectiveValues(i));
    }
    this.bestSolution.restore();
    optimizeAllRoutes();
  }

  public int taskCount(Vector<Task> mandatory, Vector<Task> mandatory2, Vector<Task> optional)
  {
    Vector n = new Vector();
    n.addAll(mandatory);
    n.addAll(mandatory2);
    n.addAll(optional);
    int count = 0;
    for (int i = 0; i < n.size(); ++i)
      if (((Task)n.get(i)).gT == null) {
        ++count;
      }
      else
      {
        count += 2;
      }


    return count;
  }

  private boolean saveBetterSolution(Vector<Task> m1, Vector<Task> m2, Vector<Task> o)
  {
    Vector otherTasks = new Vector();
    otherTasks.addAll(m1);
    otherTasks.addAll(m2);
    otherTasks.addAll(o);

    Solution currentSolution = new Solution(this.nw, otherTasks);
    if (this.bestSolution == null) {
      this.bestSolution = currentSolution;
    }
    else if (currentSolution.betterThan(this.bestSolution)) {
      System.out.print(this.shakeI);
      this.bestSolution = currentSolution;
    } else {
      System.out.print(".");
      return false;
    }

    return true;
  }

  private double swap(boolean descend, boolean firstImprove)
  {
    Vector routes = new Vector();
    int si = (this.currentShiftIndex > 0) ? this.currentShiftIndex : 0;
    int numberOfShifts = this.nw.getNumberOfShifts();
    for (; (si < this.currentShiftIndex + 3) && (si < numberOfShifts); ++si) {
      routes.addAll((Collection)this.nw.routesOfShifts.get(si));
    }

    int numberOfRoutes = routes.size();
    Vector r1 = new Vector();
    Vector r2 = new Vector();
    Vector index1 = new Vector();
    Vector index2 = new Vector();
    double bestEdReduce = 0D;

    for (int i = 0; i < numberOfRoutes; ++i) {
      Route routeI = (Route)routes.elementAt(i);
      int routeILength = routeI.taskSet.size();
      routeI.check();
      double cr_ed = routeI.emptyDistance + routeI.softTimeWindowPenalty;

      for (int j = 0; j < routeILength; ++j) {
        if (((Task)routeI.taskSet.get(j)).freezed())
          continue;

        if (isTabu((Task)routeI.taskSet.elementAt(j)))
          continue;

        for (int k = i; k < numberOfRoutes; ++k) {
          int l;
          Route routeK = (Route)routes.elementAt(k);
          int routeJLength = routeK.taskSet.size();
          routeK.check();
          double sr_ed = routeK.emptyDistance + routeK.softTimeWindowPenalty;

          if (k == i)
            l = j + 1;
          else
            l = 0;

          for (; l < routeJLength; ++l)
            if ((!(((Task)routeK.taskSet.get(l)).freezed())) && 
              (!(isTabu((Task)routeK.taskSet.elementAt(l))))) {
              if (((Task)routeI.taskSet.elementAt(j)).cmdt.id.equals(((Task)routeK.taskSet.elementAt(l)).cmdt.id))
                continue;

              if (swapTask(routeI, j, routeK, l, false)) {
                double edReduce = cr_ed - routeI.emptyDistance - routeI.softTimeWindowPenalty;
                if (i != k)
                  edReduce += sr_ed - routeK.emptyDistance - routeK.softTimeWindowPenalty;

                if (descend) {
                  if (Double.compare(edReduce, bestEdReduce) > 0) {
                    if (firstImprove) {
                      swapTask(routeI, j, routeK, l, true);
                      return edReduce;
                    }
                    bestEdReduce = edReduce;
                    r1.add(0, routeI);
                    r2.add(0, routeK);
                    index1.add(0, Integer.valueOf(j));
                    index2.add(0, Integer.valueOf(l));
                  }
                }
                else if (Double.compare(edReduce, 0D) != 0) {
                  r1.add(routeI);
                  r2.add(routeK);
                  index1.add(Integer.valueOf(j));
                  index2.add(Integer.valueOf(l));
                }

              }

              if (routeK.lastRouteCheckError == l)
                break;
            }

        }

      }

    }

    if (descend) {
      if (Double.compare(bestEdReduce, 0D) > 0) {
        addToTabu((Task)((Route)r1.get(0)).taskSet.get(((Integer)index1.get(0)).intValue()));
        addToTabu((Task)((Route)r2.get(0)).taskSet.get(((Integer)index2.get(0)).intValue()));
        swapTask((Route)r1.get(0), ((Integer)index1.get(0)).intValue(), (Route)r2.get(0), ((Integer)index2.get(0)).intValue(), true);
      }

      return bestEdReduce;
    }
    if (r1.size() == 0) return -1.0D;
    Random rg = new Random();
    int n = rg.nextInt(r1.size());
    addToTabu((Task)((Route)r1.get(n)).taskSet.get(((Integer)index1.get(n)).intValue()));
    addToTabu((Task)((Route)r2.get(n)).taskSet.get(((Integer)index2.get(n)).intValue()));
    swapTask((Route)r1.get(n), ((Integer)index1.get(n)).intValue(), (Route)r2.get(n), ((Integer)index2.get(n)).intValue(), true);
    return 1D;
  }

  private double move(boolean descend_mode, boolean firstImprove)
  {
    int r_size = this.currentShift.size();
    Vector routes = new Vector();
    int si = (this.currentShiftIndex > 0) ? this.currentShiftIndex : 0;
    int numberOfShifts = this.nw.getNumberOfShifts();
    for (; (si < this.currentShiftIndex + 3) && (si < numberOfShifts); ++si) {
      routes.addAll((Collection)this.nw.routesOfShifts.get(si));
    }

    Vector route1 = new Vector();
    Vector route2 = new Vector();
    Vector index1 = new Vector();
    Vector index2 = new Vector();
    double bestEdReduce = 0D;
    for (int i = 0; i < r_size; ++i) {
      Route currentRoute = (Route)routes.elementAt(i);
      int cr_size = currentRoute.taskSet.size();
      currentRoute.check();
      double cr_ed = currentRoute.emptyDistance + currentRoute.softTimeWindowPenalty;
      for (int j = 0; j < cr_size; ++j)
        if (!(((Task)currentRoute.taskSet.elementAt(j)).freezed())) {
          if (isTabu((Task)currentRoute.taskSet.elementAt(j))) {
            continue;
          }

          for (int k = 0; k < r_size; ++k) {
            Route insertRoute = (Route)routes.elementAt(k);
            int ir_size = insertRoute.taskSet.size();
            insertRoute.check();
            double ir_ed = insertRoute.emptyDistance + insertRoute.softTimeWindowPenalty;

            for (int l = 0; l <= ir_size; ++l) {
              if ((l != ir_size) && (((Task)insertRoute.taskSet.get(l)).freezed()))
                continue;

              if (moveTask(currentRoute, j, insertRoute, l, false)) {
                double edReduce = cr_ed - currentRoute.emptyDistance - currentRoute.softTimeWindowPenalty;
                if (i != k)
                  edReduce += ir_ed - insertRoute.emptyDistance - insertRoute.softTimeWindowPenalty;

                if (descend_mode) {
                  if (Double.compare(edReduce, bestEdReduce) > 0) {
                    if (firstImprove) {
                      moveTask(currentRoute, j, insertRoute, l, true);
                      return edReduce;
                    }
                    bestEdReduce = edReduce;
                    route1.add(0, currentRoute);
                    route2.add(0, insertRoute);
                    index1.add(0, Integer.valueOf(j));
                    index2.add(0, Integer.valueOf(l));
                  }
                }
                else if (Double.compare(edReduce, 0D) != 0) {
                  route1.add(currentRoute);
                  route2.add(insertRoute);
                  index1.add(Integer.valueOf(j));
                  index2.add(Integer.valueOf(l));
                }

              }

              if (insertRoute.lastRouteCheckError == l) break;
              if (insertRoute.lastRouteCheckError == -1)
                break;
            }
          }

        }


    }

    if (descend_mode) {
      if (Double.compare(bestEdReduce, 0D) > 0) {
        addToTabu((Task)((Route)route1.get(0)).taskSet.elementAt(((Integer)index1.get(0)).intValue()));
        moveTask((Route)route1.get(0), ((Integer)index1.get(0)).intValue(), (Route)route2.get(0), ((Integer)index2.get(0)).intValue(), true);
      }
      return bestEdReduce;
    }

    if (route1.size() == 0) return -1.0D;
    Random rg = new Random();
    int n = rg.nextInt(route1.size());

    addToTabu((Task)((Route)route1.get(n)).taskSet.get(((Integer)index1.get(n)).intValue()));
    moveTask((Route)route1.get(n), ((Integer)index1.get(n)).intValue(), (Route)route2.get(n), ((Integer)index2.get(n)).intValue(), true);
    return 1D;
  }

  private boolean insertFromUnassigned(Vector<Task> unassigned, boolean firstImprove)
  {
    int ta_size = unassigned.size();
    double rsize = this.currentShift.size();

    Vector r = new Vector();
    Vector slot = new Vector();
    Vector ta = new Vector();

    double BestEmptyRateDecrease = -99999999999999.0D;
    for (int k = 0; k < ta_size; ++k) {
      Task t = (Task)unassigned.get(k);
      if (isTabu(t))
        continue;

      for (int i = 0; i < rsize; ++i) {
        Route croute = (Route)this.currentShift.get(i);
        int cr_size = croute.taskSet.size();
        croute.check();
        double emptyRateO = 
          (croute.emptyDistance + croute.softTimeWindowPenalty) / croute.loadedDistance;
        for (int j = 0; j <= cr_size; ++j) {
          if ((j != cr_size) && (((Task)croute.taskSet.get(j)).freezed()))
            continue;
          croute.insert(k, j, unassigned);

          if (croute.check() > 0) {
            double emptyRateDecrease = 
              emptyRateO - 
              (croute.emptyDistance + croute.softTimeWindowPenalty) / croute.loadedDistance;
            if (Double.compare(emptyRateDecrease, BestEmptyRateDecrease) > 0) {
              if (firstImprove)
                return true;

              BestEmptyRateDecrease = emptyRateDecrease;
            }

            r.add(0, croute);
            slot.add(0, Integer.valueOf(j));
            ta.add(0, t);
          }
          croute.remove(j, unassigned);

          if (croute.lastRouteCheckError == j) break; if (croute.lastRouteCheckError == -1)
            break;
        }
      }
    }

    if (r.size() == 0)
      return false;

    addToTabu((Task)ta.get(0));
    ((Route)r.get(0)).insert(unassigned.indexOf(ta.get(0)), ((Integer)slot.get(0)).intValue(), unassigned);
    return true;
  }

  private double twoOpt(boolean descend, boolean firstImprove)
  {
    Vector routes = new Vector();
    routes.addAll(this.currentShift);
    if (this.currentShiftIndex > 0)
      routes.addAll((Collection)this.nw.routesOfShifts.get(this.currentShiftIndex - 1));

    int numberOfShifts = this.nw.getNumberOfShifts();
    if (this.currentShiftIndex < numberOfShifts - 1) {
      routes.addAll((Collection)this.nw.routesOfShifts.get(this.currentShiftIndex + 1));
    }

    double bestEdReduce = 0D;
    Vector r1v = new Vector();
    Vector r2v = new Vector();
    Vector slot1 = new Vector();
    Vector slot2 = new Vector();

    int numberOfRoutes = routes.size();
    for (int i = 0; i < numberOfRoutes; ++i) {
      Route r1 = (Route)routes.get(i);
      int taskSetSize1 = r1.taskSet.size();
      if (taskSetSize1 < 4) continue;
      r1.check();
      double ed1 = r1.emptyDistance + r1.softTimeWindowPenalty;

      for (int j = i + 1; j < numberOfRoutes; ++j) {
        Route r2 = (Route)routes.get(j);
        int taskSetSize2 = r2.taskSet.size();
        if (taskSetSize2 < 4) continue;
        r2.check();
        double ed2 = r2.emptyDistance + r2.softTimeWindowPenalty;

        for (int k = 2; k < taskSetSize1 - 2; ++k)
          if (!(((Task)r1.taskSet.get(k)).freezed())) { if (isTabu((Task)r1.taskSet.get(k)))
              continue;
            for (int l = 2; l < taskSetSize2 - 2; ++l)
              if (!(((Task)r2.taskSet.get(l)).freezed())) { if (isTabu((Task)r2.taskSet.get(l)))
                  continue;
                if (twoOpt(r1, k, r2, l, false)) {
                  double edReduce = ed1 + ed2 - 
                    r1.emptyDistance + r1.softTimeWindowPenalty + 
                    r2.emptyDistance + 
                    r2.softTimeWindowPenalty;
                  if (descend) {
                    if (Double.compare(edReduce, bestEdReduce) > 0) {
                      if (firstImprove) {
                        twoOpt(r1, k, r2, l, true);
                        return edReduce;
                      }
                      r1v.add(0, r1);
                      r2v.add(0, r2);
                      slot1.add(0, Integer.valueOf(k));
                      slot2.add(0, Integer.valueOf(l));
                      bestEdReduce = edReduce;
                    }

                  }
                  else if (Double.compare(edReduce, 0D) != 0) {
                    r1v.add(r1);
                    r2v.add(r2);
                    slot1.add(new Integer(k));
                    slot2.add(new Integer(l));
                  }
                }
              }
          }

      }

    }

    if (descend) {
      if (Double.compare(bestEdReduce, 0D) > 0)
        twoOpt((Route)r1v.get(0), ((Integer)slot1.get(0)).intValue(), (Route)r2v.get(0), ((Integer)slot2.get(0)).intValue(), true);

      return bestEdReduce;
    }
    if (r1v.size() == 0) return 0D;
    Random rg = new Random();
    int n = rg.nextInt(r1v.size());
    twoOpt((Route)r1v.get(n), ((Integer)slot1.get(n)).intValue(), (Route)r2v.get(n), ((Integer)slot2.get(n)).intValue(), true);
    return 1D;
  }

  public double crossExchange(boolean descend, boolean firstImprove)
  {
    Vector routes = new Vector();
    routes.addAll(this.currentShift);
    if (this.currentShiftIndex > 0)
      routes.addAll((Collection)this.nw.routesOfShifts.get(this.currentShiftIndex - 1));

    int numberOfShifts = this.nw.getNumberOfShifts();
    if (this.currentShiftIndex < numberOfShifts - 1) {
      routes.addAll((Collection)this.nw.routesOfShifts.get(this.currentShiftIndex + 1));
    }

    double bestEdReduce = 0D;
    Vector r1v = new Vector();
    Vector r2v = new Vector();
    Vector from1v = new Vector(); Vector length1v = new Vector();
    Vector from2v = new Vector(); Vector length2v = new Vector();

    int routesSize = routes.size();
    for (int i = 0; i < routesSize; ++i) {
      Route r1 = (Route)routes.get(i);
      int r1length = r1.taskSet.size();
      if (r1length < 3) continue;
      r1.check();
      double ed1 = r1.emptyDistance + r1.softTimeWindowPenalty;

      for (int j = i + 1; j < routesSize; ++j) {
        Route r2 = (Route)routes.get(j);
        int r2length = r2.taskSet.size();
        if (r2length < 3) continue;
        r2.check();
        double ed2 = r2.emptyDistance + r2.softTimeWindowPenalty;

        for (int from1 = 0; from1 + 2 <= r1length; ++from1)
        {
          for (int length1 = 2; (length1 < 4) && (from1 + length1 <= r1length); ++length1)
            for (int from2 = 0; from2 + 2 <= r2length; ++from2)
              for (int length2 = 2; (length2 < 4) && (from2 + length2 <= r2length); ++length2)
                if (cross(r1, from1, length1, r2, from2, length2, false)) {
                  double edReduce = ed1 + ed2 - 
                    r1.emptyDistance + r1.softTimeWindowPenalty + 
                    r2.emptyDistance + 
                    r2.softTimeWindowPenalty;
                  if (descend) {
                    if (Double.compare(edReduce, bestEdReduce) > 0) {
                      if (firstImprove) {
                        cross(r1, from1, length1, r2, from2, length2, true);
                        return edReduce;
                      }
                      r1v.add(0, r1);
                      r2v.add(0, r2);
                      from1v.add(0, Integer.valueOf(from1));
                      from2v.add(0, Integer.valueOf(from2));
                      length1v.add(0, Integer.valueOf(length1));
                      length2v.add(0, Integer.valueOf(length2));
                      bestEdReduce = edReduce;
                    }

                  }
                  else if (Double.compare(edReduce, 0D) != 0) {
                    r1v.add(r1);
                    r2v.add(r2);
                    from1v.add(Integer.valueOf(from1));
                    from2v.add(Integer.valueOf(from2));
                    length1v.add(Integer.valueOf(length1));
                    length2v.add(Integer.valueOf(length2));
                  }
                }



        }

      }

    }

    if (descend) {
      if (Double.compare(bestEdReduce, 0D) > 0)
      {
        cross((Route)r1v.get(0), ((Integer)from1v.get(0)).intValue(), ((Integer)length1v.get(0)).intValue(), (Route)r2v.get(0), ((Integer)from2v.get(0)).intValue(), ((Integer)length2v.get(0)).intValue(), true);
      }
      return bestEdReduce;
    }

    if (r1v.size() == 0) return 0D;
    Random rg = new Random();
    int n = rg.nextInt(r1v.size());
    cross((Route)r1v.get(n), ((Integer)from1v.get(n)).intValue(), ((Integer)length1v.get(n)).intValue(), (Route)r2v.get(n), ((Integer)from2v.get(n)).intValue(), ((Integer)length2v.get(n)).intValue(), true);
    return 1D;
  }

  private boolean shake(Vector<Task> mandatory, Vector<Task> mandatory2, Vector<Task> optional)
  {
    switch (this.shakeI)
    {
    case 0:
      if (move(false, false) > 0D) { return true;
      }

    case 1:
      if (swap(false, false) > 0D) { return true;
      }

    case 2:
      if (Double.compare(crossExchange(false, false), 0D) > 0) return true;

    }

    System.out.println("Invalid shakeI");

    return false;
  }
}