package uk.ac.nottingham.ningboport.planner.algorithms;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;
import uk.ac.nottingham.ningboport.planner.Network;
import uk.ac.nottingham.ningboport.planner.Route;
import uk.ac.nottingham.ningboport.planner.RoutingPlanner;
import uk.ac.nottingham.ningboport.planner.Task;
import uk.ac.nottingham.ningboport.planner.Task.sortingMethod;
import uk.ac.nottingham.ningboport.planner.Task.taskStatus;

public class Insertion
{
  private Network nw;
  private boolean mandatoryInsertBest;
  private boolean optionalInsertBest;
  int currentPeriodIndex;
  Vector<Route> currentPeriod;

  public Insertion(Network nw, boolean pickBestMan, boolean pickBestOpt)
  {
    this.nw = nw;
    this.mandatoryInsertBest = pickBestMan;
    this.optionalInsertBest = pickBestOpt;
  }

  private void initializeRoutes(Vector<Task> tasks)
  {
    Object[] t = tasks.toArray();

    Task.sortBy = Task.sortingMethod.DEADLINE;
    Arrays.sort(t);

    for (int i = 0; (((i < t.length) ? 1 : 0) & ((i < this.currentPeriod.size()) ? 1 : 0)) != 0; ++i) {
      Route r = (Route)this.currentPeriod.elementAt(i);
      if (!(r.taskSet.isEmpty()))
        continue;

      r.insert((Task)t[i], 0, tasks);

      int check = r.check();
      if (check != 1) {
        System.out.println("Failed to give initial node at route " + i + 
          " check code " + check);
        System.out.println("Commodity: " + ((Task)r.taskSet.get(0)).cmdt);
        r.remove(0, tasks);
        return;
      }
    }
  }

  public void run()
  {
    System.out.println("=== Insertion Started ===");

    int numberOfShifts = this.nw.getNumberOfShifts();
    for (int i = 0; i < numberOfShifts; ++i) {
      this.currentPeriodIndex = i;
      this.currentPeriod = ((Vector)this.nw.routesOfShifts.elementAt(i));

      Vector mandatory = this.nw.getPeriodTaskSet(i, true);

      initializeRoutes(mandatory);
      while (mandatory.size() != 0)
        if (!(insertActionsToRoutes(mandatory, this.mandatoryInsertBest)))
          break;


      System.out.println("mandatory remainning: " + mandatory.size());
      for (int s = 0; s < mandatory.size(); ++s) {
        System.out.println(mandatory.get(s));
      }

      this.nw.returnPeriodTasks(mandatory);

      for (int j = i + 1; (j < i + 8) && (j <= numberOfShifts); ++j)
      {
        Vector optional;
        if (j < numberOfShifts)
          optional = this.nw.getPeriodTaskSet(j, true);
        else {
          optional = this.nw.getPeriodTaskSet(j - 1, false);
        }

        initializeRoutes(optional);
        while (optional.size() != 0)
          if (!(insertActionsToRoutes(optional, this.optionalInsertBest)))
            break;


        System.out.println("optional remainning on period " + j + ": " + optional.size());

        this.nw.returnPeriodTasks(optional);
      }
    }

    this.nw.checkAllRoutes();
  }

  private boolean insertActionsToRoutes(Vector<Task> unnassigned, boolean only_best)
  {
    Vector ias = new Vector();
    for (int i = 0; i < unnassigned.size(); ++i)
      for (int j = 0; j < this.currentPeriod.size(); ++j) {
        InsertAction ia = findBestSlot((Task)unnassigned.elementAt(i), (Route)this.currentPeriod.elementAt(j));
        if (ia.slot != -1)
          if (only_best)
            if ((ias.size() == 0) || (Double.compare(ia.score, ((InsertAction)ias.lastElement()).score) < 0)) {
              ias.removeAllElements();
              ias.add(ia);
            }

          else
            ias.add(ia);


      }


    if (ias.size() == 0) {
      return false;
    }

    if (only_best) {
      InsertOneAction(ias, unnassigned);
    }
    else {
      Collections.sort(ias);

      while (ias.size() > 0)
        InsertOneAction(ias, unnassigned);
    }
    return true;
  }

  private void InsertOneAction(Vector<InsertAction> ias, Vector<Task> uta) {
    InsertAction ia = (InsertAction)ias.elementAt(0);
    ia.r.insert(ia.t, ia.slot, uta);

    for (int i = 0; i < ias.size(); ++i) {
      InsertAction ia2 = (InsertAction)ias.elementAt(i);
      if ((ia2.t.equals(ia.t)) || (ia2.r.equals(ia.r)))
        ias.remove(i--);
    }
  }

  private InsertAction findBestSlot(Task t, Route r)
  {
    if (r.check() != 1) {
      RoutingPlanner.errormsg("find slot: Bad route");
      System.exit(0);
    }

    InsertAction ia = new InsertAction(r, t);
    ia.score = 99999.0D;
    Vector ta = r.taskSet;
    int taSize = ta.size();

    double loadedDistanceO = r.loadedDistance;
    double emptyDistanceO = r.emptyDistance;
    double softTwPenalty = r.softTimeWindowPenalty;

    for (int i = taSize; i >= 0; --i) {
      if ((i != taSize) && (((Task)ta.get(i)).currentStatus == Task.taskStatus.FINISHED)) {
        continue;
      }

      ta.add(i, t);
      if (r.check() != 1) {
        ta.remove(i);
      }
      else
      {
        double score = (r.emptyDistance + r.softTimeWindowPenalty) / (r.emptyDistance + r.loadedDistance) - 
          (emptyDistanceO + softTwPenalty) / (emptyDistanceO + loadedDistanceO);

        if (score < ia.score) {
          ia.score = score;
          ia.slot = i;
        }

        ta.remove(i); }
    }
    return ia;
  }
}