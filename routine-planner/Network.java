package uk.ac.nottingham.ningboport.planner;

import java.io.PrintStream;
import java.util.Calendar;
import java.util.Vector;
import uk.ac.nottingham.ningboport.network.model.XMLSession;

public class Network
{
  public Vector<Node> nodes;
  public Node depot;
  public double[][] travelingTimes;
  public double[][] travelingTimesWithQueue;
  public double[][] travelingDistances;
  public Vector<Vector<Route>> routesOfShifts;
  public int loadedVehilceFuelCost = 0;
  public int emptyVehicleFuelCost = 0;
  public Calendar[] periodStartTimes;
  public Calendar[] periodEndTimes;
  public Vector<Vector<Commodity>> mandatoryCommodities;
  public Vector<Vector<Commodity>> optionalCommodities;
  public int periodLength = -1;
  public Vector<Task> taskSet;
  public Vector<Commodity> commodities;
  public Calendar alg_stime;

  public Network()
  {
    this.nodes = new Vector();
    this.taskSet = new Vector();
    this.commodities = new Vector();
    this.alg_stime = Calendar.getInstance();
  }

  public void createPeriodBoundaries(Calendar stime, int pLengthInHour, int nperiods)
  {
    this.mandatoryCommodities = new Vector(nperiods);
    this.optionalCommodities = new Vector(nperiods);
    this.periodLength = pLengthInHour;
    this.periodStartTimes = new Calendar[nperiods];
    this.periodEndTimes = new Calendar[nperiods];
    for (int i = 0; i < nperiods; ++i) {
      this.periodStartTimes[i] = ((Calendar)stime.clone());
      stime.add(11, pLengthInHour);
      this.periodEndTimes[i] = ((Calendar)stime.clone());
      this.mandatoryCommodities.add(new Vector());
      this.optionalCommodities.add(new Vector());
    }
  }

  public void createEmptyRoutes(int numberOfRoutes)
  {
    this.routesOfShifts = new Vector();
    for (int i = 0; i < this.periodStartTimes.length; ++i) {
      Vector r = new Vector();
      this.routesOfShifts.add(r);
      for (int j = 0; j < numberOfRoutes; ++j)
        r.add(new Route(this, j, i));
    }
  }

  public int getNumberOfShifts()
  {
    if (this.routesOfShifts == null)
      return 0;

    return this.routesOfShifts.size();
  }

  public Node getNodeById(String id)
  {
    for (int i = 0; i < this.nodes.size(); ++i)
      if (((Node)this.nodes.elementAt(i)).id.equals(id))
        return ((Node)this.nodes.elementAt(i));


    return null;
  }

  public int commodityIndex(String commodityID)
  {
    int csize = this.commodities.size();
    for (int i = 0; i < csize; ++i) {
      if (((Commodity)this.commodities.get(i)).id.equals(commodityID))
        return i;

    }

    return -1;
  }

  public Commodity getCommodityById(String commodityID) {
    int csize = this.commodities.size();
    for (int i = 0; i < csize; ++i) {
      if (((Commodity)this.commodities.get(i)).id.equals(commodityID))
        return ((Commodity)this.commodities.get(i));

    }

    return null;
  }

  public Vector<Task> getTasksForCommodity(Commodity c) {
    Vector allTasks = new Vector();
    int routesOfShiftsSize = this.routesOfShifts.size();
    for (int i = 0; i < routesOfShiftsSize; ++i) {
      Vector shiftI = (Vector)this.routesOfShifts.get(i);
      int shiftISize = shiftI.size();
      for (int j = 0; j < shiftISize; ++j)
        allTasks.addAll(((Route)shiftI.get(j)).taskSet);
    }

    allTasks.addAll(this.taskSet);

    Vector cTasks = new Vector();
    int allTaskSize = allTasks.size();
    for (int i = 0; i < allTaskSize; ++i) {
      Task t = (Task)allTasks.get(i);
      if (t.cmdt.id.equals(c.id)) {
        cTasks.add(t);
        if (t.gT != null)
          cTasks.add(t.gT);
      }
    }
    return cTasks;
  }

  public void classifyCommodities()
  {
    int commodityQuantity = this.commodities.size();
    for (int i = 0; i < commodityQuantity; ++i)
      classifyCommodity((Commodity)this.commodities.get(i));
  }

  public void classifyCommodity(Commodity c)
  {
    Vector cTasks = getTasksForCommodity(c);
    if (cTasks.isEmpty()) return;
    int numberOfShifts = getNumberOfShifts();

    for (int i = 0; i < numberOfShifts; ++i) {
      Route tmp = new Route(this, 1, i);
      tmp.taskSet.add((Task)cTasks.firstElement());
      int res = tmp.check();
      if (res > 0)
        c.completableShifts.add(Integer.valueOf(i));

    }

    if (c.completableShifts.size() > 0) {
      c.latestPeriod = ((Integer)c.completableShifts.lastElement()).intValue();
      c.completableShifts.remove(c.completableShifts.lastElement());

      ((Vector)this.mandatoryCommodities.get(c.latestPeriod)).add(c);
      for (i = 0; i < c.completableShifts.size(); ++i)
        ((Vector)this.optionalCommodities.get(((Integer)c.completableShifts.get(i)).intValue())).add(c);
    }
  }

  public double getCommodityTimeConsumption(Commodity c)
  {
    return (this.travelingTimes[c.src.index][c.dest.index] + c.src.loadTime + c.dest.unloadTime);
  }

  public Task getTaskByID(String ID, boolean takeOutTask) {
    for (int i = 0; i < this.taskSet.size(); ++i)
      if (((Task)this.taskSet.get(i)).cmdt.id.equals(ID)) {
        if (takeOutTask)
          return ((Task)this.taskSet.remove(i));

        return ((Task)this.taskSet.get(i));
      }

    return null;
  }

  public Vector<Task> getPeriodTaskSet(int periodIndex, boolean mandatory)
  {
    Vector commoditySet;
    Vector periodTaskSet = new Vector();

    if (mandatory) commoditySet = (Vector)this.mandatoryCommodities.elementAt(periodIndex);
    else commoditySet = (Vector)this.optionalCommodities.elementAt(periodIndex);

    for (int i = 0; i < commoditySet.size(); ++i) {
      Commodity c = (Commodity)commoditySet.elementAt(i);
      for (int j = 0; j < this.taskSet.size(); ++j) {
        Task t = (Task)this.taskSet.elementAt(j);
        if (t.cmdt.id.equals(c.id)) {
          periodTaskSet.add(t);
          this.taskSet.remove(j--);
        }
      }
    }
    return periodTaskSet;
  }

  public void returnPeriodTasks(Vector<Task> detachedTA) {
    this.taskSet.addAll(detachedTA);
    detachedTA.clear();
  }

  public void combineLightTask(Vector<Task> unassignedTa)
  {
    for (int i = 0; i < unassignedTa.size(); ++i) {
      Task ti = (Task)unassignedTa.elementAt(i);
      if (ti.size != 2) { if (ti.gT != null)
          continue;

        for (int j = i + 1; j < unassignedTa.size(); ++j) {
          Task tj = (Task)unassignedTa.elementAt(j);
          if (tj.size != 2) { if (tj.gT != null)
              continue;

            if (ti.cmdt.id.equals(tj.cmdt.id)) {
              ti.gT = tj;
              unassignedTa.remove(j);

              break; }
          }
        }
      }
    }
  }

  public void uncombineLightTask(Vector<Task> unassignedTA) {
    int tsize = unassignedTA.size();
    Vector TAtemp = new Vector();
    for (int i = 0; i < tsize; ++i) {
      Task t = (Task)unassignedTA.get(i);
      if (t.gT != null) {
        TAtemp.add(t.gT);
        t.gT = null;
      }
    }
    unassignedTA.addAll(TAtemp);
  }

  public void setFinishedTask(Calendar ctime)
  {
    if (!(checkAllRoutes())) {
      RoutingPlanner.errormsg("Error setting finished ta: not all routes currect!");
      System.exit(0);
    }

    int numberOfShifts = getNumberOfShifts();
    for (int i = 0; i < numberOfShifts; ++i) {
      int nroutes = ((Vector)this.routesOfShifts.get(i)).size();
      for (int j = 0; j < nroutes; ++j)
        ((Route)((Vector)this.routesOfShifts.get(i)).get(j)).setFinishedTA(ctime);
    }
  }

  public Vector<Task> removeUnfinishedTask(int periodIndex)
  {
    Vector removedTaskSet = new Vector();

    Vector currentPeriod = (Vector)this.routesOfShifts.elementAt(periodIndex);
    int rsize = currentPeriod.size();
    for (int i = 0; i < rsize; ++i) {
      Route currentRoute = (Route)currentPeriod.elementAt(i);
      for (int j = 0; j < currentRoute.taskSet.size(); ++j) {
        Task t = (Task)currentRoute.taskSet.get(j);
        Task gT = t.gT;
        if (!(t.freezed())) {
          currentRoute.taskSet.remove(j--);
          this.taskSet.add(t);
          if (gT != null) {
            t.gT = null;
            this.taskSet.add(gT);
          }
        }
      }
    }

    return removedTaskSet;
  }

  public String getCommodityInfo()
  {
    String s = new String();
    s = s + "id\tsource node\tdest node\tsmall\tlarge\n";
    for (int i = 0; i < this.commodities.size(); ++i)
      s = s + this.commodities.elementAt(i) + "\n";

    return s;
  }

  public String outputRoutes() {
    String output = "";
    for (int i = 0; i < this.routesOfShifts.size(); ++i) {
      output = output + "===Shift " + i + " (from " + this.periodStartTimes[i].getTime() + 
        ")" + " with " + ((Vector)this.routesOfShifts.get(i)).size() + " routes===\n";
      Vector currentPeriod = (Vector)this.routesOfShifts.elementAt(i);
      for (int j = 0; j < currentPeriod.size(); ++j)
        output = output + ((Route)currentPeriod.elementAt(j)).toString() + "\n";
    }

    return output;
  }

  public String getCommodityCount() {
    String output = "";
    if ((this.routesOfShifts == null) || (this.taskSet == null)) {
      return "No commodity data read.";
    }

    output = output + "Tasks in unrouted task set: ";
    int unroutedTaskCount = 0;
    for (int i = 0; i < this.taskSet.size(); ++i)
      if (((Task)this.taskSet.get(i)).gT == null)
        ++unroutedTaskCount;
      else
        unroutedTaskCount += 2;

    output = output + unroutedTaskCount + "\n";

    int routedTasksCount = 0; int numberOfShifts = getNumberOfShifts();
    for (int p = 0; p < numberOfShifts; ++p)
      for (int r = 0; r < ((Vector)this.routesOfShifts.get(p)).size(); ++r) {
        Vector ts = ((Route)((Vector)this.routesOfShifts.get(p)).get(r)).taskSet;
        for (int t = 0; t < ts.size(); ++t)
          if (((Task)ts.get(t)).gT == null)
            ++routedTasksCount;
          else
            routedTasksCount += 2;

      }


    output = output + "Tasks routed: ";
    output = output + routedTasksCount + "\n";
    return output;
  }

  public boolean assignSession(Route r, XMLSession s)
  {
    return false;
  }

  public boolean startSession(Route r) {
    if (r.getSession() == null) {
      return false;
    }

    r.setStatus(Route.status.ASSIGNED);
    return true;
  }

  public boolean suspendSession(Route r) {
    if (r.getSession() == null) {
      return false;
    }

    r.setStatus(Route.status.SESSION_SUSPENDED);
    return true;
  }

  public boolean resumeSession(Route r) {
    if (r.getSession() == null) {
      return false;
    }

    r.setStatus(Route.status.ASSIGNED);
    return true;
  }

  public boolean endSession(Route r) {
    if (r.getSession() == null) {
      return false;
    }

    r.setStatus(Route.status.SESSION_ENDED);
    return true;
  }

  public int getTotalRoutedTaskCount()
  {
    int count = 0;
    int nShifts = this.routesOfShifts.size();
    for (int i = 0; i < nShifts; ++i) {
      count += getShiftRoutedTaskCount(i);
    }

    return count;
  }

  public int getShiftRoutedTaskCount(int shiftI) {
    int count = 0;
    Vector routesInShiftI = (Vector)this.routesOfShifts.get(shiftI);
    int shiftISize = routesInShiftI.size();
    for (int j = 0; j < shiftISize; ++j)
      count += ((Route)routesInShiftI.get(j)).taskSet.size();

    return count;
  }

  public String getSolutionObjectiveValues()
  {
    String res = "=============== Objective Values ===============\n";
    int psize = this.routesOfShifts.size();
    double total_e = 0D; double total_l = 0D;
    if (!(checkAllRoutes())) {
      System.out.println("Route infeasibility occured!");
    }

    res = res + "Shift\tLD\tED\tLDR\tCount\n";
    for (int i = 0; i < psize; ++i) {
      res = res + i + "\t";
      double e = 0D; double l = 0D;

      Vector shift = (Vector)this.routesOfShifts.elementAt(i);
      int rsize = shift.size();

      for (int j = 0; j < rsize; ++j) {
        e += ((Route)shift.elementAt(j)).emptyDistance;
        l += ((Route)shift.elementAt(j)).loadedDistance;
        total_e += ((Route)shift.elementAt(j)).emptyDistance;
        total_l += ((Route)shift.elementAt(j)).loadedDistance;
      }

      res = res + l + "\t" + e + "\t";
      res = res + (l / (e + l)) + "\t" + getShiftRoutedTaskCount(i) + "\n";
    }

    res = res + "Avg\t" + total_l + "\t" + total_e + 
      "\t" + (total_l / (total_l + total_e)) + 
      "\t" + getTotalRoutedTaskCount() + "\n";
    return res;
  }

  public String getShiftObjectiveValues(int shiftIndex) {
    String out = "";
    Vector shift = (Vector)this.routesOfShifts.get(shiftIndex);
    double e = 0D; double l = 0D;
    int rsize = shift.size();

    for (int j = 0; j < rsize; ++j) {
      e += ((Route)shift.elementAt(j)).emptyDistance;
      l += ((Route)shift.elementAt(j)).loadedDistance;
    }
    out = out + l + "\t" + e + "\t" + (l / (e + l));
    return out;
  }

  public boolean checkAllRoutes() {
    int psize = this.routesOfShifts.size();
    for (int i = 0; i < psize; ++i) {
      Vector r = (Vector)this.routesOfShifts.elementAt(i);
      int rsize = r.size();
      for (int j = 0; j < rsize; ++j) {
        int checkResult = ((Route)r.elementAt(j)).check();
        if (checkResult < 1)
          return false;
      }
    }

    return true;
  }

  public void updateObjectiveValues(Route r)
  {
    Vector taskSet = r.taskSet;
    r.emptyDistance = (r.loadedDistance = 0D);

    for (int i = 0; i < taskSet.size(); ++i) {
      Task taskI = (Task)r.taskSet.elementAt(i);
      Commodity c = taskI.cmdt;
      int srcI = c.src.index;
      int destI = c.dest.index;
      int depotI = this.depot.index;

      Commodity g = null;
      int gSrcI = 0; int gDestI = 0;
      if (taskI.gT != null) {
        g = taskI.gT.cmdt;
        gSrcI = g.src.index;
        gDestI = g.dest.index;
        if ((taskI.size == 2) || (taskI.size == 2)) {
          RoutingPlanner.errormsg("Checking Route: Error, overweight when grabbing additional ta.");
          System.exit(0);
        }

      }

      if (i == 0) {
        r.emptyDistance += this.travelingDistances[depotI][srcI];
      }
      else {
        Commodity c2 = ((Task)r.taskSet.elementAt(i - 1)).cmdt;
        if (!(c.src.equals(c2.dest))) {
          r.emptyDistance += this.travelingDistances[c2.dest.index][srcI];
        }

      }

      if (g == null)
      {
        r.loadedDistance += this.travelingDistances[srcI][destI];
      }
      else {
        r.loadedDistance += this.travelingDistances[srcI][gSrcI];

        r.loadedDistance += this.travelingDistances[gSrcI][gDestI];

        r.loadedDistance += this.travelingDistances[gDestI][destI];
      }

      if (i == r.taskSet.size() - 1)
        r.emptyDistance += this.travelingDistances[destI][depotI];
    }
  }
}