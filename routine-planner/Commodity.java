package uk.ac.nottingham.ningboport.planner;

import java.util.Calendar;
import java.util.Vector;

public class Commodity
  implements Identical<Commodity>
{
  public String id;
  public Calendar availTime;
  public Calendar deadline;
  public int small;
  public int large;
  public Node src;
  public Node dest;
  public Vector<Integer> completableShifts = new Vector();
  public int latestPeriod;

  public Commodity(String id, Calendar avail, Calendar deadline, Node src, Node dest, int small, int large)
  {
    this.id = id;
    this.availTime = avail;
    this.deadline = deadline;
    this.small = small;
    this.large = large;
    this.src = src;
    this.dest = dest;
  }

  public String toString() {
    return this.id + " " + 
      this.src + "(" + this.availTime.getTime() + ") " + 
      this.dest + "(" + this.deadline.getTime() + ") " + 
      this.small + " " + this.large;
  }

  public boolean geographicallyIdenticalTo(Commodity t)
  {
    return ((this.src == t.src) && (this.dest == t.dest));
  }
}