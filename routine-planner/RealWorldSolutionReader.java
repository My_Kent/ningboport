package uk.ac.nottingham.ningboport.planner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Collections;
import java.util.Vector;

public class RealWorldSolutionReader
{
  private Network nw;
  private Vector<String[]> solutionFile = new Vector();

  public RealWorldSolutionReader(Network nw)
  {
    this.nw = nw;
  }

  public boolean readSolution(String path) throws IOException {
    String lineStr;
    this.solutionFile.clear();

    BufferedReader br = new BufferedReader(new FileReader(path));

    while ((lineStr = br.readLine()) != null) {
      String[] line = lineStr.split("\t");
      if (line.length == 13)
        this.solutionFile.add(line);
      else
        System.out.println("Error in line: " + lineStr);
    }
    this.solutionFile.remove(0);
    parseCommodities();

    System.out.println(this.nw.commodities.size() + " commodity parsed from existing solution.");
    br.close();
    useParsedRoutesAsTaskSet(parseVehicleRoutes());

    for (int i = 0; i < this.nw.routesOfShifts.size(); ++i) {
      Vector rs = (Vector)this.nw.routesOfShifts.get(i);
      for (int j = 0; j < rs.size(); ++j)
        ((Route)rs.get(j)).taskSet.clear();

    }

    this.nw.classifyCommodities();

    return true;
  }

  private void parseCommodities() {
    int flen = this.solutionFile.size();
    for (int i = 0; i < flen; ++i) {
      String[] line = (String[])this.solutionFile.get(i);
      if (line.length != 13)
        continue;

      Task t = new Task();
      Commodity c = this.nw.getCommodityById(line[8]);

      Node src = this.nw.getNodeById(line[2]);
      Node dest = this.nw.getNodeById(line[3]);

      t.actualStartT = RoutingPlanner.readCalString(line[6]);

      if ((src != null) && (dest != null)) { if (t.actualStartT == null) {
          continue;
        }

        if (c == null) {
          Calendar avail = RoutingPlanner.readCalString(line[9]);
          if (avail == null) {
            avail = (Calendar)this.nw.periodStartTimes[0].clone();
          }

          Calendar dead = RoutingPlanner.readCalString(line[11]);

          if (dead == null)
            dead = RoutingPlanner.readCalString(line[10]);

          if (dead == null)
            dead = (Calendar)this.nw.periodEndTimes[(this.nw.getNumberOfShifts() - 1)].clone();

          c = new Commodity(line[8], avail, dead, 
            src, dest, 0, 0);
          this.nw.commodities.add(c);
        }
        t.cmdt = c;

        t.actualFinishT = RoutingPlanner.readCalString(line[7]);
        if (t.actualFinishT != null)
        {
          if (t.actualFinishT.after(t.cmdt.deadline)) {
            t.cmdt.deadline = ((Calendar)t.actualFinishT.clone());
            t.cmdt.deadline.add(10, 12);
          }
        }
        else {
          t.actualFinishT = ((Calendar)t.actualStartT.clone());
          t.actualFinishT.add(12, (int)this.nw.getCommodityTimeConsumption(c));
        }

        if (Integer.parseInt(line[4]) == 40) {
          t.size = 2;
          c.large += 1;
        } else {
          t.size = 1;
          c.small += 1;
        }

        if (Integer.parseInt(line[5]) == 2) {
          Task t2 = t.clone();
          t.gT = t2;

          c.small += 1;
        }

        t.vehicleID = line[0];
        t.driver = line[1];

        this.nw.taskSet.add(t); }
    }
  }

  private Vector<Route> parseVehicleRoutes() {
    int numberOfShifts = this.nw.getNumberOfShifts();

    Vector nwTaskSet = (Vector)this.nw.taskSet.clone();
    int routeID = 0;
    Vector routesParsed = new Vector();
    while (!(nwTaskSet.isEmpty())) {
      ++routeID;
      String currentVehicleID = ((Task)nwTaskSet.get(0)).vehicleID;
      Vector vehicleTaskSet = new Vector();

      for (int i = 0; i < nwTaskSet.size(); ++i) {
        if (((Task)nwTaskSet.get(i)).vehicleID.equals(currentVehicleID)) {
          vehicleTaskSet.add((Task)nwTaskSet.remove(i--));
        }

      }

      if (vehicleTaskSet.size() == 0) {
        continue;
      }

      Task.sortBy = Task.sortingMethod.ACTUAL_STARTING_TIME;
      Collections.sort(vehicleTaskSet);

      int periodNumber = 0;

      Calendar shiftStart = this.nw.periodStartTimes[periodNumber];
      Calendar shiftEnd = this.nw.periodEndTimes[periodNumber];
      Route r = new Route(this.nw, routeID, periodNumber);
      routesParsed.add(r);

      while (!(vehicleTaskSet.isEmpty())) {
        Task currentTask = (Task)vehicleTaskSet.get(0);
        if (currentTask.actualFinishT.before(shiftStart)) {
          vehicleTaskSet.remove(0);
        }
        else {
          if (currentTask.actualFinishT.after(shiftEnd)) {
            ++periodNumber;
            if (periodNumber >= numberOfShifts)
              break;

            shiftStart = this.nw.periodStartTimes[periodNumber];
            shiftEnd = this.nw.periodEndTimes[periodNumber];

            r = new Route(this.nw, routeID, periodNumber);
            routesParsed.add(r);
          }

          r.taskSet.add((Task)vehicleTaskSet.remove(0));
        }
      }
    }

    for (int i = 0; i < numberOfShifts; ++i) {
      Vector rs = (Vector)this.nw.routesOfShifts.get(i);
      rs.clear();
      for (int j = 0; j < routesParsed.size(); ++j)
        if (((Route)routesParsed.get(j)).period == i)
          rs.add((Route)routesParsed.get(j));


    }

    System.out.println("Got " + routesParsed.size() + " routes from the real world file.");
    return routesParsed;
  }

  public void useParsedRoutesAsTaskSet(Vector<Route> routesParsed) {
    this.nw.taskSet.clear();
    for (int i = 0; i < routesParsed.size(); ++i)
      this.nw.taskSet.addAll(((Route)routesParsed.get(i)).taskSet);

    System.out.println("Number of tasks: " + this.nw.taskSet.size());
  }
}