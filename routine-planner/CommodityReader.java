package uk.ac.nottingham.ningboport.planner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Vector;

public class CommodityReader
{
  private static Vector<String> commodityFile = new Vector();

  public static void readFile(String path, Network nw)
    throws IOException
  {
    String lineStr;
    BufferedReader br = new BufferedReader(new FileReader(path));

    while ((lineStr = br.readLine()) != null)
      commodityFile.add(lineStr);

    br.close();
    parseCommodity(nw);
  }

  private static void parseCommodity(Network nw)
  {
    Calendar firstPeriodStartTime = nw.periodStartTimes[0];
    Calendar lastPeriodsEndTime = nw.periodEndTimes[(nw.periodEndTimes.length - 1)];
    commodityFile.remove(0);
    for (int i = 0; i < commodityFile.size(); ++i) {
      String[] ts = ((String)commodityFile.elementAt(i)).split("\t");
      if (ts.length != 7) {
        RoutingPlanner.errormsg("Wrong commodity file!");
        System.exit(0);
      }

      Node from = Node.findByID(ts[1], nw);
      int fromI = nw.nodes.indexOf(from);
      Node to = Node.findByID(ts[2], nw);
      int toI = nw.nodes.indexOf(to);
      double fuel = nw.travelingDistances[fromI][toI] / 100.0D;

      Calendar commodityAvailTime = RoutingPlanner.readCalString(ts[3]);

      if (commodityAvailTime.after(lastPeriodsEndTime))
        continue;

      Calendar commodityDeadline = RoutingPlanner.readCalString(ts[4]);

      if (commodityDeadline.before(firstPeriodStartTime))
        continue;

      Commodity c = new Commodity(ts[0], 
        commodityAvailTime, commodityDeadline, 
        from, to, Integer.parseInt(ts[5]), Integer.parseInt(ts[6]));
      nw.commodities.add(c);
      int total = c.small + c.large;
      for (int j = 0; j < total; ++j) {
        Task t = new Task();
        if (j >= c.small) t.size = 2;
        else t.size = 1;
        t.fuel = fuel;
        t.cmdt = c;

        nw.taskSet.add(t);
      }

    }

    nw.classifyCommodities();
  }

  public static void readDatabase(Network nw, String name, String password) throws IOException {
    try {
      Class.forName("com.mysql.jdbc.Driver");
      Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/nbp", name, password);
      Statement s = connect.createStatement();
      ResultSet r = s.executeQuery("SELECT * FROM Commodity;");

      Calendar firstPeriodStartTime = nw.periodStartTimes[0];
      Calendar lastPeriodsEndTime = nw.periodEndTimes[(nw.periodEndTimes.length - 1)];

      while (r.next())
      {
        Node from = Node.findByID(r.getString("source"), nw);
        int fromI = nw.nodes.indexOf(from);
        Node to = Node.findByID(r.getString("destination"), nw);
        int toI = nw.nodes.indexOf(to);
        double fuel = nw.travelingDistances[fromI][toI] / 100.0D;

        Calendar commodityAvailTime = Calendar.getInstance();
        commodityAvailTime.setTime(r.getDate("available_t", commodityAvailTime));

        if (commodityAvailTime.after(lastPeriodsEndTime))
          continue;

        Calendar commodityDeadline = Calendar.getInstance();
        commodityDeadline.setTime(r.getDate("deadline", commodityDeadline));

        if (commodityDeadline.before(firstPeriodStartTime))
          continue;

        Commodity c = new Commodity(r.getString("id"), 
          commodityAvailTime, commodityDeadline, 
          from, to, r.getInt("small") - r.getInt("finished_small"), 
          r.getInt("large") - r.getInt("finished_large"));

        nw.commodities.add(c);
        int total = c.small + c.large;
        for (int j = 0; j < total; ++j) {
          Task t = new Task();
          if (j >= c.small) t.size = 2;
          else t.size = 1;
          t.fuel = fuel;
          t.cmdt = c;

          nw.taskSet.add(t);
        }
      }
      nw.classifyCommodities();

      connect.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
    }
  }
}