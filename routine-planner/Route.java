package uk.ac.nottingham.ningboport.planner;

import java.io.PrintStream;
import java.util.Calendar;
import java.util.Vector;
import uk.ac.nottingham.ningboport.network.model.XMLSession;

public class Route
  implements Identical<Route>
{
  public int id;
  private XMLSession session;
  public int period;
  public Vector<Task> taskSet;
  public double loadedDistance;
  public double emptyDistance;
  public double softTimeWindowPenalty;
  public double waitTime;
  public status currentStatus = status.UNASSIGNED;
  private Network nw;
  public int lastRouteCheckError = -2;

  public Route(Network nw, int id, int period)
  {
    this.taskSet = new Vector();
    this.nw = nw;
    this.id = id;
    this.period = period;
  }

  public void insert(int ta_index, int route_slot, Vector<Task> unassignedTaskSet) {
    route_slot = (route_slot > this.taskSet.size()) ? this.taskSet.size() : route_slot;
    this.taskSet.add(route_slot, (Task)unassignedTaskSet.remove(ta_index));
  }

  public void insert(Task t, int route_slot, Vector<Task> unassignedTaskSet) {
    insert(unassignedTaskSet.indexOf(t), route_slot, unassignedTaskSet);
  }

  public void remove(int ta_index, Vector<Task> unassignedTaskSet) {
    unassignedTaskSet.add((Task)this.taskSet.remove(ta_index));
  }

  public boolean setSession(XMLSession s)
  {
    if (this.session != null) {
      return false;
    }

    this.session = s;
    this.currentStatus = status.ASSIGNED;
    return true;
  }

  public XMLSession getSession() {
    return this.session;
  }

  public status getStatus() {
    return this.currentStatus;
  }

  public void setStatus(status s) {
    this.currentStatus = s;
  }

  public String outputPortSequence() {
    String out = "";
    for (int i = 0; i < this.taskSet.size(); ++i) {
      Task t = (Task)this.taskSet.elementAt(i);

      out = out + "[" + t.cmdt.src.id + " " + t.cmdt.dest.id + "] ";
    }

    return out;
  }

  public int getTightness()
  {
    return 0;
  }

  public String toString() {
    String statistics = new String();
    String ports = "Ports: ";
    String tasks = "Tasks: ";
    String times = "Times: ";
    String timesActual = "Times(actual): ";
    if (check() <= 0) {
      statistics = statistics + "*";
      this.nw.updateObjectiveValues(this);
    }

    for (int i = 0; i < this.taskSet.size(); ++i) {
      Task t = (Task)this.taskSet.elementAt(i);

      if (i == 0) {
        statistics = statistics + "Route: " + this.id + " driver: " + t.driver + " vehicle: " + t.vehicleID;
        statistics = statistics + " ED: " + this.emptyDistance + " LD: " + this.loadedDistance;
      }

      ports = ports + "[" + t.cmdt.src.id + " " + t.cmdt.dest.id + "] ";

      tasks = tasks + t.cmdt.id + "(";
      if (t.size == 1)
        tasks = tasks + "s";
      else
        tasks = tasks + "b";

      if (t.gT != null)
        tasks = tasks + "s";

      if (t.freezed())
        tasks = tasks + "f";

      tasks = tasks + ") ";

      times = times + "<";
      if ((t.startT != null) && (t.finishT != null))
        times = times + t.startT.getTime() + " " + t.finishT.getTime();
      else
        times = times + "Infeasible";

      times = times + " " + t.getLateness() + " ";
      times = times + "> ";

      timesActual = timesActual + "<";
      if ((t.actualStartT != null) && (t.actualFinishT != null))
        timesActual = timesActual + t.actualStartT.getTime() + " " + t.actualFinishT.getTime();

      timesActual = timesActual + "> ";
    }
    return statistics + "\n" + ports + "\n" + tasks + "\n" + times + "\n" + timesActual + "\n";
  }

  public void setFinishedTA(Calendar ctime)
  {
    if (this.nw.periodStartTimes[this.period].after(ctime)) {
      return;
    }

    int size = this.taskSet.size();
    for (int i = 0; i < size; ++i) {
      Calendar startTime;
      Task t = (Task)this.taskSet.get(i);

      if (i == 0)
        startTime = this.nw.periodStartTimes[this.period];
      else
        startTime = ((Task)this.taskSet.get(i - 1)).startT;

      if (ctime.after(startTime))
        t.currentStatus = Task.taskStatus.FINISHED;

      if (ctime.before(t.finishT))
        return;
    }
  }

  public boolean setRouteDelay(int delayInMinutes)
  {
    int size = this.taskSet.size();

    if (!(((Task)this.taskSet.get(0)).freezed())) {
      System.out.println("Delay not set, incorrect delay.");
      return false;
    }
    int lastIndex = 0;
    for (int i = 0; i < size; ++i) {
      if (!(((Task)this.taskSet.get(i)).freezed())) break;
      lastIndex = i;
    }

    ((Task)this.taskSet.get(lastIndex)).setLateness(delayInMinutes);

    return true;
  }

  public Route clone() {
    Route r = new Route(this.nw, this.id, this.period);
    r.session = this.session;
    r.emptyDistance = this.emptyDistance;
    r.loadedDistance = this.loadedDistance;
    r.softTimeWindowPenalty = this.softTimeWindowPenalty;
    r.waitTime = this.waitTime;
    r.currentStatus = this.currentStatus;

    int taskSetSize = this.taskSet.size();
    for (int i = 0; i < taskSetSize; ++i)
      r.taskSet.add(((Task)this.taskSet.get(i)).clone());

    return r;
  }

  public boolean geographicallyIdenticalTo(Route t)
  {
    if (this.taskSet.size() != t.taskSet.size())
      return false;

    int tsize = this.taskSet.size();
    Vector t2 = t.taskSet;
    for (int i = 0; i < tsize; ++i) {
      Commodity c1 = ((Task)this.taskSet.get(i)).cmdt;
      Commodity c2 = ((Task)t2.get(i)).cmdt;
      if (!(c1.geographicallyIdenticalTo(c2)))
        return false;
    }
    return true;
  }

  public int check()
  {
    Calendar currentTime = (Calendar)this.nw.periodStartTimes[this.period].clone();
    Calendar endTime = this.nw.periodEndTimes[this.period];
    this.loadedDistance = (this.emptyDistance = this.waitTime = this.softTimeWindowPenalty = 0D);

    int taskSetSize = this.taskSet.size();
    for (int i = 0; i < taskSetSize; ++i) {
      Task currentTa = (Task)this.taskSet.elementAt(i);
      currentTa.emergent = false;
      Commodity c = currentTa.cmdt;
      int srcI = c.src.index;
      int destI = c.dest.index;
      int depotI = this.nw.depot.index;

      if (i == 0) {
        currentTime.add(12, (int)this.nw.travelingTimes[depotI][srcI]);
        this.emptyDistance += this.nw.travelingDistances[depotI][srcI];
      }
      else {
        Commodity c2 = ((Task)this.taskSet.elementAt(i - 1)).cmdt;
        if (!(c.src.equals(c2.dest))) {
          currentTime.add(12, (int)this.nw.travelingTimes[c2.dest.index][srcI]);
          this.emptyDistance += this.nw.travelingDistances[c2.dest.index][srcI];
        }

      }

      if (currentTime.compareTo(c.availTime) < 0) {
        this.waitTime += (c.availTime.getTimeInMillis() - currentTime.getTimeInMillis()) / 
          1000L / 60L;
        currentTime = (Calendar)c.availTime.clone();
      }
      currentTa.startT = ((Calendar)currentTime.clone());

      currentTa.emergent = 
        ((int)((c.deadline.getTimeInMillis() - currentTime.getTimeInMillis()) / 1000L / 60L - 120L) < 
        this.nw.travelingTimesWithQueue[srcI][destI]);

      currentTime.add(12, c.src.loadTime);

      currentTime.add(12, (int)this.nw.travelingTimes[srcI][destI]);
      this.loadedDistance += this.nw.travelingDistances[srcI][destI];

      currentTime.add(12, c.dest.unloadTime);

      if (currentTime.compareTo(c.deadline) > 0)
      {
        this.lastRouteCheckError = i;
        return -1;
      }
      currentTa.finishT = ((Calendar)currentTime.clone());

      if (i == taskSetSize - 1) {
        currentTime.add(12, (int)this.nw.travelingTimes[destI][depotI]);
        this.emptyDistance += this.nw.travelingDistances[destI][depotI];
      }

      if (currentTime.compareTo(endTime) > 0) {
        if (i == taskSetSize - 1) {
          if (!(currentTa.emergent)) break label570;
          return 3;
        }

        this.lastRouteCheckError = -1;
        return 0;
      }

      label570: currentTime.add(12, currentTa.getLateness());
    }
    this.lastRouteCheckError = -2;
    return 1;
  }

  public static enum status
  {
    UNASSIGNED, ASSIGNED, SESSION_SUSPENDED, SESSION_ENDED;

    private int value;

    public int getValue()
    {
      return this.value;
    }

    public static status convertFromInt(int i) {
      if (i == 0)
        return UNASSIGNED;
      if (i == 1)
        return ASSIGNED;
      if (i == 2)
        return SESSION_SUSPENDED;

      return SESSION_ENDED;
    }
  }
}