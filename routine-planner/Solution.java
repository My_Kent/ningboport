package uk.ac.nottingham.ningboport.planner;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import uk.ac.nottingham.ningboport.network.model.XMLSession;

public class Solution
{
  public int numberOfTasksCompleted = 0;
  public double emptyDistance = 0D;
  public double loadedDistance = 0D;
  public Vector<Vector<Route>> routesOfShifts;
  public Vector<Task> taskSet;
  public Network nw;

  public Solution(Network nw, Vector<Task> unassignedTask)
  {
    this.nw = nw;
    this.taskSet = new Vector();
    if (unassignedTask != null) {
      unassignedTask.addAll(nw.taskSet);
      int nwTaskSetSize = unassignedTask.size();
      for (int i = 0; i < nwTaskSetSize; ++i) {
        this.taskSet.add(((Task)unassignedTask.get(i)).clone());
      }

    }

    Vector routesOfShifts = nw.routesOfShifts;
    int numberOfShifts = routesOfShifts.size();
    this.routesOfShifts = new Vector(numberOfShifts);
    for (int i = 0; i < numberOfShifts; ++i)
    {
      Vector solutionRoutesOfPeriodI = new Vector();
      this.routesOfShifts.add(solutionRoutesOfPeriodI);

      Vector networkRoutesInShiftI = (Vector)routesOfShifts.elementAt(i);
      int numberOfRoutes = networkRoutesInShiftI.size();
      for (int j = 0; j < numberOfRoutes; ++j) {
        Route current = (Route)networkRoutesInShiftI.elementAt(j);
        current.check();
        this.emptyDistance += current.emptyDistance;
        this.loadedDistance += current.loadedDistance;
        this.numberOfTasksCompleted += 1;
        solutionRoutesOfPeriodI.add(current.clone());
      }
    }
  }

  public void restore() {
    this.nw.routesOfShifts = this.routesOfShifts;
    this.nw.taskSet = this.taskSet;
  }

  public int compareTo(Solution b) {
    return Double.compare(b.emptyDistance, this.emptyDistance);
  }

  public boolean betterThan(Solution b) {
    if (this.numberOfTasksCompleted > b.numberOfTasksCompleted) return true;
    if (this.numberOfTasksCompleted < b.numberOfTasksCompleted) return false;

    double empty_rate_a = this.emptyDistance / (this.loadedDistance + this.emptyDistance);
    double empty_rate_b = b.emptyDistance / (b.loadedDistance + b.emptyDistance);

    return (Double.compare(empty_rate_a, empty_rate_b) < 0);
  }

  public void readSolutionFromDatabase(String name, String password)
  {
    Vector routesOfShifts = new Vector();
    Vector shiftStartTimes = new Vector();
    try
    {
      Class.forName("com.mysql.jdbc.Driver");
      Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/nbp", name, password);
      Statement s = connect.createStatement();
      ResultSet rs = s.executeQuery("SELECT Task.id as task_id, commodity_id, sequence_num, small, large, planned_start, planned_finish, actual_start, actual_finish, session_driverName, session_vehicleID, startTime as session_start, expireTime as session_expire, shift_normal_start_time, route_num, Route.id as route_id, Route.status as route_status, Task.status as task_status FROM Task inner join Route on Task.route_id=Route.id left join Session on Route.sessionID=Session.sessionID order by shift_normal_start_time, route_num, sequence_num;");

      while (rs.next())
      {
        Timestamp ts = rs.getTimestamp("shift_normal_start_time");
        Vector currentShift = null;
        if ((shiftStartTimes.size() != 0) && (((Timestamp)shiftStartTimes.lastElement()).equals(ts))) {
          currentShift = (Vector)routesOfShifts.lastElement();
        } else {
          currentShift = new Vector();
          routesOfShifts.add(currentShift);
          shiftStartTimes.add(ts);
        }

        XMLSession session = null;
        String drivername = rs.getString("session_driverName");
        if (drivername != null) {
          session = new XMLSession(drivername, rs.getString("session_vehicleID"), Long.parseLong(rs.getString("session_start")), Long.parseLong(rs.getString("expireTime")));
        }

        int route_num = rs.getInt("route_num");

        Route currentRoute = null;
        while (currentShift.size() <= route_num) {
          currentShift.add(new Route(this.nw, currentShift.size(), routesOfShifts.size() - 1));
        }

        currentRoute = (Route)currentShift.lastElement();
        currentRoute.setSession(session);

        int route_id = rs.getInt("route_id");
        currentRoute.id = route_id;

        currentRoute.currentStatus = Route.status.convertFromInt(rs.getInt("route_status"));

        Task t = new Task();
        int task_status = rs.getInt("task_status");
        if (Task.taskStatus.PLANNED == Task.taskStatus.convertFromInt(task_status)) {
          continue;
        }

        currentRoute.taskSet.add(t);
        t.currentStatus = Task.taskStatus.convertFromInt(task_status);

        int task_id = rs.getInt("task_id");
        t.taskID = task_id;

        Commodity c = this.nw.getCommodityById(rs.getString("commodity_id"));
        if (c == null) {
          RoutingPlanner.errormsg("Commodity " + rs.getString("commodity_id") + " not found! skipping this task!");
        }
        else {
          t.cmdt = c;

          Timestamp ps = rs.getTimestamp("planned_start");
          Timestamp pf = rs.getTimestamp("planned_finish");
          Timestamp as = rs.getTimestamp("actual_start");
          Timestamp af = rs.getTimestamp("actual_finish");
          t.startT = Calendar.getInstance();
          t.startT.setTimeInMillis(ps.getTime());
          t.finishT = Calendar.getInstance();
          t.finishT.setTimeInMillis(pf.getTime());
          if (as != null) {
            t.actualStartT = Calendar.getInstance();
            t.actualStartT.setTimeInMillis(as.getTime());
          }
          if (af != null) {
            t.actualFinishT = Calendar.getInstance();
            t.actualFinishT.setTimeInMillis(af.getTime());
          }

          int small = rs.getInt("small"); int large = rs.getInt("large");
          if (large == 1) {
            t.size = 2;
          } else {
            t.size = 1;
            if (small == 2)
              t.gT = t.clone();
          }

          System.out.println("Task: " + t + "added");
        }

      }

      if (routesOfShifts.size() == 0) return;
      this.nw.routesOfShifts = routesOfShifts;
    }
    catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void writeSolutionToDatabase(String name, String password)
  {
    try
    {
      Class.forName("com.mysql.jdbc.Driver");
      Connection connect = DriverManager.getConnection("jdbc:mysql://localhost/nbp?&useUnicode=true&characterEncoding=UTF-8", name, password);
      Statement s = connect.createStatement(1005, 1008);

      s.executeUpdate("DELETE FROM Gps;");
      s.executeUpdate("DELETE FROM Task;");
      s.executeUpdate("DELETE FROM Route;");
      s.executeUpdate("DELETE FROM Session;");

      int numberOfShifts = this.routesOfShifts.size();
      for (int i = 0; i < numberOfShifts; ++i) {
        Vector routesInShiftI = (Vector)this.routesOfShifts.get(i);
        int numberOfRoutesInI = routesInShiftI.size();
        Timestamp shiftStartTime = new Timestamp(this.nw.periodStartTimes[i].getTimeInMillis());
        for (int j = 0; j < numberOfRoutesInI; ++j) {
          ((Route)routesInShiftI.get(j)).check();
          ResultSet rss = s.executeQuery("SELECT * FROM Session;");
          XMLSession session = ((Route)routesInShiftI.get(j)).getSession();
          int sessionID = -1;
          if (session != null) {
            rss.moveToInsertRow();
            rss.updateString("session_driverName", session.getDriverName());
            rss.updateString("session_vehicleID", session.getVehicleID());
            rss.updateString("startTime", String.valueOf(session.getStartTime()));
            rss.updateString("expireTime", String.valueOf(session.getExpireTime()));
            rss.insertRow();
            rss.last();
            sessionID = rss.getInt("sessionID");
          }

          ResultSet rsr = s.executeQuery("SELECT * FROM Route;");
          rsr.moveToInsertRow();
          rsr.updateTimestamp("shift_normal_start_time", shiftStartTime);
          rsr.updateInt("route_num", j);
          rsr.updateInt("status", ((Route)routesInShiftI.get(j)).getStatus().getValue());
          if (sessionID != -1) rsr.updateInt("sessionID", sessionID);
          rsr.insertRow();
          rsr.last();

          int routeID = rsr.getInt("id");
          Vector tasksInRoutej = ((Route)routesInShiftI.get(j)).taskSet;
          int routejSize = tasksInRoutej.size();
          for (int k = 0; k < routejSize; ++k) {
            Task t = (Task)tasksInRoutej.get(k);
            ResultSet rst = s.executeQuery("SELECT * FROM Task;");
            rst.moveToInsertRow();
            rst.updateString("commodity_id", t.cmdt.id);
            rst.updateInt("route_id", routeID);
            rst.updateInt("sequence_num", k);
            rst.updateInt("status", t.currentStatus.getValue());

            if (t.size == 1) {
              if (t.gT == null) {
                rst.updateInt("small", 1);
                rst.updateInt("large", 0);
              }
              else {
                rst.updateInt("small", 2);
                rst.updateInt("large", 0);
              }
            }
            else {
              rst.updateInt("small", 0);
              rst.updateInt("large", 1);
            }

            rst.updateTimestamp("planned_start", new Timestamp(t.startT.getTimeInMillis()));
            rst.updateTimestamp("planned_finish", new Timestamp(t.finishT.getTimeInMillis()));

            if (t.actualStartT != null) {
              rst.updateTimestamp("actual_start", new Timestamp(t.actualStartT.getTimeInMillis()));
            }

            if (t.actualFinishT != null) {
              rst.updateTimestamp("actual_finish", new Timestamp(t.actualFinishT.getTimeInMillis()));
            }

            rst.insertRow();
            rst.moveToCurrentRow();
          }
        }
      }
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}