package uk.ac.nottingham.ningboport.planner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import uk.ac.nottingham.ningboport.planner.algorithms.Insertion;
import uk.ac.nottingham.ningboport.planner.algorithms.VNS;
import uk.ac.nottingham.ningboport.planner.statistics.StatisticsAnalyzer;

public class RoutingPlanner
{
  private static String output = "";
  private static String networkFileName = null;
  private static String commodityFileName = null;
  private static String solutionFileName = null;
  private static String startDate = null;
  private static String periodLength = null;
  private static String numOfPeriods = null;
  private static boolean readCommodityFromDB = false;
  private static String dbUsername = null;
  private static String dbPassword = null;
  private static int maximumShakeTime = 17;
  private static int maximumRunTime = 300;
  private static boolean tabuEnabled = false;
  private static int shakeBase = 5;
  private static boolean firstImprove = false;
  private static int postfix = 0;

  public static void main(String[] args)
  {
    for (int i = 0; i < args.length; ++i)
      if (args[i].charAt(0) == '-') {
        String argType = args[i].substring(1);
        if (argType.equals("n")) {
          networkFileName = args[(++i)];
        }
        else if (argType.equals("c")) {
          commodityFileName = args[(++i)];
        }
        else if (argType.equals("s")) {
          startDate = args[(++i)];
        }
        else if (argType.equals("pl")) {
          periodLength = args[(++i)];
        }
        else if (argType.equals("np")) {
          numOfPeriods = args[(++i)];
        }
        else if (argType.equals("sol")) {
          solutionFileName = args[(++i)];
        }
        else if (argType.equals("maximumRunTime")) {
          maximumRunTime = Integer.parseInt(args[(++i)]);
        }
        else if (argType.equals("maximumShakeTime")) {
          maximumShakeTime = Integer.parseInt(args[(++i)]);
        }
        else if (argType.equals("shakeBase")) {
          shakeBase = Integer.parseInt(args[(++i)]);
        }
        else if (argType.equals("tabuEnabled")) {
          tabuEnabled = Boolean.parseBoolean(args[(++i)]);
        }
        else if (argType.equals("firstImprove")) {
          firstImprove = Boolean.parseBoolean(args[(++i)]);
        }
        else if (argType.equals("db")) {
          readCommodityFromDB = true;
        }
        else if (argType.equals("dbUsername")) {
          dbUsername = args[(++i)];
        }
        else if (argType.equals("dbPassword")) {
          dbPassword = args[(++i)];
        }
        else {
          errormsg("Bad parameter!");
          return;
        }

      }


    Network nw = new Network();
    Calendar stime = null;

    if (readCommodityFromDB) {
      if ((networkFileName == null) || (startDate == null) || 
        (periodLength == null) || (numOfPeriods == null) || 
        (dbUsername == null) || (dbPassword == null)) {
        errormsg("Missing Parameter");
        return;
      }

      stime = readCalString(startDate);
      nw.createPeriodBoundaries(stime, Integer.parseInt(periodLength), Integer.parseInt(numOfPeriods));
      try {
        NetworkReader.readFile(nw, networkFileName);
        CommodityReader.readDatabase(nw, dbUsername, dbPassword);
        Solution s = new Solution(nw, null);
        s.readSolutionFromDatabase(dbUsername, dbPassword);
        System.out.println(nw.outputRoutes());
      } catch (IOException e) {
        e.printStackTrace();
      }

      nw.combineLightTask(nw.taskSet);
    }
    else if (solutionFileName == null) {
      if ((networkFileName == null) || (startDate == null) || 
        (commodityFileName == null) || 
        (periodLength == null) || (numOfPeriods == null)) {
        errormsg("Missing Parameter");
        return;
      }
      try
      {
        stime = readCalString(startDate);
        nw.createPeriodBoundaries(stime, Integer.parseInt(periodLength), Integer.parseInt(numOfPeriods));
        NetworkReader.readFile(nw, networkFileName);
        CommodityReader.readFile(commodityFileName, nw);
      }
      catch (IOException e) {
        e.printStackTrace();
      }

      nw.combineLightTask(nw.taskSet);
    }
    else {
      stime = readCalString(startDate);
      nw.createPeriodBoundaries(stime, Integer.parseInt(periodLength), Integer.parseInt(numOfPeriods));
      try {
        NetworkReader.readFile(nw, networkFileName);
        RealWorldSolutionReader rwsr = new RealWorldSolutionReader(nw);
        rwsr.readSolution(solutionFileName);
      } catch (IOException rwsr) {
        e.printStackTrace();
      }
    }

    Insertion ins = new Insertion(nw, false, false);
    ins.run();

    VNS ts = new VNS(nw);
    ts.run(maximumRunTime, maximumShakeTime, shakeBase, tabuEnabled, firstImprove);

    if (readCommodityFromDB) {
      Solution s = new Solution(nw, null);
      s.writeSolutionToDatabase(dbUsername, dbPassword);
    }

    try
    {
      File f = new File(startDate + "_" + numOfPeriods + "__" + postfix + ".routes");
      while (f.exists()) {
        postfix += 1;
        f = new File(startDate + "_" + numOfPeriods + "__" + postfix + ".routes");
      }
      PrintWriter pw = new PrintWriter(f);
      pw.println(nw.outputRoutes());
      pw.println(nw.getSolutionObjectiveValues());
      pw.flush();
      pw.close();
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    StatisticsAnalyzer sa = new StatisticsAnalyzer(nw);
    sa.outputStatistics(startDate + "_" + numOfPeriods + "__" + postfix + ".statistics");

    writeOutput();
  }

  public static void addToOutput(String s) {
    if (s == null) return;

    output += s; }

  public static void writeOutput() {
    File f;
    try {
      f = new File(startDate + "_" + numOfPeriods + "__" + postfix + ".output");
      PrintWriter pw = new PrintWriter(f);
      pw.println(output);
      pw.flush();
      pw.close();
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    System.out.println(output);
  }

  public static Calendar readCalString(String s) {
    if ((s == null) || (s.length() != 14))
    {
      return null;
    }
    Calendar c = new GregorianCalendar();
    c.set(Integer.parseInt(s.substring(0, 4)), 
      Integer.parseInt(s.substring(4, 6)) - 1, 
      Integer.parseInt(s.substring(6, 8)), 
      Integer.parseInt(s.substring(8, 10)), 
      Integer.parseInt(s.substring(10, 12)), 
      Integer.parseInt(s.substring(12, 14)));
    return c;
  }

  public static void errormsg(String s) {
    System.out.println(s);
  }
}