package uk.ac.nottingham.ningboport.planner.statistics;

import java.util.Vector;
import uk.ac.nottingham.ningboport.planner.Commodity;
import uk.ac.nottingham.ningboport.planner.Node;
import uk.ac.nottingham.ningboport.planner.Task;

class FlowVector
{
  private Node node1;
  private Node node2;
  private Vector<FlowVector> flowVectors;
  public int flow;

  public FlowVector(Task a, Vector<FlowVector> v)
  {
    this.node1 = a.cmdt.src;
    this.node2 = a.cmdt.dest;
    this.flowVectors = v;
    if (a.gT == null) this.flow = 1;
    else this.flow = 2;
  }

  public FlowVector(Node n1, Node n2, Vector<FlowVector> v) {
    this.node1 = n1;
    this.node2 = n2;
    this.flow = 1;
    this.flowVectors = v;
  }

  private FlowVector get(Node n1, Node n2) {
    int l = this.flowVectors.size();
    for (int i = 0; i < l; ++i) {
      FlowVector fv = (FlowVector)this.flowVectors.get(i);
      if ((fv.node1 == n1) && (fv.node2 == n2))
        return fv;

    }

    return null;
  }

  public int add(FlowVector v)
  {
    int c = 0;
    FlowVector a = null; FlowVector b = null;
    if ((v.node1 == this.node1) && (v.node2 == this.node2)) {
      c = 1;
    }
    else if ((v.node1 == this.node2) && (v.node2 == this.node1)) {
      c = 2;
    }
    else if (v.node1 == this.node2) {
      a = this;
      b = v;
      c = 3;
    }
    else if (v.node2 == this.node1) {
      a = v;
      b = this;
      c = 3;
    }

    switch (c)
    {
    case 1:
      this.flow += v.flow;
      this.flowVectors.remove(v);
      return 2;
    case 2:
      if (this.flow >= v.flow) {
        this.flow -= v.flow;
        this.flowVectors.remove(v);
        return 2;
      }
      v.flow -= this.flow;
      this.flowVectors.remove(this);
      return 1;
    case 3:
      FlowVector fv = get(a.node1, b.node2);
      if (fv == null) {
        this.flowVectors.add(new FlowVector(a.node1, b.node2, this.flowVectors));
        fv = (FlowVector)this.flowVectors.lastElement();
      }

      if (a.flow > b.flow) {
        fv.flow += b.flow;
        a.flow -= b.flow;
        this.flowVectors.remove(b);
        if (b == this) return 1;
        return 2;
      }
      if (b.flow > a.flow) {
        fv.flow += a.flow;
        b.flow -= a.flow;
        this.flowVectors.remove(a);
        if (a == this) return 1;
        return 2;
      }

      fv.flow += a.flow;
      this.flowVectors.remove(a);
      this.flowVectors.remove(b);
      return 3;
    }

    return 0;
  }

  public String toString()
  {
    return "[" + this.node1.id + " " + this.node2.id + "]\t" + this.flow;
  }
}