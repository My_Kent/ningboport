package uk.ac.nottingham.ningboport.planner.statistics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;
import uk.ac.nottingham.ningboport.planner.Commodity;
import uk.ac.nottingham.ningboport.planner.Network;
import uk.ac.nottingham.ningboport.planner.Node;
import uk.ac.nottingham.ningboport.planner.Route;
import uk.ac.nottingham.ningboport.planner.Task;
import uk.ac.nottingham.ningboport.planner.Task.sortingMethod;

public class StatisticsAnalyzer
{
  private Network nw;

  public StatisticsAnalyzer(Network nw)
  {
    this.nw = nw;
  }

  public String getArcUsage() {
    Vector nodes = this.nw.nodes;
    Vector taskSet = this.nw.taskSet;

    int nsize = nodes.size();
    int[][] usageMatrix = new int[nsize][nsize];
    for (int i = 0; i < taskSet.size(); ++i) {
      Commodity c = ((Task)taskSet.get(i)).cmdt;
      usageMatrix[c.src.index][c.dest.index] += 1;
    }
    String out = "-----: arc usage :----\n";
    out = out + "from\n";

    for (int i = 0; i < nsize; ++i) {
      out = out + nodes.get(i) + "\t";
      for (int j = 0; j < nsize; ++j)
        out = out + usageMatrix[i][j] + "\t";

      out = out + "\n";
    }
    return out;
  }

  public String getTaskCountStatistics() {
    int numberOfShifts = this.nw.getNumberOfShifts();
    Vector mandatoryCommodities = this.nw.mandatoryCommodities;
    Vector optionalCommodities = this.nw.optionalCommodities;

    String out = "----: Task count for each period :----\n";
    out = out + "\tmandatory\toptional\n";
    for (int i = 0; i < numberOfShifts; ++i) {
      out = out + (i + 1);

      int count = 0;
      Vector commoditySet = (Vector)mandatoryCommodities.get(i);
      int size = commoditySet.size();
      for (int j = 0; j < size; ++j)
        count += this.nw.getTasksForCommodity((Commodity)commoditySet.get(j)).size();

      out = out + "\t" + count;

      count = 0;
      commoditySet = (Vector)optionalCommodities.get(i);
      size = commoditySet.size();
      for (j = 0; j < size; ++j)
        count += this.nw.getTasksForCommodity((Commodity)commoditySet.get(j)).size();

      out = out + "\t" + count + "\n";
    }
    return out;
  }

  public String getNodeThroughputDeadlineStatistics(Task.sortingMethod method)
  {
    Vector nodes = this.nw.nodes;
    Vector taskSet = this.nw.taskSet;
    int numberOfShifts = this.nw.getNumberOfShifts();

    String out1 = new String();
    String out2 = new String();
    String out3 = new String();
    String out4 = new String();

    Vector ts = new Vector();
    ts.addAll(taskSet);
    Task.sortBy = method;
    Collections.sort(ts);
    for (int i = 0; i < nodes.size(); ++i) {
      Node n = (Node)nodes.get(i);

      for (int j = 0; j < numberOfShifts; ++j) {
        int inTask = 0; int outTask = 0; int inContainer = 0; int outContainer = 0;
        Calendar pStart = this.nw.periodStartTimes[j];
        Calendar pEnd = this.nw.periodEndTimes[j];
        for (int k = 0; k < ts.size(); ++k) {
          Task t = (Task)ts.get(k);
          Commodity c = t.cmdt;
          if (method == Task.sortingMethod.DEADLINE) {
            if (c.deadline.before(pEnd)) if (!(c.deadline.after(pStart)));

          }
          else if (method == Task.sortingMethod.AVAIL_TIME) {
            if (c.availTime.before(pEnd)) {
              if (!(c.availTime.after(pStart)));
            }

          }
          else if (c.src == n) {
            ++outTask;
            ++outContainer;
            if (t.gT != null)
              ++outContainer;
          }
          else if (c.dest == n) {
            ++inTask;
            ++inContainer;

            if (t.gT != null)
              ++inContainer;
          }
        }

        out1 = out1 + inTask + "\t";
        out2 = out2 + outTask + "\t";
        out3 = out3 + inContainer + "\t";
        out4 = out4 + outContainer + "\t";
      }

      out1 = out1 + "\n";
      out2 = out2 + "\n";
      out3 = out3 + "\n";
      out4 = out4 + "\n";
    }
    return "Node Throughput(Deadline)\n" + out1 + "\n\n" + out2 + "\n\n" + out3 + "\n\n" + out4 + "\n\n";
  }

  public String getNodeThroughputStatistics()
  {
    Vector nodes = this.nw.nodes;
    int numberOfShifts = this.nw.getNumberOfShifts();

    Vector statistics = new Vector();
    int nodeQuantity = nodes.size();
    for (int i = 0; i < numberOfShifts; ++i) {
      Vector rs = (Vector)this.nw.routesOfShifts.get(i);

      Vector periodStatistics = new Vector();
      statistics.add(periodStatistics);

      for (int j = 0; j < nodeQuantity; ++j) {
        Node n = (Node)nodes.get(j);
        int inTask = 0; int outTask = 0; int inContainer = 0; int outContainer = 0;
        for (int k = 0; k < rs.size(); ++k) {
          Vector ts = ((Route)rs.get(k)).taskSet;
          for (int l = 0; l < ts.size(); ++l) {
            Task t = (Task)ts.get(l);

            if (t.cmdt.src == n) {
              ++outTask;
              ++outContainer;
              if (t.gT != null)
                ++outContainer;
            }
            else if (t.cmdt.dest == n) {
              ++inTask;
              ++inContainer;

              if (t.gT != null)
                ++inContainer;
            }
          }

        }

        periodStatistics.add(Integer.valueOf(inTask));
        periodStatistics.add(Integer.valueOf(outTask));
        periodStatistics.add(Integer.valueOf(inContainer));
        periodStatistics.add(Integer.valueOf(outContainer));
      }
    }

    String out1 = "---: Task in :---\n";
    String out2 = "---: Task out :---\n";
    String out3 = "---: Container in :---\n";
    String out4 = "---: Container out :---\n";
    for (int nodeI = 0; nodeI < nodeQuantity; ++nodeI) {
      for (int periodI = 0; periodI < numberOfShifts; ++periodI) {
        Vector periodStatistics = (Vector)statistics.get(periodI);
        out1 = out1 + periodStatistics.get(nodeI * 4) + "\t";
        out2 = out2 + periodStatistics.get(nodeI * 4 + 1) + "\t";
        out3 = out3 + periodStatistics.get(nodeI * 4 + 2) + "\t";
        out4 = out4 + periodStatistics.get(nodeI * 4 + 3) + "\t";
      }
      out1 = out1 + "\n";
      out2 = out2 + "\n";
      out3 = out3 + "\n";
      out4 = out4 + "\n";
    }

    return out1 + "\n\n" + out2 + "\n\n" + out3 + "\n\n" + out4 + "\n\n";
  }

  public String getSolutionFlowVectorStatistics()
  {
    String out = "-----: FlowVector(solution) :----\n";
    int numberOfShifts = this.nw.getNumberOfShifts();
    for (int i = 0; i < numberOfShifts; ++i) {
      out = out + getShiftFlowVectorStatistics(i);
    }

    out = out + "-----: end of FlowVector(solution) :----\n";
    return out;
  }

  public String getShiftFlowVectorStatistics(int shift_index) {
    String out = "|----------- " + shift_index + " -----------|\n";
    Vector fvv = new Vector();

    Vector rs = (Vector)this.nw.routesOfShifts.get(shift_index);
    int rs_size = rs.size();
    for (int i = 0; i < rs_size; ++i) {
      Vector r = ((Route)rs.get(i)).taskSet;
      int r_size = r.size();
      for (int j = 0; j < r_size; ++j)
        fvv.add(new FlowVector((Task)r.get(j), fvv));

    }

    if (fvv.size() == 0) {
      out = out + "Empty\n\n";
      return out;
    }

    int fvv_index = 0;
    FlowVector f1 = null;
    while (fvv_index < fvv.size()) {
      f1 = (FlowVector)fvv.get(fvv_index);

      FlowVector f2 = null;
      int fvv_index2 = fvv_index + 1;
      int r = 0;
      while (fvv_index2 < fvv.size()) {
        f2 = (FlowVector)fvv.get(fvv_index2);

        r = f1.add(f2);
        if ((r == 1) || (r == 3)) {
          --fvv_index;
          break;
        }
        if (r == 0)
          ++fvv_index2;

      }

      ++fvv_index;
    }

    int total_imb = 0;
    for (int i = 0; i < fvv.size(); ++i) {
      FlowVector fv = (FlowVector)fvv.get(i);
      out = out + fv + "\n";
      total_imb += fv.flow;
    }

    out = out + "total:\t" + total_imb + "\n\n";
    return out;
  }

  public String getRouteAppearanceRate() {
    int numberOfShifts = this.nw.getNumberOfShifts();

    Vector allRoutes = new Vector();
    for (int i = 0; i < numberOfShifts; ++i)
      allRoutes.addAll((Collection)this.nw.routesOfShifts.get(i));

    int[] times = new int[allRoutes.size()];
    Route[] route = new Route[allRoutes.size()];
    int c = 0;

    while (!(allRoutes.isEmpty())) {
      route[c] = ((Route)allRoutes.remove(0));
      for (int i = 0; i < allRoutes.size(); ++i)
        if (((Route)allRoutes.get(i)).geographicallyIdenticalTo(route[c])) {
          times[c] += 1;
          allRoutes.remove(i);
          --i;
        }

      ++c;
    }

    String res = "========Route appearance rate========\n";
    for (int i = 0; i < c; ++i) {
      res = res + "Route: " + route[i].outputPortSequence();
      res = res + "\t" + (times[i] + 1) + "\n";
    }
    res = res + "----------------------\n";
    return res; }

  public void outputStatistics(String filename) {
    PrintWriter pw;
    try {
      pw = new PrintWriter(new File(filename));
      pw.println(getNodeThroughputStatistics());
      pw.println(getSolutionFlowVectorStatistics());
      pw.flush();
      pw.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
}