package uk.ac.nottingham.ningboport.planner;

import java.io.PrintStream;
import java.util.Calendar;

public class Task
  implements Comparable<Task>
{
  public int taskID;
  public Commodity cmdt;
  public int size;
  public double fuel;
  public Calendar startT;
  public Calendar finishT;
  public Calendar actualStartT;
  public Calendar actualFinishT;
  public boolean emergent;
  private int lateness;
  public String driver = "none";
  public String vehicleID = "none";
  public static sortingMethod sortBy = sortingMethod.DEADLINE;
  public Task gT;
  public taskStatus currentStatus = taskStatus.PLANNED;

  public boolean freezed()
  {
    return (this.currentStatus != taskStatus.PLANNED);
  }

  public int getLateness()
  {
    if (this.currentStatus == taskStatus.FINISHED)
      return this.lateness;

    return 0;
  }

  public boolean setLateness(int delay) {
    System.out.println("set delay:" + this.cmdt.id);
    if (this.currentStatus != taskStatus.FINISHED)
      return false;

    this.lateness = delay;
    return true;
  }

  public int compareTo(Task that)
  {
    if (sortBy == sortingMethod.DEADLINE) {
      if (this.cmdt.deadline.before(that.cmdt.deadline))
        return -1;
      if (this.cmdt.deadline.after(that.cmdt.deadline))
        return 1;

      return 0;
    }
    if (sortBy == sortingMethod.AVAIL_TIME) {
      if (this.cmdt.availTime.before(that.cmdt.availTime))
        return -1;
      if (this.cmdt.availTime.after(that.cmdt.availTime))
        return 1;

      return 0;
    }
    if (sortBy == sortingMethod.ACTUAL_STARTING_TIME)
      return this.actualStartT.compareTo(that.actualStartT);

    return 0;
  }

  public String toString() {
    String res = this.cmdt.id + 
      ": " + this.cmdt.src.id + 
      "->" + this.cmdt.dest.id + " (" + 
      this.cmdt.availTime.getTime() + " - " + this.cmdt.deadline.getTime() + 
      ") weight:" + this.size;
    if (this.gT != null)
      res = res + " with " + this.gT.cmdt.id + " " + this.gT.size;

    return res;
  }

  public String toDisplayString() {
    String res = this.cmdt.id + 
      ": \n" + this.cmdt.src.id + 
      "->" + this.cmdt.dest.id + " \n(" + 
      this.cmdt.availTime.getTime() + " - " + this.cmdt.deadline.getTime() + 
      ") \nweight:" + this.size;
    if (this.gT != null)
      res = res + " with " + this.gT.cmdt.id + " " + this.gT.size;

    return res;
  }

  public String toDisplayString1() {
    String res = this.cmdt.id + 
      ": ";
    return res;
  }

  public String toDisplayString2() {
    String res = this.cmdt.src.id + 
      "->" + this.cmdt.dest.id;
    return res;
  }

  public String toDisplayString3() {
    String res = " (" + 
      this.cmdt.availTime.getTime() + " - " + this.cmdt.deadline.getTime() + 
      ")";
    return res;
  }

  public String toDisplayString4() {
    String res = "weight:" + this.size;
    return res;
  }

  public String toDisplayString5() {
    String res = "";
    if (this.gT != null)
      res = res + " with " + this.gT.cmdt.id + " " + this.gT.size;

    return res;
  }

  public Task clone()
  {
    Task t = new Task();
    t.cmdt = this.cmdt;
    t.size = this.size;
    t.fuel = this.fuel;
    if (t.startT != null) {
      t.startT = ((Calendar)this.startT.clone());
    }

    if (t.finishT != null)
      t.finishT = ((Calendar)this.finishT.clone());

    t.emergent = this.emergent;
    t.lateness = this.lateness;
    t.driver = this.driver;
    t.vehicleID = this.vehicleID;

    if (this.gT != null)
      t.gT = this.gT.clone();

    t.currentStatus = this.currentStatus;

    return t;
  }

  public static enum sortingMethod
  {
    AVAIL_TIME, DEADLINE, SHORTEST_DISTANCE, LONGEST_DISTANCE, ACTUAL_STARTING_TIME;
  }

  public static enum taskStatus
  {
    PLANNED, RUNNING, FINISHED;

    private int value;

    public int getValue()
    {
      return this.value;
    }

    public static taskStatus convertFromInt(int i) {
      if (i == 0)
        return PLANNED;
      if (i == 0)
        return RUNNING;

      return FINISHED;
    }
  }
}