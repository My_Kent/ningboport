package uk.ac.nottingham.ningboport.network.model;

public class XMLLoginComm {

	XMLSession session;
	XMLLogin login;
	
	public XMLLoginComm( XMLSession s, XMLLogin l) {
		session = s;
		login = l;
	}

	public XMLSession getSession() {
		return session;
	}

	public void setSession(XMLSession session) {
		this.session = session;
	}

	public XMLLogin getLogin() {
		return login;
	}

	public void setLogin(XMLLogin login) {
		this.login = login;
	}
}
