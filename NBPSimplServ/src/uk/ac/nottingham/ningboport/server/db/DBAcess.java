package uk.ac.nottingham.ningboport.server.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBAcess {
	private static Connection connect;
	
	public final static Connection getConnection(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost/test","nbp","123456");
		} catch(ClassNotFoundException e){
			e.printStackTrace();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return connect;
	}
	
	public void closeConnection() throws Exception{
		connect.close();
	}
}
