package uk.ac.nottingham.ningboport.server.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {
	private Connection con;
	private Statement stat;
	private ResultSet rs;
	
	public DBManager(){
		con = DBAcess.getConnection();
	}
	
	/*
	 * Use sql statement to get a result set
	 */
	public ResultSet getRs(String sql){
		try{
			stat = con.createStatement();
			rs = stat.executeQuery(sql);
		} catch(SQLException e){
			e.printStackTrace();
		}
		return rs;
	}
	
	/*
	 * Update the database with certian sql statement
	 */
	public int updb(String sql){
		int count = 0;
		try{
			stat = con.createStatement();
			count = stat.executeUpdate(sql);
			destroy();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return count;
	}
	
	public void destroy(){
		try{
			con.close();
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	
}
