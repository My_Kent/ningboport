package uk.ac.nottingham.ningboport.server.datamgr;

import java.sql.ResultSet;
import java.sql.SQLException;
import uk.ac.nottingham.ningboport.client.test.TaskUpdate;
import uk.ac.nottingham.ningboport.network.model.XMLLogin;
import uk.ac.nottingham.ningboport.network.model.XMLLoginComm;
import uk.ac.nottingham.ningboport.network.model.XMLSession;
import uk.ac.nottingham.ningboport.server.db.DBManager;

public class Verification {
	
	public static XMLLoginComm validateLogin(XMLLoginComm comm){
		XMLLogin login = comm.getLogin();
		
		String driverName = login.getUsername();
		String password = login.getPassword();
		String vehicleID = login.getVehicleID();
		
		XMLSession session = new XMLSession();
		if(checkPassword(driverName, password, vehicleID)){
			session = checkSession(driverName, vehicleID);
		}
		comm.setSession(session);
		return comm;
	}
	
	public static boolean checkPassword(String driverName, String password, String vehicleID){
		long login_time = System.currentTimeMillis();
		DBManager dbm = new DBManager();
		
		//TODO sql query statement end with ;??
		ResultSet rs = dbm.getRs("SELECT * FROM Driver WHERE driverName = '" + driverName + "'AND password =" + password + "'");
		
		try{
			if(rs.next()){
				rs.close();
				ResultSet rss = dbm.getRs("SELECT * FROM Vehicle WHERE VehicleID = '" + vehicleID + "'");
				if(rss.next()){
					rss.close();
					dbm.updb("INSERT INTO Login(login_driverName, login_password, login_vehicleID, login_success, login_time) VALUES('" + driverName + 
							"," + password + "," + vehicleID + "," + true + "," + login_time + ");");
					dbm.destroy();
					return true;
				}
				else{
					rss.close();
					dbm.updb("INSERT INTO Login(login_driverName, login_password, login_vehicleID, login_success, login_time) VALUES('" + driverName + 
							"," + password + "," + vehicleID + "," + false + "," + login_time + ");");
					dbm.destroy();
					return false;
				}
			}
			else{
				rs.close();
				dbm.updb("INSERT INTO Login(login_driverName, login_password, login_vehicleID, login_success, login_time) VALUES('" + driverName + 
						"," + password + "," + vehicleID + "," + false + "," + login_time + ");");
				return false;
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	private static void insertSession(XMLSession session){
		DBManager dbm = new DBManager();
		String driverName = session.getDriverName();
		String vehicleID = session.getVehicleID();
		long startTime = session.getStartTime();
		long expireTime = session.getExpireTime();
		TaskUpdate a = new TaskUpdate();
		ResultSet rs = dbm.getRs("SELECT * FROM Session WHERE session_driverName = '" + driverName +
				"'AND session_vehicleID = '" + vehicleID + "'AND startTime = " + startTime +
				"AND exipreTime = " + expireTime + ";");
		try{
			if(rs.next()){
				//TODO: if exists, return some message??
			}
			else{
				// if do not exist, insert into DBS and assign Route
				dbm.updb("INSERT INTO Session(session_drivrName, session_vehicleID, startTime, expireTime) values ('" +
				driverName + "','" +  vehicleID + "'," + startTime + "," + expireTime + ");");
				dbm = new DBManager();
				ResultSet rss = dbm.getRs("SELECT sessionID FROM Session WHERE session_driverName = '" + driverName + "'AND session_vehicleID = '"
						+ vehicleID + "' AND startTime = " + startTime + "AND expireTime = " + expireTime + ";");
				if(rss.next()){
					a.assignRoutes(rss.getInt("sessionID"));
				}
				else{
					System.out.println("error");
				}
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			dbm.destroy();
		}
	}
	
	
	private static XMLSession checkSession(String driverName, String vehicleID){
		DBManager dbm = new DBManager();
		long currentTime = System.currentTimeMillis();
		TaskUpdate a = new TaskUpdate();
		ResultSet rs = dbm.getRs("SELECT * FROM Session WHERE session_driverName = '" + driverName + "'AND session_vehicleID = '"
				+ vehicleID + "'ORDER BY sessionID DESC;");
		
		try{
			if(rs.next()){
				long startTime = Long.parseLong(rs.getString("startTime"));
				long expireTime = Long.parseLong(rs.getString("expireTime"));
				int sessionID = rs.getInt("sessionID");
				if(expireTime > currentTime){
					// the session already exist, return the session
					// TODO return all the tasks of this session to Phone
					a.getTasks(sessionID);
					return new XMLSession(driverName, vehicleID, startTime, expireTime);
				}
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			dbm.destroy();
		}
		
		// else, return new session
		// TODO Assign routes that doesn't assigned and return them to Phone
		XMLSession session = new XMLSession(driverName, vehicleID);
		insertSession(session);
		return session;
	}
	
	
	

}
