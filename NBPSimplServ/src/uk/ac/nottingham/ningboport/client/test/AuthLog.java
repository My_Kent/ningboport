package uk.ac.nottingham.ningboport.client.test;

import java.util.Date;

public class AuthLog {
	private String driver;
	private String password;
	private String id;
	private boolean success;
	private Date time;
	
	// TODO Boolean -> boolean?
	public AuthLog(String d, String p, String id, boolean s, Date t){
		this.driver = d;
		this.password = p;
		this.id = id;
		this.success = success;
		this.time = t;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	
}
