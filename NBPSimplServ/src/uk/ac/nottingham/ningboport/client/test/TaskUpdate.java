package uk.ac.nottingham.ningboport.client.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import uk.ac.nottingham.ningboport.network.model.XMLSession;
import uk.ac.nottingham.ningboport.network.model.XMLTask.Action;
import uk.ac.nottingham.ningboport.planner.Commodity;
import uk.ac.nottingham.ningboport.planner.Node;
import uk.ac.nottingham.ningboport.planner.Task;
import uk.ac.nottingham.ningboport.server.datamgr.SingleUpdateManager;
import uk.ac.nottingham.ningboport.server.datamgr.UpdateManager;
//import uk.ac.nottingham.ningboport.server.datamgr.SingleUpdateManager;
//import uk.ac.nottingham.ningboport.server.datamgr.UpdateManager;
import uk.ac.nottingham.ningboport.server.db.DBManager;
//import uk.ac.nottingham.ningboport.server.var.Global;
import uk.ac.nottingham.ningboport.server.var.Global;



/**
 * contains method that deal with task updating
 *
 */
public class TaskUpdate {
	
	/**
	 * When user finish/start a task, save the time into database
	 */
	public void taskAction(XMLSession s, int sequenceNo, Action action){
		DBManager m = new DBManager();
		String driverName = s.getDriverName();
		String startTime = Long.toString(s.getStartTime());
		String expireTime = Long.toString(s.getExpireTime());
		String vehicleID = s.getVehicleID();
		ResultSet rs = m.getRs("SELECT t.id From Session s, Route r, Task t"
				+ " WHERE session_driverName = '" + driverName
				+ "' AND s.startTime = " + startTime + " AND s.expireTime = " + expireTime
				+ " AND session_vehicleID = '" + vehicleID + " 'AND s.sessionID = r.sessionID"
				+ " AND r.id = t.route_id AND t.sequence_num = " + sequenceNo);
		
		try{
			if(rs.next()){
				int task_id = rs.getInt("id");
				String currentTime = getCurrentTime();
				if(action.equals(Action.start)){
					m.updb("UPDATE Task SET actual_start = '" + currentTime
							+ "' WHERE id = " + task_id);
				}
				if(action.equals(Action.finish)){
					m.updb("UPDATE Task SET actual_finish = '" + currentTime
							+ "' WHERE id = " + task_id);
				}
				
				//TODO: Send to Phone about changing action
				ArrayList<UpdateManager> ml = Global.getUpdateTasks();
				int l = ml.size();
				int index = -1;
				for(int i = 0; i < l; i++){
					if(ml.get(i).getSession().equals(s)){
						index = i;
						break;
					}
				}
				UpdateManager um = new UpdateManager(s);
				ArrayList<SingleUpdateManager> sl = new ArrayList<SingleUpdateManager>();
				if(index != -1){
					um = ml.get(index);
					sl = um.getTasks();
				}
				
				Task t = new Task();
				SingleUpdateManager a = new SingleUpdateManager(action, t);
				a.setSequenceNo(sequenceNo);
				sl.add(a);
				um.setTask(sl);
				// SAVE THE UPDATE TASK INTO GLOBAL VARIABLE
				if(index != -1){
					ml.set(index, um);
					Global.setUpdateTasks(ml);
				}
				else{
					Global.addUpdateTask(um);
				}				
			} 
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
	}
	
	private static String getCurrentTime(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	/**
	 * When User Login and generate a new session, assign route to this session
	 */
	public void assignRoutes(int sessionID){
		DBManager m = new DBManager();
		m.updb("UPDATE Route Set sessionID = " + sessionID + "WHERE sessionID IS NULL LIMIT1;");
		getTasks(sessionID);
	}
	
	/**
	 * get All the task by sessionID
	 * used in case of first-login and re-login
	 */
	public void getTasks(int sessionID){
		DBManager m = new DBManager();
		ResultSet rs = m.getRs("SELECT * FROM Session WHERE sessionID = " + sessionID);
		try{
			if(rs.next()){
				String driverName = rs.getString("session_driverName");
				String vehicleID = rs.getString("session_vehicleID");
				long startTime = Long.parseLong(rs.getString("startTime"));
				long expireTime = Long.parseLong(rs.getString("expireTime"));
				XMLSession s = new XMLSession(driverName, vehicleID, startTime, expireTime);
				ArrayList<UpdateManager> ml = Global.getUpdateTasks();
				int l = ml.size();
				int index = -1;
				for(int i = 0; i < l; i++){
					if(ml.get(i).getSession().equals(s)){
						index = i;
						break;
					}
				}
				
				UpdateManager um = new UpdateManager(s);
				ArrayList<SingleUpdateManager> sl = new ArrayList<SingleUpdateManager>();
				if(index != -1){
					um = ml.get(index);
					// clear the tasks of this session
					// sl = um.getTasks();
				}
				
				ResultSet rss = m.getRs("SELECT commodity_id, sequence_num FROM Route r, Commodity c, Task t WHERE r.sessionID = " + sessionID + 
						" AND route_id = r.id AND c.id = t.commodity_id ORDER BY sequence_num ASC");
				while(rss.next()){
					String commodityID = rss.getString("commodity_id");
					int sequenceNo = rss.getInt("sequence_num");
					ResultSet rsss = m.getRs("SELECT * FROM Commodity WHERE id = '" + commodityID + "'");
					if(rsss.next()){
						String source = rsss.getString("commodity_id");
						String destination = rsss.getString("destination");
						String available_t = rsss.getString("available_t");
						String deadline = rsss.getString("deadline");
						int small = rsss.getInt("small");
						int large = rsss.getInt("large");
						
						Task t = new Task();
						t.driver = s.getDriverName();
						Node src = new Node(source, 0, 0);
						Node dest = new Node(destination, 0, 0);
						
						Commodity c = new Commodity(commodityID, StringToCalendar(available_t), StringToCalendar(deadline), src, dest, small, large);
						t.cmdt = c;
						SingleUpdateManager a = new SingleUpdateManager(Action.add, t);
						a.setSequenceNo(sequenceNo);
						
						sl.add(a);
						um.setTask(sl);
						// SAVE THE UPDATE TASK INTO GLOBAL VARIABLE
						if(index != -1){
							ml.set(index, um);
						}
						else{
							Global.addUpdateTask(um);
						}
					}
				}

			}
		} catch(NumberFormatException e){
			e.printStackTrace();
		} catch(SQLException e){
			e.printStackTrace();
		}
		m.destroy();
	}
	
	
	
	private static Calendar StringToCalendar(String timeString){
		if(timeString == "" || timeString == null){
			return null;
		}
		else{
			SimpleDateFormat dateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = null;
			Calendar c = Calendar.getInstance();
			try{
				date = dateFormat.parse(timeString);
				c.setTime(date);
			} catch(ParseException e){
				e.printStackTrace();
			}
			return c;
		}
	}
	
	
	
	
}
